const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// Sketch parameters
const settings = {
  dimensions: [window.innerWidth, window.innerHeight],
  animate: true,
  units: 'px'
};

// Artwork function
const sketch = async ({ update, width, height }) => {
  // load image
  const image = await load('/brogrammer-by-waren-clark.png');

  // some resposiveness in sizes
  let size, jumpLimit, fontSize;
  if (width >= height) {
    size = width / 50;
    fontSize = size / 2;
    jumpLimit = height / 3;
  } else {
    size = width / 30;
    fontSize = size / 3;
    jumpLimit = height / 3;
  }

  let score = 0;
  const acceleration = 10;
  const bgVelocity = 5;
  const text = ';[]()""<=>!&|';

  // distance calculation
  const dist = (x1, y1, x2, y2) => {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
  };

  const point = { x: width / 3, y: height - size, vy: 0 };
  const challenges = Array(100)
    .fill()
    .map((v, i) => {
      const x = width / 3 + (i + 1) * 800 + Math.random() * 100;
      const y = height - (Math.random() * jumpLimit) / 2;
      const c = text.split('')[Math.floor(Math.random() * text.length)];
      return { x: x, y: y, c: c, indx: i };
    });

  let jump = false;
  document.body.onkeyup = e => {
    // when you hit the j - it jumps!
    if (e.keyCode == 74) {
      jump = true;
    }
  };

  //LOOP
  return ({ context, width, height, time }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 100%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 2;
    context.font = `${fontSize}pt Monospace`;
    context.strokeStyle = 'hsl(0, 0%, 0%)';
    context.fillStyle = 'hsl(0, 0%, 0%)';

    // change direction if it goes past the jump limit
    if (point.y <= height - jumpLimit) {
      point.vy += acceleration;
    }

    // stop the movement if it hits the ground
    if (point.y >= height - size - 2) {
      point.vy = 0;
    }

    // on jump
    if (jump) {
      point.vy = -acceleration;
      jump = false;
    }

    // add current speed to the position
    point.y += point.vy;

    // draw brogrammer
    context.drawImage(
      image,
      point.x - size / 2,
      point.y - size / 2,
      size,
      size
    );

    // circle around the image
    context.beginPath();
    context.arc(point.x, point.y, size, 0, Math.PI * 2, true);
    context.stroke();

    // draw challenges
    challenges.forEach((v, i) => {
      // draw the element
      context.fillText(v.c, v.x, v.y);

      // remove the element if it exits the screen
      if (v.x < 0) {
        challenges.splice(0, 1);
      }

      // move the element
      challenges[i].x -= bgVelocity;

      // collision detection
      let d = dist(v.x, v.y, point.x, point.y);
      if (d < size + fontSize) {
        // encouragement message
        //context.fillText('YEAAH', 100, 100);

        // remove the collided element
        challenges.splice(0, 1);

        // increase score text
        score++;
      }
    });

    context.save();
    if (challenges.length === 0) {
      // draw end text
      const endWidth = context.measureText('THE END').width;
      context.font = `${fontSize * 2}pt Monospace`;
      context.fillText('THE END', width / 2 - endWidth, height / 3);
    } else {
      // draw score text
      const scoreText = `${score}x BROGRAMMER!`;
      const scoreWidth = context.measureText(scoreText).width;
      context.font = `${fontSize * 2}pt Monospace`;
      context.fillText(scoreText, width / 2 - scoreWidth, height / 3);
    }
    context.restore();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
