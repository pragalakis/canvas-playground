// Classifier Variable
let classifier;
// Model URL
let imageModelURL = 'https://teachablemachine.withgoogle.com/models/qPHCFkZad/';

// Video
let video;
let flippedVideo;
// To store the classification
let label = '';

let question;
let questionFade = 0;
let toucan;
let toucanFade = 0;
let community;
let communityFade = 0;
let laugh;
let laughFade = 0;
let point;
let pointFade = 0;

// Load the model first
function preload() {
  classifier = ml5.imageClassifier(imageModelURL + 'model.json');
  question = loadImage('images/question.png');
  toucan = loadImage('images/toucan.png');
  community = loadImage('images/community.png');
  laugh = loadImage('images/laugh.png');
  point = loadImage('images/point.png');
}

function setup() {
  createCanvas(1280, 720);
  // Create the video
  video = createCapture(VIDEO);
  video.size(160, 120);
  // video.hide();

  flippedVideo = ml5.flipImage(video);
  // Start classifying
  classifyVideo();
}

function draw() {
  background(0, 255, 0);
  imageMode(CORNER);

  // Draw the video
  // tint(255);
  // image(flippedVideo, 0, 0);
  if (label == 'question') {
    questionFade = 255;
  } else if (label == 'toucan') {
    toucanFade = 255;
  } else if (label == 'communiteam') {
    communityFade = 255;
  } else if (label == 'laugh') {
    laughFade = 255;
  } else if (label == 'point') {
    pointFade = 255;
  }

  if (questionFade > 0) {
    tint(255, questionFade);
    image(question, 0, 0);
    questionFade -= 10;
  }

  if (toucanFade > 0) {
    tint(255, toucanFade);
    image(toucan, 0, 0, width, height);
    toucanFade -= 10;
  }

  if (communityFade > 0) {
    tint(255, communityFade);
    image(
      community,
      width / 2 - community.width / 2,
      height / 2 - community.height / 2
    );
    communityFade -= 10;
  }

  if (laughFade > 0) {
    tint(255, laugh);
    image(laugh, 0, 0, width, height);
    laughFade -= 10;
  }

  if (pointFade > 0) {
    tint(255, point);
    image(point, 0, 0, width, height);
    pointFade -= 10;
  }
}

// Get a prediction for the current video frame
function classifyVideo() {
  flippedVideo = ml5.flipImage(video);
  classifier.classify(flippedVideo, gotResult);
}

// When we get a result
function gotResult(error, results) {
  // If there is an error
  if (error) {
    console.error(error);
    return;
  }
  // The results are in an array ordered by confidence.
  // console.log(results[0]);
  label = results[0].label;
  // Classifiy again!
  classifyVideo();
}
