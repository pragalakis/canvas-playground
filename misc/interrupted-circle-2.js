const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 96%)';

    // fixing offset
    context.translate(-2, 0);

    const lineWidth = 1;
    const lineAngle = 2;
    let rad = 10;
    while (rad > 0) {
      // draw arcs
      context.beginPath();
      context.arc(
        width / 2 + lineAngle + lineAngle / 2 - rad / 5,
        height / 2 + lineWidth / 2 + rad / 10,
        rad,
        -Math.PI - (Math.PI - 1),
        2 * Math.PI,
        true
      );

      context.arc(
        width / 2 + lineAngle * (rad / 5),
        height / 2 - lineWidth,
        rad,
        0,
        Math.PI,
        true
      );

      context.arc(
        width / 2 - lineAngle + rad / 5,
        height / 2 + lineWidth,
        rad,
        -Math.PI,
        2 * Math.PI + 1,
        true
      );
      context.closePath();
      context.stroke();

      rad -= 0.3;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
