const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(40,90%,60%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,7%)';
    context.fillStyle = 'hsl(0,0%,7%)';
    context.lineWidth = 0.5;

    const drawBorderAndSmallSquare = (x, y, size, size2) => {
      context.beginPath();
      context.moveTo(x - size2, y - size / 2);
      context.lineTo(x - size / 2, y - size / 2);
      context.lineTo(x - size / 2, y - size2);
      context.stroke();

      context.fillRect(x - 2 * size2, y - 2 * size2, size2, size2);
    };

    const drawRotatedSquare = (x, y, offsetX, offsetY, size2) => {
      context.save();
      context.translate(x, y);
      context.rotate((45 * Math.PI) / 180);
      context.translate(-x, -y);
      context.fillRect(
        x + offsetX - (1.4 * size2) / 2,
        y + offsetY - (1.4 * size2) / 2,
        1.4 * size2,
        1.4 * size2
      );
      context.restore();
    };

    const drawTile = (x, y, size, size2) => {
      // top left
      drawBorderAndSmallSquare(x, y, size, size2);

      // top right
      context.save();
      context.translate(x, y);
      context.scale(-1, 1);
      context.translate(-x, -y);
      drawBorderAndSmallSquare(x, y, size, size2);
      context.restore();

      // bottom left
      context.save();
      context.translate(x, y);
      context.scale(1, -1);
      context.translate(-x, -y);
      drawBorderAndSmallSquare(x, y, size, size2);
      context.restore();

      // bottom right
      context.save();
      context.translate(x, y);
      context.scale(-1, -1);
      context.translate(-x, -y);
      drawBorderAndSmallSquare(x, y, size, size2);
      context.restore();

      // middle square
      drawRotatedSquare(x, y, 0, 0, size2);

      // top square
      drawRotatedSquare(x, y, -2, -2, size2);

      //  left square
      drawRotatedSquare(x, y, -2, 2, size2);
    };

    const size = 4;
    const size2 = size / 7;

    for (let i = 0; i < width + size; i += 1.4 * size) {
      for (let j = 0; j < height + size; j += 1.4 * size) {
        drawTile(i, j, size, size2);
      }
    }
  };
};

canvasSketch(sketch, settings);
