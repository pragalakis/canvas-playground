const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const color = ['black', 'white'];
  let radius = 12;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,94%)';
    context.fillRect(0, 0, width, height);

    const circle = function(x, y, rad, colorIndex) {
      context.fillStyle = `${color[colorIndex]}`;
      context.beginPath();
      context.arc(x, y, rad, 0, Math.PI * 2, true);
      context.fill();

      if (rad > radius / radius) {
        if (colorIndex == 1) {
          colorIndex = 0;
          x = x + rad * 0.13;
          y = y + rad * 0.13;
        } else {
          x = x + rad * 0.13;
          y = y - rad * 0.13;
          colorIndex = 1;
        }
        circle(x, y, rad - rad * 0.2, colorIndex);
      }
    };

    circle(width / 2, height / 2, radius, 0);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
