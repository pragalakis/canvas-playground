const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 95%)';
    context.fillRect(0, 0, width, height);

    let margin = 1;
    let fontSize = 2;
    const text = 'TEXTMAGIC';

    context.font = `bold ${fontSize}px Arial`;
    context.fillStyle = 'rgba(0,0,0,0.8)';

    // draw text
    for (let j = 2.5 * margin; j < height - margin; j += fontSize) {
      fontSize = fontSize - fontSize * 0.045;
      context.font = `bold ${fontSize}px Arial`;
      textLength = context.measureText(text).width;

      for (let i = margin; i < width; i += textLength) {
        context.fillText(text, i, j);
      }
    }

    // off-white right margin
    context.fillStyle = 'hsl(0, 0%, 95%)';
    context.fillRect(width - margin, 0, margin, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
