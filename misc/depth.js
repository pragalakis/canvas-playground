const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  //pixelsPerInch: 24.77,
  pixelsPerInch: 72,
  //pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const margin = 1;
    const sizex = 0.5;
    const sizey = 0.5;
    const cx = width / 2;
    const cy = height / 3;

    function draw(flip = false) {
      context.save();
      if (flip) {
        context.translate(width / 2, height / 2);
        context.scale(-1, -1);
        context.translate(-width / 2, -height / 2);
      }

      // top
      n = width / sizex / margin;
      for (let i = margin; i < width / 2; i += margin) {
        context.beginPath();
        context.moveTo(i, 0);
        context.lineTo(cx - sizex / 2 + i / n, cy - sizey / 2);
        context.stroke();
      }

      // left
      n = height / sizey / margin;
      for (let j = 0; j <= height; j += margin) {
        context.beginPath();
        context.moveTo(0, j);
        context.lineTo(cx - sizex / 2, cy - sizey / 2 + j / n);
        context.stroke();
      }

      // right
      n = height / sizey / margin;
      for (let j = 0; j <= height; j += margin) {
        context.beginPath();
        context.moveTo(width / 2, j);
        context.lineTo(cx, cy - sizey / 2 + j / n);
        context.stroke();
      }

      // bottom
      n = width / sizex / margin;
      for (let i = margin; i < width / 2; i += margin) {
        context.beginPath();
        context.moveTo(i, height);
        context.lineTo(cx - sizex / 2 + i / n, cy + sizey / 2);
        context.stroke();
      }

      context.restore();
    }

    draw();
    draw(true);
  };
};
canvasSketch(sketch, settings);
