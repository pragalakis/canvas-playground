const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 08%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.25;

    // draw frame
    let size = 22;
    const x = width / 2 - size / 2;
    const y = height / 2 - size / 2;
    context.strokeRect(x, y, size, size);

    // draw text-timestamp
    context.font = '0.6pt Monospace';
    const timestamp = Date.now();
    context.fillText(timestamp, x, y - 0.4);

    // time
    const d = new Date();
    const hour = (d.getHours() + 24) % 12 || 12;
    const min = d.getMinutes();
    const sec = d.getSeconds();
    const msec = d.getMilliseconds();

    context.lineWidth = 0.1;
    context.lineCap = 'round';
    context.translate(width / 2, height / 2);

    // draw arc - milliseconds
    context.beginPath();
    context.arc(
      0,
      0,
      size / 2.2,
      (3 * Math.PI) / 2,
      msec * ((2 * Math.PI) / 1000) - Math.PI / 2,
      false
    );
    context.stroke();

    // draw arc - seconds
    context.save();
    context.rotate((3 * Math.PI) / 2);
    context.beginPath();
    context.arc(0, 0, size / 3, 0, sec * (Math.PI / 30), false);
    context.stroke();
    context.restore();

    // draw arc - minutes
    context.save();
    context.rotate((3 * Math.PI) / 2);
    context.beginPath();
    context.arc(
      0,
      0,
      size / 3.5,
      0,
      min * (Math.PI / 30) + sec * ((2 * Math.PI) / (60 * 60)),
      false
    );
    context.stroke();
    context.restore();

    // draw arc - hours
    context.save();
    context.rotate((3 * Math.PI) / 2);
    context.beginPath();
    context.arc(
      0,
      0,
      size / 5,
      0,
      hour * (Math.PI / 6) + min * (Math.PI / 30) * 0.1,
      false
    );
    context.stroke();
    context.restore();

    // draw hand - milliseconds
    context.save();
    context.beginPath();
    context.moveTo(0, 0);
    context.rotate(msec * ((2 * Math.PI) / 1000));
    context.lineTo(0, -size / 2.2);
    context.stroke();
    context.restore();

    // draw hand - seconds
    context.save();
    context.beginPath();
    context.moveTo(0, 0);
    context.rotate(sec * (Math.PI / 30));
    context.lineTo(0, -size / 3);
    context.stroke();
    context.restore();

    // draw hand - minutes
    context.save();
    context.beginPath();
    context.moveTo(0, 0);
    context.rotate(min * (Math.PI / 30) + sec * ((2 * Math.PI) / (60 * 60)));
    context.lineTo(0, -size / 3.5);
    context.stroke();
    context.restore();

    // draw hand - hour
    context.save();
    context.beginPath();
    context.moveTo(0, 0);
    context.rotate(hour * (Math.PI / 6) + min * (Math.PI / 30) * 0.1),
      context.lineTo(0, -size / 5);
    context.stroke();
    context.restore();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
