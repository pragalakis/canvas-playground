const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;
    const fontSize = 0.5;

    // grid
    for (let j = 0.3 + fontSize; j < height + fontSize; j++) {
      for (let i = 0; i < width; i += fontSize) {
        // text
        context.font = `${fontSize - Math.cos(i) * 0.1}pt Monospace`;
        context.fillText('A', i, j);

        // cosine line
        context.beginPath();
        context.moveTo(i, j - fontSize + Math.cos(i) * 0.1);
        context.lineTo(
          i + fontSize,
          j - fontSize + Math.cos(i + fontSize) * 0.1
        );
        context.stroke();

        // staight line
        context.beginPath();
        context.moveTo(i, j);
        context.lineTo(i + fontSize, j);
        context.stroke();
      }
    }
  };
};
canvasSketch(sketch, settings);
