const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background color
    context.fillStyle = 'hsl(100,25%,50%)';
    context.fillRect(0, 0, width, height);

    const bytes = 4;

    // wiki text
    let text = `Leonardo da Vinci is thought by some to have begun painting the Mona Lisa in 1503 or 1504 in Florence, Italy. Although the Louvre states that it was "doubtless painted between 1503 and 1506", the art historian Martin Kemp says there are some difficulties in confirming the actual dates with certainty. In addition, many Leonardo experts, such as Carlo Pedretti and Alessandro Vezzosi, are of the opinion that the painting is characteristic of Leonardo’s style in the final years of his life, post-1513. Other academics argue that, given the historical documentation, Leonardo would have painted the work from 1513. According to Leonardo's contemporary, Giorgio Vasari, "after he had lingered over it four years, he left it unfinished". Leonardo, later in his life, is said to have regretted "never having completed a single work".
Circa 1504, Raphael executed a pen and ink sketch, today in the Louvre museum, in which the subject is flanked by large columns. Experts universally agree it is based on Leonardo’s portrait of Mona Lisa. Other later copies of the Mona Lisa, such as those in the National Museum of Art, Architecture and Design in Oslo and The Walters Art Museum in Baltimore, also display large flanking columns. As a result, it was originally thought that the Mona Lisa in the Louvre had side columns and had been cut. However, as early as 1993, Zöllner observed that the painting surface had never been trimmed. This was confirmed through a series of tests conducted in 2004. In view of this, Vincent Delieuvin, curator of 16th-century Italian painting at the Louvre museum states that the sketch and these other copies must have been inspired by another version, while Frank Zöllner states that the sketch brings up the possibility that Leonardo executed another work on the subject of Mona Lisa.
It is unclear as to who commissioned the painting. Vasari states that the work was painted for Francesco del Giocondo, the husband of Lisa del Giocondo. However, Antonio de Beatis, following a visit with Leonardo in 1517, records that the painting was executed at the instance of Giuliano di Lorenzo de' Medici. 
In 1516, Leonardo was invited by King François I to work at the Clos Lucé near the king's castle in Amboise. It is believed that he took the Mona Lisa with him and continued to work after he moved to France. Art historian Carmen C. Bambach has concluded that Leonardo probably continued refining the work until 1516 or 1517.
The fate of the painting around Leonardo’s death and just after it has divided academic opinion. Some, such as Kemp, believe that upon Leonardo’s death, the painting was inherited with other works by his pupil and assistant Salaì and was still in the latter’s possession in 1525. Others believe that the painting was sold to Francis I by Salaì, together with The Virgin and Child with St. Anne and the St. John the Baptist in 1518. The Louvre Museum lists the painting as having entered the Royal collection in 1518.
Given the issue surrounding the dating of the painting, the presence of the flanking columns in the Raphael sketch, the uncertainty concerning the person who commissioned it and its fate around the time of Leonardo’s death, a number of experts have argued that Leonardo painted two versions of the Mona Lisa. The first would have been commissioned by Francesco del Giocondo circa 1503, had flanking columns, have been left unfinished and have been in Salaì’s possession in 1525. The second, commissioned by Giuliano de' Medici circa 1513, without the flanking columns, would have been sold by Salaì to Francis I in 1518 and be the one in the Louvre today.
The painting was kept at the Palace of Fontainebleau, where it remained until Louis XIV moved the painting to the Palace of Versailles. After the French Revolution, it was moved to the Louvre, but spent a brief period in the bedroom of Napoleon in the Tuileries Palace. 
During the Franco-Prussian War (1870–71) it was moved from the Louvre to the Brest Arsenal. During World War II, Mona Lisa was again removed from the Louvre and taken safely, first to Château d'Amboise, then to the Loc-Dieu Abbey and Château de Chambord, then finally to the Ingres Museum in Montauban. In December 2015, it was reported that French scientist Pascal Cotte had found a hidden portrait underneath the surface of the painting using reflective light technology. The portrait is an underlying image of a model looking off to the side. Having been given access to the painting by Louvre in 2004, Cotte spent ten years using layer amplification methods to study the painting. According to Cotte, the underlying image is Leonardo's original Mona Lisa. 
However, this portrait does not fit with the description of the painting in the historical records: Both Vasari and Gian Paolo Lomazzo describe the subject as smiling; the subject in Cotte’s portrait displays no smile. In addition, the portrait lacks the flanking columns drawn by Raphael in his c.1504 sketch of Mona Lisa. Moreover, Cotte admits that his reconstitution had been carried out only in support of his hypotheses and should not be considered a real painting; he stresses that the images never existed. Kemp is also adamant that Cotte’s images in no way establish the existence of a separate underlying portrait.`;

    // font size so the given text will fit the whole screen
    let fontSize = Math.sqrt((width * height) / text.length);
    // offset font size fixing
    fontSize = Math.sqrt(
      (width * height) / text.length - (width * fontSize) / text.length
    );
    context.font = `${fontSize}px Arial`;

    let letterIndex = 0;
    for (let i = fontSize; i < height + fontSize; i += fontSize) {
      for (let j = 0; j < width; j += fontSize) {
        const letter = text[letterIndex];
        if (letter == undefined) continue;

        const colorI = Math.floor(i) * (width * bytes) + Math.floor(j) * bytes;

        if (data[colorI] <= 60) {
          context.fillStyle = 'hsl(0,0%,0%)';
        } else {
          context.fillStyle = 'hsl(0,0%,100%)';
        }

        context.fillText(letter, j, i);
        letterIndex++;
      }
    }
  };
});
