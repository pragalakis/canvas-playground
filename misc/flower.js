const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.01;
    context.strokeStyle = 'red';

    const total = 600;
    const zoom = 45;
    const angle = 55.6230589875 * (Math.PI / 180);
    let j = 0;
    context.beginPath();
    context.moveTo(width / 2, height / 2);
    for (let i = 0; i < total; i++) {
      context.lineTo(
        width / 2 + j * Math.sin(i * angle),
        height / 2 + j * Math.cos(i * angle)
      );
      context.stroke();
      j = i / zoom;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
