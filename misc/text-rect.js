const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    const color = ['black', 'white'];
    const alphabet = ['A', 'Ω', '0', '1', 'S', 'Z'];
    let x = 0;
    let fontSize = 10;
    let colorIndex = 0;
    let letterIndex = 0;
    let linewidth = 0.3;

    for (let i = 0; i < 10; i++) {
      context.font = `${fontSize}pt arial`;
      context.lineWidth = linewidth - i * 0.1;

      // top left rect
      colorIndex = colorIndex >= color.length - 1 ? 0 : colorIndex + 1;
      context.fillStyle = color[colorIndex];
      context.fillRect(x, x, width / 2 - x, height / 2 - x);

      // bottom right rect
      context.fillRect(width / 2, height / 2, width / 2 - x, height / 2 - x);

      // top right rect
      colorIndex = colorIndex >= color.length - 1 ? 0 : colorIndex + 1;
      context.fillStyle = color[colorIndex];
      context.fillRect(width / 2, x, width / 2 - x, height / 2 - x);

      // bottom left rect
      context.fillRect(0 + x, height / 2, width / 2 - x, height / 2 - x);

      if (i < 3) {
        context.strokeStyle = color[colorIndex];
        // top left letter
        letterIndex = letterIndex >= alphabet.length - 1 ? 0 : letterIndex + 1;
        let letter = alphabet[letterIndex];
        context.strokeText(letter, x + 0.2, x + fontSize);

        // bottom right letter
        letterIndex = letterIndex >= alphabet.length - 1 ? 0 : letterIndex + 1;
        letter = alphabet[letterIndex];
        let letterLength = context.measureText(letter).width;
        context.strokeText(letter, width - x - letterLength, height - x - 0.7);
      }

      colorIndex = colorIndex >= color.length - 1 ? 0 : colorIndex + 1;

      if (i < 3) {
        context.strokeStyle = color[colorIndex];
        // top right letter
        letterIndex = letterIndex >= alphabet.length - 1 ? 0 : letterIndex + 1;
        let letter = alphabet[letterIndex];
        let letterLength = context.measureText(letter).width;
        context.strokeText(letter, width - letterLength - x, x + fontSize);

        // bottom left letter
        letterIndex = letterIndex >= alphabet.length - 1 ? 0 : letterIndex + 1;
        letter = alphabet[letterIndex];
        context.strokeText(letter, x + 0.2, height - x - 0.7);
      }

      let remWidth = width / 2 - x;
      x = x + remWidth * 0.35;
      fontSize -= 4;
    }

    // border
    context.lineWidth = 0.7;
    context.strokeStyle = 'black';
    context.strokeRect(0, 0, width, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
