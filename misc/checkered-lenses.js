const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  //pixelsPerInch: 72,
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    function checkered(x, y, w, h, size, colors) {
      let cIndex = 0;
      for (let j = x; j < h; j += size) {
        cIndex = cIndex >= colors.length - 1 ? 0 : cIndex + 1;
        for (let i = y; i < w; i += size) {
          context.fillStyle = colors[cIndex];
          context.fillRect(i, j, size, size);
          cIndex = cIndex >= colors.length - 1 ? 0 : cIndex + 1;
        }
      }
    }

    let w = width - 200;
    let h = height - 200;
    let tile = 65;
    // checkered tiles
    while (w > 200 && h > 200) {
      checkered(height - h, width - w, w, h, tile, ['black', 'white']);
      h -= 400;
      w -= 400;
      tile = tile / 1.8;
    }

    // arcs
    context.beginPath();
    context.fillStyle = 'rgba(255,0,0,0.3)';
    context.arc(width / 2, height / 2 - 730, 350, 0, Math.PI * 2, true);
    context.fill();

    context.beginPath();
    context.fillStyle = 'rgba(255,255,0,0.3)';
    context.arc(width / 2, height / 2 - 420, 350, 0, Math.PI * 2, true);
    context.fill();

    context.beginPath();
    context.fillStyle = 'rgba(255,0,255,0.3)';
    context.arc(width / 2, height / 2 - 130, 350, 0, Math.PI * 2, true);
    context.fill();

    context.beginPath();
    context.fillStyle = 'rgba(0,0,255,0.3)';
    context.arc(width / 2, height / 2 + 180, 350, 0, Math.PI * 2, true);
    context.fill();

    context.beginPath();
    context.fillStyle = 'rgba(0,255,255,0.3)';
    context.arc(width / 2, height / 2 + 480, 350, 0, Math.PI * 2, true);
    context.fill();

    context.beginPath();
    context.fillStyle = 'rgba(0,255,0,0.3)';
    context.arc(width / 2, height / 2 + 730, 350, 0, Math.PI * 2, true);
    context.fill();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
