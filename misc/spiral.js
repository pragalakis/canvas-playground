const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.03;
    context.strokeStyle = 'black';

    let N = 9000;
    let data = Array(N)
      .fill(0)
      .map((v, i) => {
        let x = Math.sin(i) * i;
        let y = Math.cos(i) * i;
        return [0.003 * x, 0.003 * y];
      });

    data.forEach(v => {
      context.beginPath();
      context.arc(
        width / 2 + v[0],
        height / 2 + v[1],
        0.03,
        0,
        Math.PI * 2,
        true
      );
      context.stroke();
    });
  };
};
// Start the sketch
canvasSketch(sketch, settings);
