const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    context.translate(5, 15);

    const bytes = 4;
    let step = 40;
    let rad = 15;

    for (let j = step; j < width - step; j += step) {
      for (let i = step; i < height - step; i += step) {
        // get color indexes
        let colorIndex = i * (width * bytes) + j * bytes;

        let r = data[colorIndex];
        let g = data[colorIndex + 1];
        let b = data[colorIndex + 2];
        let a = data[colorIndex + 3];

        context.fillStyle = `rgba(${r},${g},${b},${a})`;

        context.beginPath();
        context.arc(j, i, rad, 0, 2 * Math.PI, true);
        context.fill();
      }
    }
  };
});
