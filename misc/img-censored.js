const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    /// pixeled area
    const bytes = 4;
    const size = 15; // pixel size
    for (let j = 210; j < 260; j += size) {
      for (let i = 240; i < 400; i += size) {
        // color
        const colorIndex = j * (width * bytes) + i * bytes;
        let r = data[colorIndex];
        let g = data[colorIndex + 1];
        let b = data[colorIndex + 2];
        let a = data[colorIndex + 3];
        context.fillStyle = `rgb(${r},${g},${b})`;

        // draw pixel
        context.fillRect(i, j, size, size);
      }
    }
  };
});
