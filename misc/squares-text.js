const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.1;

    const text1 = 'out';
    const text2 = 'in';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;
    const text1Width = context.measureText(text1).width;
    const text2Width = context.measureText(text2).width;

    const size = 10;
    const offset = 2;

    // left square 'out' text
    const x1 = width / 2 - size + offset;
    const y1 = height / 2 - size + offset;
    for (let j = y1 + 0.6; j <= y1 + size; j += fontSize + 0.2) {
      for (
        let i = x1 + 0.25;
        i < x1 + size - text1Width;
        i += text1Width + 0.2
      ) {
        context.fillText(text1, i, j);
      }
    }

    // right square 'out' text
    const x2 = width / 2 - offset;
    const y2 = height / 2 - offset;
    for (let j = y2 + 0.6; j <= y2 + size; j += fontSize + 0.2) {
      for (
        let i = x2 + 0.25;
        i < x2 + size - text1Width;
        i += text1Width + 0.2
      ) {
        context.fillText(text1, i, j);
      }
    }

    // 'in' text
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(x2, y2, 2 * offset, 2 * offset);

    context.fillStyle = 'hsl(0, 0%, 6%)';
    for (let j = y2 + 0.7; j <= y2 + 2 * offset; j += fontSize + 0.1) {
      for (let i = x2 + 0.1; i < x2 + 2 * offset; i += text2Width + 0.2) {
        context.fillText(text2, i, j);
      }
    }

    // square borders
    context.strokeRect(x1, y1, size, size);
    context.strokeRect(x2, y2, size, size);
  };
};
canvasSketch(sketch, settings);
