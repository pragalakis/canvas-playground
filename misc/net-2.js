const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const margin = 2;
    const step = 0.5;

    // vertical lines
    for (let j = margin; j < height - margin; j += step) {
      for (let i = margin; i < width - margin; i += step) {
        context.beginPath();
        context.moveTo(i + Math.cos(j / 4), j + Math.cos(i / 4));
        context.lineTo(
          i + step + Math.cos((j + step) / 4),
          j + Math.cos((i + step) / 4)
        );
        context.stroke();
      }
    }

    // horizontal lines
    for (let j = margin + step; j < height - margin; j += step) {
      for (let i = margin; i < width - margin + step; i += step) {
        context.beginPath();
        context.moveTo(i + Math.cos(j / 4), j + Math.cos(i / 4));
        context.lineTo(
          i + Math.cos((j - step) / 4),
          j - step + Math.cos((i + step) / 4)
        );
        context.stroke();
      }
    }
  };
};
canvasSketch(sketch, settings);
