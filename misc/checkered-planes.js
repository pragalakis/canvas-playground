const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    function checkered(x, y, w, h, size, colors) {
      let cIndex = 0;
      for (let j = x; j < h; j += size) {
        cIndex = cIndex >= colors.length - 1 ? 0 : cIndex + 1;
        for (let i = y; i < w; i += size) {
          context.fillStyle = colors[cIndex];
          context.fillRect(i, j, size, size);
          cIndex = cIndex >= colors.length - 1 ? 0 : cIndex + 1;
        }
      }
    }

    checkered(0, 0, width, height, 120, ['orange', 'black']);
    checkered(600, 600, width - 600, height - 600, 65, ['black', 'white']);
    checkered(400, 400, width - 1200, height - 1200, 30, ['orange', 'black']);
    checkered(200, 200, width - 1800, height - 1800, 13, ['black', 'white']);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
