const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  fps: 10,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 7%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 96%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;

    const drawText = (text, rad) => {
      // calculate letter position points
      const points = Array(text.length)
        .fill()
        .map((v, i) => {
          const phi = (i * Math.PI * 2) / text.length;
          const x = Math.cos(phi);
          const y = Math.sin(phi);
          return [rad * x, rad * y, phi];
        });

      // draw letters
      let indx = 0;
      points.forEach(v => {
        const letter = text[indx];
        const x = width / 2 + v[0];
        const y = height / 2 + v[1];

        context.save();
        context.translate(x + fontSize / 2, y - fontSize / 2);
        // alternative you can rotate according
        // to the center of the circle with Math.atan2(midy-y,midx-x)
        context.rotate(-(3 * Math.PI) / 2 + v[2]);
        context.translate(-x - fontSize / 2, -y + fontSize / 2);
        context.fillText(letter, x, y);
        context.restore();

        indx = indx >= text.length - 1 ? 0 : indx + 1;
      });
    };

    // animate rotation
    context.save();
    context.translate(width / 2, height / 2);
    context.rotate(-0.1 * time * Math.PI);
    context.translate(-width / 2, -height / 2);
    // calling the function for different text and radius each time
    drawText('IF I ONLY HAD A TIME MACHINE ', 10);
    drawText('I WOULD JUMP RIGHT IN ', 8);
    drawText('GOING BACK IN TIME ', 6);
    drawText('IS DOPE ', 4);
    context.restore();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
