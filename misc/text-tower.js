const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.3;
    const lineH = fontSize * 1.1;
    context.font = `${fontSize}pt Monospace`;

    const textW = context.measureText('A').width;
    const margin = 2;

    const drawText = (x, y, length, rotate) => {
      for (let j = y; j < length; j += fontSize + lineH) {
        context.save();

        // rotation
        if (rotate) {
          context.translate(width / 2, height / 2);
          context.scale(-1, 1);
          context.translate(-width / 2, -height / 2);
        }

        context.fillText('A', x, j);
        context.restore();
      }

      // recursion
      if (length > margin) {
        return drawText(
          x - textW - 0.2,
          y + fontSize + lineH,
          length - fontSize - lineH,
          rotate ? true : false
        );
      } else {
        return;
      }
    };

    // draw
    let x = width / 2;
    let y = margin + fontSize / 2 + lineH;
    let length = height / 2;
    drawText(x - 0.5, y, length, false);

    y = height / 2 + fontSize / 2 + lineH;
    length = height - margin;
    drawText(x - 0.5, y, length, true);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
