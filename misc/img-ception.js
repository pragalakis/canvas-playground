const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'px'
};

// Artwork function
const sketch = async () => {
  const image = await load('/space_cats.png');
  return ({ context, width, height }) => {
    // draw image and get data
    context.drawImage(image, 0, 0, width, height);
    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    const bytes = 4;
    const size = 25;
    // draw picture grid
    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        let colorIndx = j * (width * bytes) + i * bytes;

        // colors on each step
        const r = data[colorIndx];
        const g = data[colorIndx + 1];
        const b = data[colorIndx + 2];

        // draw image
        context.drawImage(image, i, j, size, size);

        // colored tile (of the original sized image)
        context.fillStyle = `rgba(${r},${g},${b},0.5)`;
        context.fillRect(i, j, size, size);
      }
    }

    // alternative
    //context.globalAlpha = 0.5;
    //context.drawImage(image, 0, 0, width, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
