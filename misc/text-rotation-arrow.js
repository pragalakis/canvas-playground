const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.25;
    context.font = `${fontSize}pt Monospace`;
    const text = 'ARROW-';

    const drawArrow = (x, y, size) => {
      let indx = 0;

      // main arrow branch
      for (let i = x; i < x + size - fontSize; i += fontSize) {
        // draw a char of the text on each iteration
        context.fillText(text[indx], i, y + fontSize);
        indx = indx >= text.length - 1 ? 0 : indx + 1;
      }

      //arrow pointer
      context.save();
      // rotation
      context.translate(x + size, y);
      context.rotate((3 * Math.PI) / 2 - Math.PI / 4);
      context.translate(-x - size, -y);
      indx = 0;
      for (let i = x + size; i < x + 1.5 * size; i += fontSize) {
        // draw a char of the text on each iteration
        context.fillText(text[indx], i, y);
        indx = indx >= text.length - 1 ? 0 : indx + 1;
      }
      context.restore();

      //arrow pointer
      context.save();
      // rotation
      context.translate(x + size, y);
      context.rotate((3 * Math.PI) / 4);
      context.translate(-x - size, -y);
      indx = 0;
      for (let i = x + size; i < x + 1.5 * size; i += fontSize) {
        // draw a char of the text on each iteration
        context.fillText(text[indx], i, y);
        indx = indx >= text.length - 1 ? 0 : indx + 1;
      }
      context.restore();
    };

    // calling the drawArrow function on each tile
    // and rotating in with a different angle
    const size = 3;
    let angle = 0;
    for (let j = 0; j <= height; j += size) {
      for (let i = 0; i < width; i += size) {
        context.save();
        context.translate(i + size / 2, j);
        context.rotate(angle / (Math.PI * 2));
        context.translate(-i - size / 2, -j);
        drawArrow(i, j, 0.8 * size);
        context.restore();
        angle += 0.1;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
