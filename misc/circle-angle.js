const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(49, 84.6%, 96.8%)';
    context.fillRect(0, 0, width, height);

    // fixing offset
    context.translate(0.2, 0.9);

    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    function circle(size, x, y) {
      context.beginPath();
      context.arc(x, y, size, 0, Math.PI * 2, true);
      context.stroke();
    }

    const size = 0.6;
    const margin = 2 * size;

    const midx = width / 2;
    const midy = height / 2;

    for (let j = margin; j < height - margin; j = j + size + margin) {
      for (let i = margin; i < width - margin; i = i + size + margin) {
        context.save();
        context.translate(i, j);
        // where atan2 is the center where everything points to
        context.rotate(Math.atan2(midy - j, midx - i));
        context.translate(-i, -j);

        // circle and sub-circles
        for (let z = 0; z < 1; z += 0.15) {
          circle(size - size * z, i + z * 0.7, j);
        }

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
