const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.1;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;
    const size = 18;

    // draw square
    context.strokeRect(width / 2 - size / 2, height / 2 - size / 2, size, size);

    const text1 = 'L    R';
    const text2 = 'LADDER';
    const twidth = context.measureText(text2).width;

    let indx = 2;
    for (
      let i = height / 2 - size / 2 + fontSize;
      i < height / 2 + size / 2;
      i += fontSize + 0.1
    ) {
      let txt;
      if (indx % 3 === 0) {
        txt = text2;
      } else {
        txt = text1;
      }
      context.fillText(txt, width / 2 - twidth / 2, i);
      indx++;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
