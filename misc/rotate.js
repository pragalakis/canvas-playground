const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 95%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = `rgba(0, 0, 255, 0.5)`;
    context.lineWidth = 0.05;

    function rects(size, t) {
      context.translate(width / 2, height / 2);
      context.rotate(t * (Math.PI / 180));
      context.translate(-width / 2, -height / 2);
      context.strokeRect(
        width / 2 - size / 2,
        height / 2 - size / 2,
        size,
        size
      );

      if (t > 0) {
        rects(size, t - 1);
      }
    }

    let times = 20;
    for (let i = 0; i <= times; i++) {
      rects(i, times / 5);
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
