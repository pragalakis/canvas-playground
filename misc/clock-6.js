const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.strokeStyle = 'hsl(0, 0%, 8%)';
    context.lineWidth = 0.1;

    // draw frame
    let rad = 9.1;
    context.beginPath();
    context.arc(width / 2, height / 2, rad, 0, Math.PI * 2, true);
    context.stroke();

    // time
    const d = new Date();
    const hour = (d.getHours() + 24) % 12 || 12;
    const min = d.getMinutes();
    const sec = d.getSeconds();

    context.lineWidth = 0.1;
    context.translate(width / 2, height / 2);

    // draw arc - minutes
    context.save();
    context.rotate((3 * Math.PI) / 2);
    context.beginPath();
    context.arc(
      0,
      0,
      rad,
      0,
      min * (Math.PI / 30) + sec * ((2 * Math.PI) / (60 * 60)),
      false
    );
    context.lineTo(0, 0);
    context.lineTo(rad, 0);
    context.fill();
    context.restore();

    // draw hand - hour
    const hourAngle =
      (hour - 3) * ((2 * Math.PI) / 12) + min * ((2 * Math.PI) / (12 * 60));
    context.globalCompositeOperation = 'difference';
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.beginPath();
    context.moveTo(
      (Math.cos(hourAngle) * rad) / 1.3,
      (Math.sin(hourAngle) * rad) / 1.3
    );
    context.lineTo(Math.cos(hourAngle) * rad, Math.sin(hourAngle) * rad);
    context.stroke();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
