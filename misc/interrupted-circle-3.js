const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(150, 50%, 50%)';

    // fixing offset
    context.translate(-2, 0);

    const lineWidth = 1;
    const lineAngle = 1;
    let rad = 10;
    while (rad > 0) {
      // draw arcs
      context.beginPath();
      context.arc(
        width / 2 + 2 * lineAngle + rad / 4,
        height / 2 - lineWidth / 2,
        rad,
        0,
        -Math.PI / 1.5,
        true
      );

      context.stroke();
      context.arc(
        width / 2 + 2 * lineAngle + rad / 6,
        height / 2 + lineWidth / 2,
        rad,
        -Math.PI / 1.5,
        Math.PI,
        true
      );

      context.stroke();
      context.arc(
        width / 2 + 2 * lineAngle - rad / 8,
        height / 2 + lineWidth + lineWidth / 2,
        rad,
        Math.PI,
        2 * Math.PI,
        true
      );
      context.closePath();
      context.stroke();

      rad -= 0.2;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
