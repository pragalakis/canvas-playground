const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // fix offset
    context.translate(0.1, 0.5);

    // points of each circle
    const circle = (x0, y0, radX, radY) => {
      const points = Array(50)
        .fill(0)
        .map((v, i) => {
          let phi = i + Math.PI * 2;
          let x = radX * Math.cos(phi);
          let y = radY * Math.sin(phi);
          return [x0 + x, y0 + y];
        });
      return points;
    };

    let size = 1;
    let phi = 1;
    let offset = size / 4;
    let offsetY = size * Math.abs(Math.cos(phi));
    for (let j = offset; j < height; j = j + offsetY) {
      phi = phi + 1 / 30;
      let radY = Math.abs(Math.cos(phi));
      let radX = size / 2;
      offsetY = size * 2.5 * radY;
      for (let i = size; i < width - size; i = i + size + offset) {
        const points = circle(i, j, radX, radY);
        // draw circle
        context.beginPath();
        context.moveTo(points[0][0], points[0][1]);
        points.forEach(v => {
          context.lineTo(v[0], v[1]);
        });
        context.closePath();

        // gradient-wave-color-magic
        let red = Math.floor(
          Math.abs(Math.sin((Math.sin(i) + j) / 10 + (Math.sin(j) + i) / 10)) *
            255
        );
        context.fillStyle = `rgba(${red},0,170,1)`;

        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
