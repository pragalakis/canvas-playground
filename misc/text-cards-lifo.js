const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.1;

    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;
    const size = 10; // square size
    const text = [
      'one',
      'two',
      'three',
      'four',
      'five',
      'six',
      'seven',
      'eight',
      'nine',
      'ten'
    ];

    // LI-FO
    text.forEach((t, i) => {
      const x = width / 2 - 1.2 * size + i * 1.5;
      const y = height / 2 - 1.2 * size + i * 1.5;

      context.save();
      // draw and clip the square
      context.fillStyle = 'hsl(0, 0%, 98%)';
      context.beginPath();
      context.rect(x, y, size, size);
      context.clip();
      context.fill();
      context.stroke();

      // draw text grid on the cliped square
      const tWidth = context.measureText(t).width;
      context.fillStyle = 'hsl(0, 0%, 6%)';
      for (let j = y + 0.2 + fontSize; j < y + size; j += fontSize + 0.2) {
        for (let i = x + 0.2; i < x + size; i += tWidth + 0.2) {
          context.fillText(t, i, j);
        }
      }

      context.restore();
    });
  };
};
canvasSketch(sketch, settings);
