const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.1;

    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.beginPath();
    context.arc(width / 2 - 0.5, height / 2 - 1, 13, 0, Math.PI * 2, true);
    context.fill();

    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.beginPath();
    context.arc(width / 2, height / 2, 10, 0, Math.PI * 2, true);
    context.fill();

    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.beginPath();
    context.arc(width / 2 + 0.5, height / 2 - 0.5, 8, 0, Math.PI * 2, true);
    context.fill();

    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.beginPath();
    context.arc(width / 2 - 0.5, height / 2 - 0.5, 6, 0, Math.PI * 2, true);
    context.fill();

    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.beginPath();
    context.arc(width / 2 + 0.5, height / 2 + 0.5, 4, 0, Math.PI * 2, true);
    context.fill();

    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.beginPath();
    context.arc(width / 2 + 1, height / 2 - 0.5, 2, 0, Math.PI * 2, true);
    context.fill();

    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.beginPath();
    context.arc(width / 2 + 1, height / 2 - 0.5, 1, 0, Math.PI * 2, true);
    context.fill();

    // draw line pattern
    const N = 10;
    const points = pointsOnCircle(N);
    points.forEach((v, i) => {
      context.strokeStyle = 'hsl(0, 0%, 96%)';
      context.beginPath();
      context.moveTo(width / 2 + v.x * 13 - 0.5, height / 2 + v.y * 13 - 1);
      context.lineTo(width / 2 + v.x * 10, height / 2 + v.y * 10);
      context.stroke();

      context.strokeStyle = 'hsl(0, 0%, 6%)';
      context.beginPath();
      context.moveTo(width / 2 + v.x * 10, height / 2 + v.y * 10);
      context.lineTo(width / 2 + v.x * 8 + 0.5, height / 2 + v.y * 8 - 0.5);
      context.stroke();

      context.strokeStyle = 'hsl(0, 0%, 96%)';
      context.beginPath();
      context.moveTo(width / 2 + v.x * 8 + 0.5, height / 2 + v.y * 8 - 0.5);
      context.lineTo(width / 2 + v.x * 6 - 0.5, height / 2 + v.y * 6 - 0.5);
      context.stroke();

      context.strokeStyle = 'hsl(0, 0%, 6%)';
      context.beginPath();
      context.moveTo(width / 2 + v.x * 6 - 0.5, height / 2 + v.y * 6 - 0.5);
      context.lineTo(width / 2 + v.x * 4 + 0.5, height / 2 + v.y * 4 + 0.5);
      context.stroke();

      context.strokeStyle = 'hsl(0, 0%, 96%)';
      context.beginPath();
      context.moveTo(width / 2 + v.x * 4 + 0.5, height / 2 + v.y * 4 + 0.5);
      context.lineTo(width / 2 + v.x * 2 + 1, height / 2 + v.y * 2 - 0.5);
      context.stroke();

      context.strokeStyle = 'hsl(0, 0%, 6%)';
      context.beginPath();
      context.moveTo(width / 2 + v.x * 2 + 1, height / 2 + v.y * 2 - 0.5);
      context.lineTo(width / 2 + v.x + 1, height / 2 + v.y - 0.5);
      context.stroke();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
