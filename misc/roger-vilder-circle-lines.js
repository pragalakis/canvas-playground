const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const rad = 2.4;

    // line grid
    const lines = (x, y) => {
      for (let j = y - rad; j < y + rad; j += 0.4) {
        for (let i = x - rad; i < x + rad; i += 0.4) {
          context.beginPath();
          context.moveTo(i, j);
          context.lineTo(i + rad * 2, j + rad * 2);
          context.stroke();
        }
      }
    };

    // circle grid
    for (let j = 1.6 * rad; j < height - rad; j += 1.6 * rad) {
      for (let i = 1.4 * rad; i < width - rad; i += 1.6 * rad) {
        context.save();

        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.clip();
        lines(i, j);
        context.stroke();

        context.restore();
      }
    }
  };
};
canvasSketch(sketch, settings);
