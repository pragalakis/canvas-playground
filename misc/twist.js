const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    // cliping the drawing area
    const margin = 1.5;
    context.rect(margin, margin, width - 2 * margin, height - 2 * margin);
    context.stroke();
    context.clip();

    // draw line pattern
    const N = 200;
    //const N = 100;
    //const N = 50;
    //const N = 25;
    //const N = 10;
    const points = pointsOnCircle(N);
    points.forEach((v, i) => {
      context.beginPath();
      context.moveTo(width / 2 + v.x * 28 - 2, height / 2 + v.y * 28 + 2);
      context.lineTo(width / 2 + v.x * 20 - 2, height / 2 + v.y * 20 - 2);
      context.stroke();

      context.beginPath();
      context.moveTo(width / 2 + v.x * 20 - 2, height / 2 + v.y * 20 - 2);
      context.lineTo(width / 2 + v.x * 15 + 2, height / 2 + v.y * 15 - 2);
      context.stroke();

      context.beginPath();
      context.moveTo(width / 2 + v.x * 15 + 2, height / 2 + v.y * 15 - 2);
      context.lineTo(width / 2 + v.x * 10, height / 2 + v.y * 10);
      context.stroke();

      context.beginPath();
      context.moveTo(width / 2 + v.x * 10, height / 2 + v.y * 10);
      context.lineTo(width / 2 + v.x * 5 + 1, height / 2 + v.y * 5 + 1);
      context.stroke();

      context.beginPath();
      context.moveTo(width / 2 + 1 + v.x * 5, height / 2 + 1 + v.y * 5);
      context.lineTo(width / 2 + v.x * 2.5 - 1, height / 2 + v.y * 2.5 + 1);
      context.stroke();

      context.beginPath();
      context.moveTo(width / 2 + v.x * 2.5 - 1, height / 2 + v.y * 2.5 + 1);
      context.lineTo(width / 2 - 1, height / 2 + 1);
      context.stroke();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
