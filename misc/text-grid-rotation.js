const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.font = '0.5pt Monospace';

    const drawTextGrid = () => {
      const txt = "IT'S OK";
      const tw = context.measureText(txt).width;
      const margin = 5;
      const size = (width - 2 * margin) / 3;
      for (let j = margin / 2; j < width - margin; j += size) {
        for (let i = margin / 2; i < width - margin; i += size) {
          // draw text
          for (let y = j; y < j + size - 0.7; y += 0.7) {
            for (let x = i - 0.5; x < i + size - 1; x += tw + 0.5) {
              context.fillText(txt, x + 0.5, y + 0.7);
            }
          }
        }
      }
    };

    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.save();
    context.translate(width / 2, height / 2);
    context.rotate(0.3 * Math.PI);
    context.translate(-width / 2, -height / 2);
    context.translate(-3, 0);
    drawTextGrid();
    context.restore();

    context.fillStyle = 'hsl(0, 50%, 50%)';
    context.translate(width / 2, height / 2);
    context.rotate(-1 * Math.PI);
    context.translate(-width / 2, -height / 2);
    drawTextGrid();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
