const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    // draw frame
    context.lineWidth = 0.5;
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    let size = 22;
    context.beginPath();
    context.arc(width / 2, height / 2, size / 2, 0, Math.PI * 2, false);
    context.fill();

    // draw text-timestamp
    const timestamp = Date.now();

    // time
    const d = new Date();
    const hour = (d.getHours() + 24) % 12 || 12;
    const min = d.getMinutes();
    const sec = d.getSeconds();
    const msec = d.getMilliseconds();
    const hoursRad = size / 3;
    const minsRad = size / 10;

    // hours circle
    context.translate(
      width / 2 +
        Math.cos(
          (hour - 3) * ((2 * Math.PI) / 12) + min * ((2 * Math.PI) / (12 * 60))
        ) *
          hoursRad,
      height / 2 +
        Math.sin(
          (hour - 3) * ((2 * Math.PI) / 12) + min * ((2 * Math.PI) / (12 * 60))
        ) *
          hoursRad
    );

    // draw arc - hours
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.save();
    context.beginPath();
    context.arc(0, 0, size / 8, 0, Math.PI * 2, true);
    context.fill();
    context.restore();

    // draw arc - minutes
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.save();

    // mins circle
    context.translate(
      Math.cos(
        (min - 15) * ((2 * Math.PI) / (1 * 60)) +
          sec * ((2 * Math.PI) / (1 * 60 * 60))
      ) * minsRad,
      Math.sin(
        (min - 15) * ((2 * Math.PI) / (1 * 60)) +
          sec * ((2 * Math.PI) / (1 * 60 * 60))
      ) * minsRad
    );

    context.beginPath();
    context.arc(0, 0, size / 80, 0, Math.PI * 2, true);
    context.fill();
    context.restore();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
