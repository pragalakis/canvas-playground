const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.strokeStyle = 'white';
    context.lineWidth = 0.05;

    // must be between 0>=angle<1
    let angle = 0.6;
    function box(x, y, w, h) {
      let squareSide = w / 2;

      // left side
      context.fillStyle = '#335b9a';
      context.beginPath();
      context.moveTo(x, y - angle);
      context.lineTo(x - squareSide, y - squareSide);
      context.lineTo(x - squareSide, y + h - squareSide);
      context.lineTo(x, y + h - squareSide);
      context.lineTo(x, y - angle);
      context.fill();
      context.stroke();

      // right side
      context.fillStyle = '#f7f4cf';
      context.beginPath();
      context.moveTo(x, y - angle);
      context.lineTo(x + squareSide, y - squareSide);
      context.lineTo(x + squareSide, y + h - squareSide);
      context.lineTo(x, y + h - squareSide);
      context.lineTo(x, y - angle);
      context.fill();
      context.stroke();

      // top side
      context.fillStyle = '#8fb1c5';
      context.beginPath();
      context.moveTo(x, y - angle);
      context.lineTo(x + squareSide, y - squareSide);
      context.lineTo(x, y - squareSide * 2 + angle);
      context.lineTo(x - squareSide, y - squareSide);
      context.lineTo(x, y - angle);
      context.fill();
      context.stroke();
    }

    const w = 2;
    const h = 1.4;
    const N = Math.floor(height / h);
    let offsetY = h;
    // points of each box
    const points = Array(N)
      .fill(0)
      .map((v, j) => {
        let arr = [];
        let offsetX = w;

        let z = j % 2 == 0 ? w / 2 : 0;
        for (let i = 0; i < Math.floor(width / w); i++) {
          arr.push([offsetX + z, offsetY]);
          offsetX += w;
        }
        offsetY += h;

        return arr;
      });

    // draw boxes
    points.forEach(arr => {
      arr.forEach(v => {
        box(-0.5 + v[0], -0.5 + v[1], w, h);
      });
    });

    // borders
    const margin = 1.5;
    context.fillStyle = '#f7f4cf';
    // left
    context.fillRect(0, 0, margin, height);
    // right
    context.fillRect(width - margin, 0, margin, height);
    // top
    context.fillRect(0, 0, width, margin);
    // bottom
    context.fillRect(0, height - margin, width, margin);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
