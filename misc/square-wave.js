const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.lineCap = 'round';
    context.strokeStyle = 'hsl(0, 0%, 96%)';

    // draw
    let size = 20;
    let csize = 4;
    while (size > 0) {
      // half squares
      context.beginPath();
      context.moveTo(width / 2.5 - size / 2, height / 3);
      context.lineTo(width / 2.5 - size / 2, height / 3 - size / 2);
      context.lineTo(width / 2.5 + size / 2, height / 3 - size / 2);
      context.lineTo(width / 2.5 + size / 2, height / 3);
      context.stroke();

      // right curve, lines and bottom half square
      context.beginPath();
      context.moveTo(width / 2.5 + size / 2, height / 3);
      let i = 0;
      for (i = csize / 3; i < height - 30; i += csize) {
        context.bezierCurveTo(
          width / 2.5 + size / 2 + 2 + i / 2,
          height / 3 + i + csize / 4,
          width / 2.5 + size / 2 - 2 + i / 2,
          height / 3 + i + (3 * csize) / 4,
          width / 2.5 + size / 2 + i / 2,
          height / 3 + csize + i
        );
      }
      context.lineTo(
        width / 2.5 + size / 2 + (i - csize) / 2,
        height - height / 3 + size / 2
      );
      context.lineTo(
        width / 2.5 - size / 2 + (i - csize) / 2,
        height - height / 3 + size / 2
      );
      context.stroke();

      // left curve and lines
      context.beginPath();
      context.moveTo(width / 2.5 - size / 2, height / 3);
      i = 0;
      for (i = csize / 3; i < height - 30; i += csize) {
        context.bezierCurveTo(
          width / 2.5 - size / 2 + 2 + i / 2,
          height / 3 + i + csize / 4,
          width / 2.5 - size / 2 - 2 + i / 2,
          height / 3 + i + (3 * csize) / 4,
          width / 2.5 - size / 2 + i / 2,
          height / 3 + csize + i
        );
      }
      context.lineTo(
        width / 2.5 - size / 2 + (i - csize) / 2,
        height - height / 3 + size / 2
      );
      context.stroke();

      size -= 0.5;
    }

    // draw arcs
    let rad = 3;
    while (rad > 0) {
      context.beginPath();
      context.arc(width / 2.5 + 10, height / 3 - 10, rad, 1.55, Math.PI, true);
      context.stroke();

      context.save();
      context.translate(width / 2.5, height - height / 3 + 10);
      context.scale(-1, 1);
      context.translate(-width / 2.5, -height + height / 3 - 10);
      context.beginPath();
      context.arc(
        width / 2.5 + 5.3,
        height - height / 3 + 10,
        rad,
        -1.55,
        Math.PI,
        false
      );
      context.stroke();
      context.restore();

      rad -= 0.2;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
