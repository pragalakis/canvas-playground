const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

var colorIndex = -1;
const sketch = update => {
  let colors = ['#f1f1f1', '#eec9d2', '#f4b6c2'];
  document.body.onkeyup = function(e) {
    // pressing space changes the color
    if (e.keyCode == 32) {
      colorIndex = colorIndex >= 3 ? 0 : colorIndex + 1;
      switch (colorIndex) {
        case 0:
          // blue palette
          colors = ['#f9f9f9', '#107dac', '#189ad3'];
          break;
        case 1:
          // green palette
          colors = ['#f7f7f7', '#9DC183', '#0B6623'];
          break;
        case 2:
          // pastel palette
          colors = ['#fae3d9', '#eabcac', '#e2b091'];
          break;
        case 3:
          // starting palette
          colors = ['#f1f1f1', '#eec9d2', '#f4b6c2'];
          break;
      }
      update.render();
    }
  };

  return ({ context, width, height }) => {
    // background color
    context.fillStyle = colors[0];
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.45;

    const margin = 3;
    const w = (width - margin * 2 - 3) / 4;
    let alternate = true;

    for (let i = margin; i < width - margin; i = i + w + 1) {
      // clip rectangles
      context.save();
      context.rect(i, margin, w, height - margin * 2);
      context.clip();

      // draw lines
      for (let j = 0; j < 40; j++) {
        context.strokeStyle = alternate ? colors[1] : colors[2];
        context.beginPath();
        if (alternate) {
          context.moveTo(i - 0.3, j);
          context.lineTo(i + w + 0.3, j + 4);
        } else {
          context.moveTo(i + w + 0.3, j);
          context.lineTo(i - 0.3, j + 4);
        }
        context.stroke();
      }
      alternate = !alternate;
      context.restore();
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
