const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = '#f7f4cf';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    // 0>=angle<1
    let angle = 0.7;
    function box(x, y, w, h) {
      let squareSide = w / 2;

      // left side
      context.fillStyle = '#335b9a';
      context.beginPath();
      context.moveTo(x, y - angle);
      context.lineTo(x - squareSide, y - squareSide);
      context.lineTo(x - squareSide, y + h - squareSide);
      context.lineTo(x, y + h);
      context.lineTo(x, y - angle);
      context.fill();
      context.stroke();

      // right side
      context.fillStyle = '#f7f4cf';
      context.beginPath();
      context.moveTo(x, y - angle);
      context.lineTo(x + squareSide, y - squareSide);
      context.lineTo(x + squareSide, y + h - squareSide);
      context.lineTo(x, y + h);
      context.lineTo(x, y - angle);
      context.fill();
      context.stroke();

      // top side
      context.fillStyle = '#8fb1c5';
      context.beginPath();
      context.moveTo(x, y - angle);
      context.lineTo(x + squareSide, y - squareSide);
      context.lineTo(x, y - squareSide * 2 + angle);
      context.lineTo(x - squareSide, y - squareSide);
      context.lineTo(x, y - angle);
      context.fill();
      context.stroke();
    }

    const w = 2;
    const h = 1;
    const N = 24;
    let offsetY = h;
    const points = Array(N)
      .fill(0)
      .map((v, j) => {
        let arr = [];
        let offsetX = w;

        // generate rhombus shape
        let total = j > N / 2 ? N - j : j;
        for (let i = 0; i < total; i++) {
          arr.push([offsetX - total * (w / 2), offsetY]);
          offsetX += w;
        }

        offsetY += h;

        return arr;
      });

    points.forEach(arr => {
      arr.forEach(v => {
        box(width / 2 - w / 2 + v[0], 4 + v[1], w, h);
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
