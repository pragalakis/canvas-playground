const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,6%)';
    context.lineWidth = 0.1;

    let step = 1;
    for (let j = 0; j < height; j += step) {
      context.beginPath();
      context.moveTo(0, j);
      context.lineTo(width, j);
      context.stroke();
    }

    for (let i = 0; i < width; i += step) {
      context.beginPath();
      context.moveTo(i, height);
      context.lineTo(width, 0);
      context.lineTo(width, height);
      context.closePath();
      context.fill();
      context.stroke();
    }
  };
};
canvasSketch(sketch, settings);
