const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 96%)';

    // draw
    let rad = 8;
    const distance = rad / 2;
    const displacement = 2.66;
    while (rad > 0) {
      context.beginPath();
      context.arc(width / 2, height / 2, rad, 0, Math.PI, true);
      context.arc(
        width / 2 - displacement,
        height / 2 + distance,
        rad,
        Math.PI,
        2 * Math.PI,
        true
      );
      context.closePath();
      context.stroke();
      rad -= 0.5;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
