const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 96%)';

    let margin = 0.5;
    let rad = (width - 11 * margin) / 10 / 2;
    // circle pattern
    for (i = rad + margin; i < width; i = i + 2 * rad + margin) {
      for (j = rad + margin; j < height; j = j + 1 * rad + margin) {
        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.fill();
        rad =
          rad - rad * 0.1 > 0 && j < height / 2
            ? rad - rad * 0.1
            : rad + rad * 0.1;
      }
      rad = (width - 11 * margin) / 10 / 2;
    }

    // color settings
    context.globalCompositeOperation = 'difference';
    context.fillStyle = 'rgba(255, 255, 255, 1)';

    function rects(x, y, sizeW, sizeH) {
      context.fillRect(x, y, sizeW, sizeH);
      rad -= rad * 0.1;
      return sizeW >= 6
        ? rects(
            x + 2 * ((width - 11 * margin) / 10 / 2) + margin,
            y + 2 * rad + margin,
            sizeW - 4 * ((width - 11 * margin) / 10 / 2) - 2 * margin,
            height - 1.91 * (y + 2 * rad + 1 * margin)
          )
        : -1;
    }

    // draw rects
    rects(
      margin + rad,
      margin + rad,
      width - 2 * margin - 2 * rad,
      height - 2 * margin - 2 * rad
    );
  };
};

// Start the sketch
canvasSketch(sketch, settings);
