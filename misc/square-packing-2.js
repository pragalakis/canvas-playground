const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const data = [
    [0, 0, 1],
    [1, 0, 2],
    [3, 0, 3],
    [6, 0, 1],
    [7, 0, 1],
    [0, 1, 1],
    [6, 1, 2],
    [0, 2, 1],
    [1, 2, 1],
    [2, 2, 1],
    [0, 3, 4],
    [4, 3, 3],
    [7, 3, 1],
    [7, 4, 1],
    [7, 5, 1],
    [4, 6, 2],
    [6, 6, 2],
    [0, 7, 1],
    [1, 7, 1],
    [2, 7, 1],
    [3, 7, 1]
  ];
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;
    const size = 20;

    const size2 = 20 / 8;
    // squares
    data.forEach((v, i) => {
      let x = width / 2 - size / 2 + v[0] * size2;
      let y = height / 2 - size / 2 + v[1] * size2;
      let s = v[2] * size2;
      const margin = 0.2;

      // sub-squares
      while (s > 0) {
        context.strokeRect(x, y, s, s);
        s -= margin * 2;
        x += margin;
        y += margin;
      }
    });
  };
};
canvasSketch(sketch, settings);
