const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    const fontSize = 0.6;
    context.font = `${fontSize}pt Roboto`;
    const text = 'time';
    const tWidth = context.measureText(text).width;

    // text grid
    for (let j = fontSize; j < height + fontSize; j += fontSize + 0.1) {
      for (let i = 0; i < width; i += tWidth + 0.2) {
        // opacity
        context.fillStyle = `rgba(0,0,0,${1 - i / width})`;

        // draw text
        context.fillText(text, i, j);
      }
    }
  };
};
canvasSketch(sketch, settings);
