const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;

    const text = 'HAHA';
    let x = context.measureText(text).width;
    let y = fontSize;

    // draw-text loop
    for (let i = 0; i < width; i += x + 0.5) {
      for (let j = 0; j <= height + 2 * fontSize; j += x) {
        // horizontal text
        context.fillText(text, i, j);
        // rotated vertical text
        context.save();
        context.translate(i, j);
        context.rotate(-Math.PI / 2);
        context.translate(-i, -j);
        context.fillText(text, i, j);
        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
