const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'white';
    context.lineWidth = 0.05;

    const circle = (x, y, rad) => {
      context.beginPath();
      context.arc(x, y, rad, 0, Math.PI * 2, true);
      context.stroke();

      // recursion
      if (rad > 0.1) {
        circle(x - rad * 2, y, rad / 2);
        circle(x + rad * 2, y, rad / 2);
        // if you remove one of the following circle callings
        // you ll get a sierpinski triangle
        circle(x, y + rad * 2, rad / 2);
        circle(x, y - rad * 2, rad / 2);
      }
    };

    circle(width / 2, height / 2, 10.5);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
