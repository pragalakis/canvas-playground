const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    const bytes = 4;
    let step = 1;

    for (let j = step; j < width - step; j += step) {
      for (let i = step; i < height - step; i += step) {
        let colorIndex = i * (width * bytes) + j * bytes;

        let r = data[colorIndex];
        let g = data[colorIndex + 1];
        let b = data[colorIndex + 2];

        data[colorIndex] = 255 - r;
        data[colorIndex + 1] = 255 - g;
        data[colorIndex + 2] = 255 - b;
      }
    }
    context.putImageData(pixels, 0, 0);
  };
});
