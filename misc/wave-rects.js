const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = '#f7f4cf';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    context.translate(0, 1.3);

    const box = function(x, y, w, h) {
      // right side
      context.fillStyle = '#335b9a';
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x + w, y - w / 2);
      context.lineTo(x + w, y - h - w / 2);
      context.lineTo(x, y - h);
      context.lineTo(x, y);
      context.fill();
      context.stroke();

      // left side
      context.fillStyle = '#f7f4cf';
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x - w, y - w / 2);
      context.lineTo(x - w, y - h - w / 2);
      context.lineTo(x, y - h);
      context.lineTo(x, y);
      context.fill();
      context.stroke();

      // top side
      context.fillStyle = '#8fb1c5';
      context.beginPath();
      context.moveTo(x, y - h);
      context.lineTo(x - w, y - h - w / 2);
      context.lineTo(x, y - h - 2 * (w / 2));
      context.lineTo(x + w, y - h - w / 2);
      context.lineTo(x, y - h);
      context.fill();
      context.stroke();
    };

    const boxWidth = 1;
    const boxHeight = 0.5;
    const N = 28;
    const points = Array(N)
      .fill(0)
      .map((v, j) => {
        let arr = [];
        let offsetX = boxWidth;

        // generate rhombus shape
        let total = j > N / 2 ? N - j : j;
        for (let i = 0; i < total; i++) {
          arr.push([offsetX - total, j * 0.5]);
          offsetX += 2 * boxWidth;
        }

        return arr;
      });

    const distance = function(x, y, cx, cy) {
      return Math.sqrt((cx - x) * (cx - x) + (cy - y) * (cy - y));
    };

    const halfplane = (boxWidth * N) / 2;

    // first plane
    points.forEach(arr => {
      arr.forEach(v => {
        // wave density = 0.5
        let wave = Math.sin(distance(v[0], v[1], halfplane, halfplane) * 0.5);
        box(width / 2 + v[0], v[1] + wave, boxWidth, boxHeight);
      });
    });

    // second plane
    points.forEach(arr => {
      arr.forEach(v => {
        // wave density = 0.4.
        let wave = Math.sin(distance(v[0], v[1], halfplane, halfplane) * 0.4);
        box(width / 2 + v[0], halfplane + v[1] + wave, boxWidth, boxHeight);
      });
    });

    // third plane
    points.forEach(arr => {
      arr.forEach(v => {
        // wave density = 0.3
        let wave = Math.sin(distance(v[0], v[1], halfplane, halfplane) * 0.3);
        box(width / 2 + v[0], 2 * halfplane + v[1] + wave, boxWidth, boxHeight);
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
