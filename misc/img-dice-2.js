const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.save();
    context.filter = 'grayscale(300%)';
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;
    // clear drawed image
    context.clearRect(0, 0, width, height);
    context.restore();

    // background color
    context.fillStyle = 'hsl(0,0%,0%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'white';
    context.strokeStyle = 'white';
    context.lineWidth = 0.5;

    const bytes = 4;
    const step = 15;

    function dice(i, j, dot) {
      const rad = step / 15;
      switch (dot) {
        case 'mid':
          context.beginPath();
          context.arc(i + step / 2, j + step / 2, rad, 0, Math.PI * 2, true);
          context.fill();
          break;
        case 'top-left':
          context.beginPath();
          context.arc(
            i + step / 3.5,
            j + step / 3.5,
            rad,
            0,
            Math.PI * 2,
            true
          );
          context.fill();
          break;
        case 'bottom-right':
          context.beginPath();
          context.arc(
            i + step - step / 3.5,
            j + step - step / 3.5,
            rad,
            0,
            Math.PI * 2,
            true
          );
          context.fill();
          break;
        case 'top-right':
          context.beginPath();
          context.arc(
            i + step - step / 3.5,
            j + step / 3.5,
            rad,
            0,
            Math.PI * 2,
            true
          );
          context.fill();
          break;

        case 'bottom-left':
          context.beginPath();
          context.arc(
            i + step / 3.5,
            j + step - step / 3.5,
            rad,
            0,
            Math.PI * 2,
            true
          );
          context.fill();
          break;
        case 'mid-right':
          context.beginPath();
          context.arc(
            i + step - step / 3.5,
            j + step / 2,
            rad,
            0,
            Math.PI * 2,
            true
          );
          context.fill();
          break;
        case 'mid-left':
          context.beginPath();
          context.arc(i + step / 3.5, j + step / 2, rad, 0, Math.PI * 2, true);
          context.fill();
          break;
      }
    }

    let totalDices = 0;
    for (let j = 0; j < height; j += step) {
      for (let i = 0; i < width; i += step) {
        const colorIndex = j * (width * bytes) + i * bytes;
        const color = data[colorIndex];
        totalDices++;

        // dice border
        context.strokeRect(i, j, step, step);

        // dice dots - depending on color
        if (color < 41) {
          // one
          dice(i, j, 'mid');
        } else if (color >= 41 && color < 82) {
          // two
          dice(i, j, 'top-left');
          dice(i, j, 'bottom-right');
        } else if (color >= 82 && color < 123) {
          // three
          dice(i, j, 'top-left');
          dice(i, j, 'mid');
          dice(i, j, 'bottom-right');
        } else if (color >= 123 && color < 164) {
          // four
          dice(i, j, 'top-left');
          dice(i, j, 'top-right');
          dice(i, j, 'bottom-left');
          dice(i, j, 'bottom-right');
        } else if (color >= 164 && color < 205) {
          // five
          dice(i, j, 'top-left');
          dice(i, j, 'top-right');
          dice(i, j, 'mid');
          dice(i, j, 'bottom-left');
          dice(i, j, 'bottom-right');
        } else {
          // six
          dice(i, j, 'top-left');
          dice(i, j, 'top-right');
          dice(i, j, 'mid-left');
          dice(i, j, 'mid-right');
          dice(i, j, 'bottom-left');
          dice(i, j, 'bottom-right');
        }
      }
    }
    console.log(totalDices);
  };
});
