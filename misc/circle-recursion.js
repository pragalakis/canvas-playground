const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,92%)';
    context.fillRect(0, 0, width, height);

    const color = ['black', 'white'];

    const circle = function(x, y, rad, colorIndex) {
      context.fillStyle = `${color[colorIndex]}`;
      context.beginPath();
      context.arc(x, y, rad, 0, Math.PI * 2, true);
      context.fill();

      if (rad > 0.2) {
        colorIndex = colorIndex == 1 ? 0 : 1;
        circle(x - 1, y + 1.4, rad / 1.3, colorIndex);
      }
    };

    circle(width / 2, height / 2, 12, 0);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
