const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.save();
    //context.filter = 'contrast(300%)';
    context.filter = 'grayscale(300%)';
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;
    // clear drawed image
    context.clearRect(0, 0, width, height);
    context.restore();

    // background color
    context.fillStyle = 'hsl(0,0%,0%)';
    context.fillRect(0, 0, width, height);

    const bytes = 4;

    const step = 11;
    context.font = `${step - step / 5}px Monospace`;

    // fix offset
    context.translate(step / 10, step / 2);

    for (let j = 0; j < height; j += step) {
      for (let i = 0; i < width; i += step) {
        const colorIndex = j * (width * bytes) + i * bytes;
        const contrast = 500 / 100 + 1;
        const intercept = 128 * (1 - contrast);
        let r = data[colorIndex];
        let g = data[colorIndex + 1] * contrast + intercept;
        let b = data[colorIndex + 2];
        let a = data[colorIndex + 3];
        context.fillStyle = `rgb(${r},${g},${b})`;
        //context.fillStyle = 'white';

        const color = g;
        if (color < 41) {
          var text = '1';
        } else if (color >= 41 && color < 82) {
          var text = '2';
        } else if (color >= 82 && color < 123) {
          var text = '3';
        } else if (color >= 123 && color < 164) {
          var text = '4';
        } else if (color >= 164 && color < 205) {
          var text = '5';
        } else {
          var text = '6';
        }

        context.fillText(text, i, j);
      }
    }
  };
});
