const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.1;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;
    const size = 17.5;

    // draw square
    context.strokeRect(width / 2 - size / 2, height / 2 - size / 2, size, size);

    const text = 'STEP';
    let indx = 1;
    let x = context.measureText(text).width + 2 * fontSize;
    let y = height / 2 + size / 2 + fontSize;

    // draw steps
    for (let i = width / 2 - size / 2; i < width / 2 + size / 2; i += x) {
      context.save();

      if (indx % 2 === 0) {
        context.translate(i, y);
        context.rotate(-Math.PI / 2);
        context.translate(-i, -y);
        x = x / 4 - 3.3 * fontSize;
        y -= fontSize;
      } else {
        x = 2 * fontSize + context.measureText(text).width;
        y -= context.measureText(text).width - fontSize;
      }

      context.fillText(text, i, y);

      context.restore();
      indx++;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
