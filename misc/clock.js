const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 08%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'white';
    context.strokeStyle = 'white';
    context.lineWidth = 0.25;

    // draw frame
    let size = 22;
    const x = width / 2 - size / 2;
    const y = height / 2 - size / 2;
    context.strokeRect(x, y, size, size);

    // draw time
    context.font = '0.6pt Monospace';
    const timestamp = Date.now();
    context.fillText(timestamp, x, y - 0.4);

    const offset = 4;

    const hour = t => {
      let sizeH = (size / 23) * t;
      let sizeW = 2;
      context.fillRect(x + offset, height / 2 + size / 2 - sizeH, sizeW, sizeH);
    };

    const min = t => {
      let sizeH = (size / 60) * t;
      let sizeW = 2;
      context.fillRect(
        x + offset * 2,
        height / 2 + size / 2 - sizeH,
        sizeW,
        sizeH
      );
    };

    const sec = t => {
      let sizeH = (size / 60) * t;
      let sizeW = 2;
      context.fillRect(
        x + offset * 3,
        height / 2 + size / 2 - sizeH,
        sizeW,
        sizeH
      );
    };

    const mil = t => {
      let sizeH = (size / 1000) * t;
      let sizeW = 2;
      context.fillRect(
        x + offset * 4,
        height / 2 + size / 2 - sizeH,
        sizeW,
        sizeH
      );
    };

    const d = new Date();
    hour(d.getHours());
    min(d.getMinutes());
    sec(d.getSeconds());
    mil(d.getMilliseconds());
  };
};

// Start the sketch
canvasSketch(sketch, settings);
