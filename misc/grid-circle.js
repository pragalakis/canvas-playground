const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,6%)';
    context.strokeStyle = 'hsl(0,0%,6%)';
    context.lineWidth = 0.1;

    let indx = 0;
    const margin = width / 8;
    const size = (width - width / 4) / 9;
    for (let j = margin; j < height - margin; j += size) {
      for (let i = margin; i < width - margin; i += size) {
        context.save();
        context.transform(1, 0.2, 0, 1, 0, -margin / 1.3);

        // square grid
        context.strokeRect(i, j, size, size);

        // circle
        indx = indx == 1 ? 0 : 1;
        if (indx === 1) {
          context.fillStyle = 'hsl(0,0%,6%)';
        } else {
          context.fillStyle = 'hsl(50,50%,50%)';
        }

        context.beginPath();
        context.arc(i + size / 2, j + size / 2, size / 3, 0, Math.PI * 2, true);
        context.fill();

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
