const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'green';
    context.lineWidth = 0.04;

    function arcPoints(rad, angle) {
      // change N for a full circle
      const N = 40;
      return Array(N)
        .fill(0)
        .map((v, i) => {
          let phi = i * 0.1 + angle / Math.PI;
          let x = Math.cos(phi);
          let y = Math.sin(phi);

          return [rad * x, rad * y];
        });
    }

    let rad = 0.3;
    const total = 30;
    // draw arcs on point arcs
    for (let j = 0; j < total; j++) {
      arcPoints(14 - j / 2, j).forEach(arr => {
        context.beginPath();
        context.arc(
          width / 2 + arr[0],
          height / 2 + arr[1],
          rad,
          0,
          Math.PI * 2,
          true
        );
        context.stroke();
      });

      rad -= 0.01;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
