const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 08%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'white';
    context.strokeStyle = 'white';
    context.lineWidth = 0.25;

    // draw frame
    let size = 22;
    const x = width / 2 - size / 2;
    const y = height / 2 - size / 2;
    context.strokeRect(x, y, size, size);

    // draw timestamp
    context.font = '0.6pt Monospace';
    const timestamp = Date.now();
    context.fillText(timestamp, x, y - 0.4);

    const d = new Date();
    const hour = d.getHours();
    const min = d.getMinutes();
    const sec = d.getSeconds();

    context.lineWidth = 0.1;
    context.lineCap = 'round';
    context.translate(width / 2, height / 2);

    context.lineWidth = 0.2;

    // draw minutes
    context.save();
    context.beginPath();
    context.moveTo(0, 0);
    context.rotate(min * (Math.PI / 30) + sec * (Math.PI / (30 * 60)));
    context.lineTo(0, -size / 3);
    context.stroke();
    context.restore();

    // draw hour
    context.save();
    context.rotate(hour * (Math.PI / 6)) +
      min * (Math.PI / (6 * 60)) +
      sec * (Math.PI / (360 * 60));
    context.lineTo(0, -size / 4);
    context.lineTo(0, 0);
    context.stroke();
    context.restore();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
