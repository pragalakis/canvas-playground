const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // line settings
    context.strokeStyle = 'hsl(0, 0%, 5%)';
    context.lineWidth = 0.05;

    let size = 5.8;
    let margin = size / 5.7;
    let n = 1;
    for (let j = margin; j < height - size; j += size + margin) {
      for (let i = 1.7 * margin; i < width - size; i += size + margin) {
        let s = 0;
        let rotation = 0;
        let N = n;

        // draw
        while (N >= 0) {
          context.save();
          context.translate(i + size / 2, j + size / 2);
          context.rotate(i + j === n ? 0 : rotation);
          context.translate(-i - size / 2, -j - size / 2);
          context.strokeRect(i + s / 2, j + s / 2, size - s, size - s);
          context.restore();
          rotation += 0.05;
          s += 0.24;
          N -= 1;
        }
        n += 1;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
