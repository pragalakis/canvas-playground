const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};
// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 92%)';
    context.fillRect(0, 0, width, height);

    function arcs(rad, startPoint, total) {
      context.lineWidth = 0.05;
      context.strokeStyle = 'rgba(0, 0, 255, 1)';

      for (let i = startPoint; i < total; i += rad) {
        context.beginPath();
        context.arc(width / 2, height / 2, rad + i, 0, Math.PI * 2, true);
        context.stroke();
      }
    }

    function border(size, lineWidth) {
      context.strokeStyle = 'hsl(0,0%,92%)';
      context.lineWidth = lineWidth;

      context.strokeRect(
        width / 2 - size / 2,
        height / 2 - size / 2,
        size,
        size
      );
    }

    arcs(0.1, 0, 5);
    border(9, 2);

    arcs(0.3, 3, 11);
    border(19, 4);

    arcs(0.6, 7.19, 17);
    border(30, 6);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
