const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;
    const x = width / 2;
    const y = height / 2;
    const size = 10;

    // draw lines
    context.beginPath();
    context.moveTo(x - size, y - size);
    for (let i = x - size; i <= x + size; i++) {
      for (let j = y - size; j <= y + size; j++) {
        context.lineTo(i, j);
      }
    }
    context.fill();
  };
};
canvasSketch(sketch, settings);
