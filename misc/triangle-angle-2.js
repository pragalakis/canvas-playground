const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,5%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    context.strokeStyle = 'white';

    function triangle(x, y, size, angle) {
      // rotation
      context.translate(width / 2, height / 2);
      context.rotate(Math.sin(angle * 0.01));
      context.translate(-width / 2, -height / 2);

      // triangle
      context.beginPath();
      // vertex A
      context.moveTo(x, y - (Math.sqrt(3) / 3) * size);
      // vertex B
      context.lineTo(x - size / 2, y + (Math.sqrt(3) / 6) * size);
      // vertex C
      context.lineTo(x + size / 2, y + (Math.sqrt(3) / 6) * size);
      // vertex A
      context.closePath();
      context.stroke();

      return size > 1 ? triangle(x, y, size - 0.5, angle + 0.05) : -1;
    }

    let size = 28;
    let angle = 0;
    //let angle = -0.9;
    triangle(width / 2, height / 2, size, angle);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
