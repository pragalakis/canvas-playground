const canvasSketch = require('canvas-sketch');
const palette = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(49, 84.6%, 96.8%)';
    //context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);

    // fixing offset
    context.translate(0.8, 0.8);

    context.lineWidth = 0.05;

    // equilateral triangle
    function triangle(size, x, y) {
      context.beginPath();
      context.moveTo(size * Math.cos(0) + x, size * Math.sin(0) + y);
      context.lineTo(
        size * Math.cos((1 / 3) * 2 * Math.PI) + x,
        size * Math.sin((1 / 3) * 2 * Math.PI) + y
      );
      context.lineTo(
        size * Math.cos((2 / 3) * 2 * Math.PI) + x,
        size * Math.sin((2 / 3) * 2 * Math.PI) + y
      );
      context.lineTo(size * Math.cos(0) + x, size * Math.sin(0) + y);
      context.stroke();
    }

    const size = 0.7;
    const margin = 2 * size;

    const midx = width / 2;
    const midy = height / 2;

    const color = palette[20];
    let colorIndex = 0;

    for (let j = margin; j < height - margin; j = j + size + margin) {
      for (let i = margin; i < width - margin; i = i + size + margin) {
        context.save();
        context.translate(i, j);
        context.rotate(Math.atan2(midy - j, midx - i));
        context.translate(-i, -j);

        context.strokeStyle = color[colorIndex];
        colorIndex = colorIndex > color.length ? 0 : colorIndex + 1;

        triangle(size, i, j);
        triangle(size / 1.5, i, j);
        triangle(size / 3, i, j);
        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
