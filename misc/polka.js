const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 96%)';

    let rad = 1.2;
    let margin = 0.5;
    // circle pattern
    for (i = rad + margin; i < width; i = i + 2 * rad + margin) {
      for (j = rad + margin; j < height; j = j + 2 * rad + margin) {
        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.fill();
        rad = rad - rad * 0.1 > 0 ? rad - rad * 0.1 : rad;
      }
      rad = 1.2;
    }

    // color settings
    context.globalCompositeOperation = 'difference';
    context.fillStyle = 'rgba(255, 255, 255, 1)';

    // color the bottom half
    context.fillRect(0, height / 2, width, height / 2);

    function centeredCircles(rad) {
      context.beginPath();
      context.arc(width / 2, height / 2, rad, 0, Math.PI * 2, true);
      context.fill();
      return rad >= 3 ? centeredCircles(rad - 3) : -1;
    }

    // draw centered circles
    centeredCircles(13);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
