const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    context.fillStyle = 'rgba(255,0,0,0.3)';

    for (let j = 0; j < height; j++) {
      for (let i = 0; i < width; i++) {
        context.fillRect(i, j, 0.7, 0.7);

        if (i >= width / 12 && j >= height / 15) {
          context.fillRect(i + 0.2, j + 0.2, 0.7, 0.7);
        }

        if (i >= width / 4 && j >= height / 6) {
          context.fillRect(i + 0.2, j + 0.2, 0.7, 0.7);
        }

        if (i >= width / 2 && j >= height / 4) {
          context.fillRect(i + 0.4, j + 0.4, 0.7, 0.7);
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
