const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background color
    context.fillStyle = 'hsl(200,25%,50%)';
    context.fillRect(0, 0, width, height);

    const bytes = 4;
    const step = 14;
    let count = 0;
    context.font = `${step}px Arial`;

    for (let i = 0; i < height; i += step) {
      for (let j = 0; j < width; j += step) {
        const colorI = i * (width * bytes) + j * bytes;

        if (data[colorI] <= 100) {
          context.fillStyle = 'hsl(0,0%,0%)';
        } else {
          context.fillStyle = 'hsl(0,0%,100%)';
        }

        let text = count % 2 == 0 ? '$' : '€';
        context.fillText(text, j, i);
        count++;
      }
    }
  };
});
