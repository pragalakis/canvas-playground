const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.1;

    // draw frame
    let rad = 9.1;
    context.beginPath();
    context.arc(width / 2, height / 2, rad, 0, Math.PI * 2, true);
    context.stroke();

    // time
    const d = new Date();
    const hour = (d.getHours() + 24) % 12 || 12;
    const min = d.getMinutes();
    const sec = d.getSeconds();

    context.lineWidth = 0.1;
    context.translate(width / 2, height / 2);

    // draw arc - minutes
    context.save();
    context.rotate(
      min * ((2 * Math.PI) / 60) + sec * ((2 * Math.PI) / (60 * 60))
    );

    context.beginPath();
    context.moveTo(1.5, -1.5);
    context.lineTo(-1.5, -1.5);
    context.lineTo(0, -rad + 0.5);
    context.closePath();
    context.stroke();
    context.restore();

    // draw arc - hour
    const hourAngle =
      (hour - 3) * ((2 * Math.PI) / 12) + min * ((2 * Math.PI) / (12 * 60));
    context.beginPath();
    context.arc(
      Math.cos(hourAngle) * rad,
      Math.sin(hourAngle) * rad,
      1.5,
      0,
      Math.PI * 2,
      true
    );
    context.stroke();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
