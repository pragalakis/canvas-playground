const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

const text = `THE CRITICAL ENGINEERING MANIFESTO 0. The Critical Engineer considers Engineering to be the most transformative language of our time, shaping the way we move, communicate and think. It is the work of the Critical Engineer to study and exploit this language, exposing its influence. 1. The Critical Engineer considers any technology depended upon to be both a challenge and a threat. The greater the dependence on a technology the greater the need to study and expose its inner workings, regardless of ownership or legal provision. 2. The Critical Engineer raises awareness that with each technological advance our techno-political literacy is challenged. 3. The Critical Engineer deconstructs and incites suspicion of rich user experiences. 4. The Critical Engineer looks beyond the "awe of implementation" to determine methods of influence and their specific effects. 5. The Critical Engineer recognises that each work of engineering engineers its user, proportional to that user's dependency upon it. 6. The Critical Engineer expands "machine" to describe interrelationships encompassing devices, bodies, agents, forces and networks. 7. The Critical Engineer observes the space between the production and consumption of technology. Acting rapidly to changes in this space, the Critical Engineer serves to expose moments of imbalance and deception. 8. The Critical Engineer looks to the history of art, architecture, activism, philosophy and invention and finds exemplary works of Critical Engineering. Strategies, ideas and agendas from these disciplines will be adopted, re-purposed and deployed. 9. The Critical Engineer notes that written code expands into social and psychological realms, regulating behaviour between people and the machines they interact with. By understanding this, the Critical Engineer seeks to reconstruct user-constraints and social action through means of digital excavation. 10. The Critical Engineer considers the exploit to be the most desirable form of exposure.`;

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,7%)';
    context.strokeStyle = 'hsl(0,0%,7%)';
    context.lineWidth = 0.1;

    const margin = 1;
    const size = 0.75;
    let charIndex = 0;
    context.font = `${size - size / 4}px Monospace`;

    for (let j = margin / 2; j < height - margin; j += size) {
      for (let i = margin; i < width - margin; i += size) {
        let char = text[charIndex];
        charIndex++;
        if (char === ' ') {
          context.fillRect(i, j, size, size);
          context.strokeRect(i, j, size, size);
        } else if (char === undefined) {
          context.fillStyle = 'hsl(0,0%,97%)';
          context.lineWidth = 0.1;

          context.save();
          context.beginPath();
          context.rect(i, j, size, size);
          context.fill();
          context.stroke();
          context.clip();

          // diagonal lines
          const step = 0.2;
          context.lineWidth = 0.05;
          for (let h = 0; h <= 2 * size; h += step) {
            context.beginPath();
            context.moveTo(i + h, j);
            context.lineTo(i, j + h);
            context.stroke();
          }
          context.restore();

          context.lineWidth = 0.1;
          context.fillStyle = 'hsl(0,0%,7%)';
        } else {
          context.fillText(char, i + size / 4, j + size / 1.3);
          context.strokeRect(i, j, size, size);
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
