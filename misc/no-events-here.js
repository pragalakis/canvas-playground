const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);

    // bottom background
    context.fillStyle = 'white';
    context.fillRect(0, height - height / 3, width, height);

    context.lineWidth = 0.03;
    context.strokeStyle = 'white';
    const fontSize = 1;
    context.font = `${fontSize}pt Roboto`;
    const text = '4RT';
    const textW = context.measureText(text).width;

    // top text grid
    for (let j = 0; j <= height - height / 3; j += fontSize) {
      for (let i = 0; i < width; i += textW) {
        if (j % 2 === 0) {
          context.strokeText(text, i, j);
        } else {
          context.fillText(text, i, j);
        }
      }
    }

    // bottom text grid
    context.fillStyle = 'black';
    const text2 = 'NO EVENTS HERE ';
    const fontSize2 = 0.7;
    context.font = `${fontSize2}pt Roboto`;
    let indx = 0;
    for (
      j = height - height / 3 + 1.5 * fontSize2;
      j < height;
      j += fontSize2
    ) {
      for (let i = 0; i < width; i += fontSize2) {
        context.fillText(text2[indx], i, j);
        indx = indx < text2.length - 1 ? indx + 1 : 0;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
