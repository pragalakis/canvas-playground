const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 8%)';
    context.lineWidth = 0.1;

    // draw frame
    let rad = 9.1;
    // time
    const d = new Date();
    const hour = (d.getHours() + 24) % 12 || 12;
    const min = d.getMinutes();
    const sec = d.getSeconds();
    const msec = d.getMilliseconds();

    context.lineWidth = 0.1;
    context.translate(width / 2, height / 2);

    // draw arc - seconds
    context.beginPath();
    context.arc(0, 0, rad / 3, 0, Math.PI * 2, true);
    context.stroke();

    context.fillStyle = 'rgb(0, 0, 255)';
    const secAngle = (sec - 15) * ((2 * Math.PI) / 60);
    context.beginPath();
    context.arc(
      Math.cos(secAngle) * (rad / 3),
      Math.sin(secAngle) * (rad / 3),
      0.4,
      0,
      Math.PI * 2,
      true
    );
    context.fill();

    // draw arc - minutes
    context.beginPath();
    context.arc(0, 0, rad / 1.5, 0, Math.PI * 2, true);
    context.stroke();

    context.fillStyle = 'rgb(0, 255, 0)';
    const minAngle =
      (min - 15) * ((2 * Math.PI) / 60) + sec * ((2 * Math.PI) / (60 * 60));
    context.beginPath();
    context.arc(
      Math.cos(minAngle) * (rad / 1.5),
      Math.sin(minAngle) * (rad / 1.5),
      0.8,
      0,
      Math.PI * 2,
      true
    );
    context.fill();

    // draw arc - hour
    context.beginPath();
    context.arc(0, 0, rad, 0, Math.PI * 2, true);
    context.stroke();

    context.fillStyle = 'rgb(255, 0, 0)';
    const hourAngle =
      (hour - 2) * ((2 * Math.PI) / 12) + min * ((2 * Math.PI) / 60);
    context.beginPath();
    context.arc(
      Math.cos(hourAngle) * rad,
      Math.sin(hourAngle) * rad,
      1.2,
      0,
      Math.PI * 2,
      true
    );
    context.fill();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
