const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};
// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 92%)';
    context.fillRect(0, 0, width, height);

    // fix offset
    context.translate(0, -0.6);

    context.strokeStyle = 'rgb(255,0,0)';
    context.lineWidth = 0.05;

    const margin = 1;
    let size = height / 6 + margin;
    let rad = 0.15;

    for (let j = 0; j < height; j += size) {
      var nHeight = height - j;
      let total = 20;

      // draw arcs on each separated space
      for (let i = 0; i < total / rad; i += rad) {
        context.beginPath();
        context.arc(width / 2, nHeight - margin, rad + i, 0, Math.PI, true);
        context.stroke();
      }
      rad = rad + 0.15;

      // draw separators
      context.fillRect(0, 0, width, nHeight - size);
    }

    // draw margin
    context.fillRect(0, 0, width, margin);
    context.fillRect(0, 0, margin, height);
    context.fillRect(width - margin, 0, width, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
