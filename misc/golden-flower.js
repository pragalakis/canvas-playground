const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,3%)';
    context.lineWidth = 0.05;

    const N = 1000;
    const data = Array(N)
      .fill(0)
      .map((v, i) => {
        // golden angle
        let phi = i + (Math.sqrt(5) + 1) / 2 - 1;
        let x = Math.cos(phi);
        let y = Math.sin(phi);
        let rad = i * 0.03;
        return [rad * x, rad * y];
      });

    let rad;
    data.forEach((v, i) => {
      rad = (i * 0.033) / (Math.PI * 4);
      context.beginPath();
      context.arc(
        width / 2 + v[0],
        height / 2 + v[1],
        rad,
        0,
        Math.PI * 2,
        true
      );
      context.fill();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
