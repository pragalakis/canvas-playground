// watch this talk
// https://www.youtube.com/watch?v=NoqQQwP1Duo
const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);

    let size = 0.03;
    let offset = 1;

    console.log(Math);
    for (let j = offset; j <= height - offset; j += size) {
      for (let i = offset; i <= width - offset; i += size) {
        let color = Math.floor(Math.pow(i, 2) + Math.cos(j)) % 2;
        context.fillStyle = color ? 'white' : 'black';
        context.fillRect(i, j, size, size);
      }
    }
  };
};
// Start the sketch
canvasSketch(sketch, settings);
