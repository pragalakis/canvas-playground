const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';

    const drawText = (text, rad) => {
      // calculate letter position points
      const points = Array(text.length)
        .fill()
        .map((v, i) => {
          const phi = (i * Math.PI * 2) / text.length;
          const x = Math.cos(phi);
          const y = Math.sin(phi);
          return [rad * x, rad * y, phi];
        });

      // draw letters
      let indx = 0;
      points.forEach(v => {
        const letter = text[indx];
        const x = width / 2 + v[0];
        const y = height / 2 + v[1];

        context.save();
        context.translate(x + fontSize / 2, y - fontSize / 2);
        // alternative you can rotate according
        // to the center of the circle with Math.atan2(midy-y,midx-x)
        context.rotate(-(3 * Math.PI) / 2 + v[2]);
        context.translate(-x - fontSize / 2, -y + fontSize / 2);
        context.fillText(letter, x, y);
        context.restore();

        indx = indx >= text.length - 1 ? 0 : indx + 1;
      });
    };

    let rad = 13;
    let fontSize = 0.5;
    // call drawText for different radius and font size
    while (rad > 1) {
      context.font = `${fontSize}pt Monospace`;
      drawText(
        'CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE CIRCLE ',
        rad
      );
      rad -= 1;
      fontSize -= 0.025;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
