const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    // change these vars for different effects
    const N = 210;
    const a = 2;
    const ax = 0.1;
    const ay = 0.2;
    const rad = 4;
    const xpos = width / 2;
    const ypos = 0;

    // circle's center point generation
    const points = Array(N)
      .fill()
      .map((v, i) => {
        const x = Math.cos(i * ax);
        const y = i * ay;
        return [a * x, y];
      });

    // draw a circle on each of the points
    points.forEach(v => {
      context.beginPath();
      context.arc(xpos + v[0], ypos + v[1], rad, 0, Math.PI * 2, true);
      context.stroke();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
