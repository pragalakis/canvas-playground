const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    /* background */
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    /* settings */
    context.lineWidth = 0.35;
    const step = 0.8;
    const offset = 2.5;

    // vertical lines
    context.strokeStyle = 'rgb(0,0,255,0.8)';
    for (let i = offset; i <= width - offset; i += step) {
      context.beginPath();
      context.moveTo(i, offset);
      context.lineTo(i, height - offset);
      context.stroke();
    }

    // rotated vertical lines
    context.strokeStyle = 'rgb(255,0,0,0.8)';
    context.save();
    context.translate(width / 2, height / 2);
    context.rotate(5 * (Math.PI / 180));
    context.translate(-width / 2, -height / 2);
    for (let i = offset; i < width - offset; i += step) {
      context.beginPath();
      context.moveTo(i, offset);
      context.lineTo(i, height - offset);
      context.stroke();
    }
    context.restore();

    // inversed rotated vertical lines
    context.strokeStyle = 'rgb(0,355,0,0.8)';
    context.save();
    context.translate(width / 2, height / 2);
    context.rotate(-5 * (Math.PI / 180));
    context.translate(-width / 2, -height / 2);
    for (let i = offset; i < width - offset; i += step) {
      context.beginPath();
      context.moveTo(i, offset);
      context.lineTo(i, height - offset);
      context.stroke();
    }
    context.restore();
  };
};
canvasSketch(sketch, settings);
