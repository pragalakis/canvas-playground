const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  // img source from @scatsx account on twitter
  const image = await load('./assets/space_cats.png');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [30, 30],
    pixelsPerInch: 300,
    units: 'cm'
  });

  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(0.5, 0.5);

    let size = 4;
    let offset = 5;

    for (let i = 0; i < width; i += offset) {
      for (let j = 0; j < height; j += offset) {
        // draw image to canvas
        context.drawImage(image, i, j, size, size);
      }
    }
  };
});
