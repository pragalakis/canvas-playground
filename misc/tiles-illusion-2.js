const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    /* background */
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    /* settings */
    context.strokeStyle = 'hsl(0,0%,6%)';
    context.lineWidth = 0.1;
    const tileW = 1.2;
    const tileH = 1.9;

    let z = 0;
    for (let j = 0.05; j < height; j += tileH + 0.1) {
      for (let i = 0; i < width; i += tileW) {
        const d = Math.abs(i - width / 2);
        const variance = Math.max(0, width / 2 - d);

        context.save();
        context.translate(width / 2, j);
        if (z % 2 === 0) {
          context.scale(-1, 1);
        }
        context.translate(-width / 2, -j);
        context.lineWidth = Math.log(variance) / 2.5;
        context.beginPath();
        context.moveTo(i, j);
        context.lineTo(i, j + tileH);
        context.stroke();
        context.restore();
      }
      z++;
    }
  };
};
canvasSketch(sketch, settings);
