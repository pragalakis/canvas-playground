const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;

    const drawText = fntSize => {
      const txt = 'ΑΘΝΣ';
      context.font = `${fntSize}pt Serif`;
      const tWidth = context.measureText(txt).width;

      context.fillText(txt, width / 2 - tWidth / 2, height / 2 + fntSize / 2);
      context.strokeText(txt, width / 2 - tWidth / 2, height / 2 + fntSize / 2);
    };

    // rotation
    context.translate(width / 2, height / 2);
    context.rotate(Math.PI / 2);
    context.translate(-width / 2, -height / 2);

    // draw text for different font size
    drawText(8);
    drawText(8.5);
    drawText(9);
    drawText(9.5);
    drawText(10);
  };
};
canvasSketch(sketch, settings);
