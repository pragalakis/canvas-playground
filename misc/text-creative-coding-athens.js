const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    const fontSize = 0.5;
    context.font = `${fontSize}pt Roboto`;
    const text1 = 'CREATIVE ';
    const text2 = 'CODING ';
    const text3 = 'ATHENS ';
    const t1w = context.measureText(text1).width;
    const t2w = context.measureText(text2).width;
    const t3w = context.measureText(text3).width;

    // 1/3
    let tw = t1w;
    let t = text1;
    context.fillStyle = 'red';

    //grid
    for (let j = 0; j <= height + fontSize; j += fontSize + 0.2) {
      for (let i = 0; i < width; i += tw) {
        // 2/3
        if (j > height / 3) {
          context.fillStyle = 'green';
          tw = t2w;
          t = text2;
        }

        // 3/3
        if (j > 2 * (height / 3)) {
          context.fillStyle = 'blue';
          tw = t3w;
          t = text3;
        }

        context.fillText(t, i, j);
      }
    }
  };
};
canvasSketch(sketch, settings);
