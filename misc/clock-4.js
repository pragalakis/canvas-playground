const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 24
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'yellow';
    context.fillRect(0, 0, width, height);

    // draw frame
    context.lineWidth = 0.5;
    context.fillStyle = 'white';
    context.strokeStyle = 'hsl(0, 0%, 0%)';
    let size = 22;
    const x = width / 2 - size / 2;
    const y = height / 2 - size / 2;
    context.fillRect(x, y, size, size);
    context.strokeRect(x, y, size, size);

    // draw text-timestamp
    const timestamp = Date.now();

    // time
    const d = new Date();
    const hour = (d.getHours() + 24) % 12 || 12;
    const min = d.getMinutes();
    const sec = d.getSeconds();
    const msec = d.getMilliseconds();

    //context.lineCap = 'round';
    context.translate(width / 2, height / 2);

    // draw arc - seconds
    context.strokeStyle = 'red';
    context.lineWidth = 1;
    context.save();
    context.rotate((3 * Math.PI) / 2);
    context.beginPath();
    context.arc(0, 0, size / 3, 0, sec * (Math.PI / 30), false);
    context.stroke();
    context.restore();

    // draw arc - minutes
    context.lineWidth = 1.5;
    context.strokeStyle = 'green';
    context.save();
    context.rotate((3 * Math.PI) / 2);
    context.beginPath();
    context.arc(
      0,
      0,
      size / 4,
      0,
      min * (Math.PI / 30) + sec * ((2 * Math.PI) / (60 * 60)),
      false
    );
    context.stroke();
    context.restore();

    // draw arc - hours
    context.lineWidth = 2;
    context.strokeStyle = 'blue';
    context.save();
    context.rotate((3 * Math.PI) / 2);
    context.beginPath();
    context.arc(
      0,
      0,
      size / 7,
      0,
      hour * (Math.PI / 6) + min * (Math.PI / 30) * 0.1,
      false
    );
    context.stroke();
    context.restore();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
