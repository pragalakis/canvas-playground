const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 90%)';
    context.fillRect(0, 0, width, height);

    // line settings
    context.strokeStyle = 'hsl(0, 0%, 8%)';
    context.lineWidth = 0.05;

    const rad = 3;
    let n = 1;
    let rotation = Math.PI / 4;
    for (let j = 1.5 * rad; j < height - rad; j += 2.7 * rad) {
      for (let i = 2.1 * rad; i < width - rad; i += 2.7 * rad) {
        let N = n;
        while (N > 0) {
          // draw elipse
          context.beginPath();
          context.ellipse(i, j, rad / 2, rad, rotation, 0, 2 * Math.PI);
          context.stroke();

          N--;
          rotation += 0.3;
        }
        n++;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
