const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};
// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // fix offset
    context.translate(-0.2, 0);

    let rad = 3;
    for (i = 2 * rad; i < width - rad; i += rad) {
      for (j = 2 * rad; j < height - rad; j += rad) {
        context.beginPath();
        context.arc(i, j, 1 * rad, 0, Math.PI * 2, true);
        if (j % 2 == 0) {
          context.fillStyle = 'rgba(0,0,255,0.2)';
          context.fill();
        } else {
          context.fillStyle = 'rgba(255,0,0,0.2)';
          context.fill();
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
