const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    function checkered(x, y, w, h, size, rotation, colors) {
      let cIndex = 0;
      context.save();
      context.translate(x + w / 2, y + h / 2);
      context.rotate(rotation);
      context.translate(-x - w / 2, -y - h / 2);

      for (let j = 0; j < h; j += size) {
        cIndex = cIndex >= colors.length - 1 ? 0 : cIndex + 1;
        for (let i = 0; i < w; i += size) {
          context.fillStyle = colors[cIndex];
          context.fillRect(x + i, y + j, size, size);
          cIndex = cIndex >= colors.length - 1 ? 0 : cIndex + 1;
        }
      }
      context.restore();
    }

    checkered(0, 0, width, height, 80, 0, ['orange', 'black']);
    for (let i = 0; i < Math.PI; i += 0.05) {
      checkered(width / 2, height / 2, 3200, 3200, 60, i, ['orange', 'black']);
      checkered(0, 0, 2000, 2000, 60, 2 * i, ['orange', 'black']);
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
