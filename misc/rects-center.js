const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    const size = 3.25;
    const margin = 0.5;

    // rect grid
    for (let j = size / 5; j < height - size; j += size + margin) {
      for (let i = 0.1; i < width; i += size + margin) {
        let N = 18;
        let s = 0;
        let lw = 0.08;
        // fading color
        context.strokeStyle = `hsl(0,0%,${3 * s}%)`;

        // draw rects
        while (N > 0) {
          context.lineWidth = lw;
          context.strokeRect(i + s / 2, j + s / 2, size - s, size - s);
          s += 0.2;
          lw -= 0.005;
          N--;
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
