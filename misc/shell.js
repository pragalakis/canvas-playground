const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.01;
    context.strokeStyle = 'green';

    const total = 600;
    const zoom = 25;
    const angle = 2 * Math.PI - 6;
    let j = 0;
    context.beginPath();
    context.moveTo(width / 2, height / 1.3);
    for (let i = 0; i < total; i++) {
      let c = -1 * Math.cos(i * angle);
      context.lineTo(
        width / 2 + c * j * Math.sin(i * angle),
        height / 1.3 + c * j * Math.cos(i * angle)
      );
      context.stroke();
      j = i / zoom;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
