/* the nyan cat image is under https://creativecommons.org/licenses/by-nc/4.0/ */
const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// Artwork function
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./nyan-cat.png');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: 'A3',
    pixelsPerInch: 72,
    units: 'px'
  });
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,7%)';
    context.fillRect(0, 0, width, height);

    const colours = ['red', 'orange', 'yellow', 'green', 'blue', 'purple'];

    const sx = 50;
    const sy = 8;
    const numOfTiles = 6;
    const imgW = sx * 2;
    const imgH = 8 * 1.5 * numOfTiles;
    context.lineWidth = sy;

    const drawTrail = y => {
      let deviationY = 0;
      for (var i = 0; i <= width - 200; i += sx) {
        cIndex = 0;
        for (
          var j = y - (numOfTiles / 2) * sy;
          j < y + (numOfTiles / 2) * sy;
          j += sy
        ) {
          context.strokeStyle = colours[cIndex];

          context.beginPath();
          context.moveTo(i, deviationY + j);
          context.lineTo(i + sx, deviationY + j);
          context.stroke();

          cIndex = cIndex > colours.length ? 0 : cIndex + 1;
        }
        deviationY = deviationY >= sy ? 0 : deviationY + sy;
      }
      context.drawImage(image, i - imgW / 3, j - imgH / 1.1, imgW, imgH);
    };

    for (let y = 50; y <= height - 50; y += sy * numOfTiles * 2) {
      drawTrail(y);
    }
  };
});
