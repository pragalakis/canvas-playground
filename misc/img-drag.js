const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    // get image data
    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    const bytes = 4;
    // glitch point
    let glpoint = height / 2;
    // pixels step
    let step = 1;

    for (let i = 0; i < width; i += step) {
      for (let j = 0; j < height; j += step) {
        // after a point, keep the same color
        if (j >= glpoint) {
          var colorIndex = Math.floor(glpoint) * (width * bytes) + i * bytes;
        } else {
          var colorIndex = j * (width * bytes) + i * bytes;
        }

        // colors on each step
        const r = data[colorIndex];
        const g = data[colorIndex + 1];
        const b = data[colorIndex + 2];
        context.fillStyle = `rgba(${r},${g},${b},1)`;

        // draw
        if (j >= glpoint) {
          context.fillRect(i, j, step, height - j);
          break;
        } else {
          context.fillRect(i, j, step, step);
        }
      }
    }
  };
});
