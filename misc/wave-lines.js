const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 96%)';
    context.lineWidth = 0.1;

    function curve(y) {
      context.beginPath();
      context.moveTo(0, y);
      context.bezierCurveTo(1, y - 2, 2.13, y - 2, 5.41, y);
      context.bezierCurveTo(7, y + 1, 11.7, y + 3, 14.5, y);
      context.bezierCurveTo(16, y - 1, 19, y - 1, 21.2, y);
      context.bezierCurveTo(23.3, y + 1, 26.4, y + 1, 26.6, y);
      context.bezierCurveTo(26.75, y - 2, 28.29, y - 2, width, y);

      context.stroke();
    }

    for (let i = -2; i < height + 2; i += 0.5) {
      curve(i);
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
