const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // hexagon radius
  const size = 1.1;
  // hexagon coords
  const points = pointsOnCircle(6, size);

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,3%)';
    context.lineWidth = 0.05;

    let odd = true;
    // draw hexagon grid
    for (let j = -size; j < height + size; j = j + (Math.sqrt(3) * size) / 2) {
      for (let i = 0; i < width + size; i += 3 * size) {
        //context.fillStyle = `rgb(${50 + j},0,0)`;
        context.beginPath();
        points.forEach(v => {
          if (odd) {
            context.lineTo(v.x + i + size + size / 2, v.y + j);
          } else {
            context.lineTo(v.x + i, v.y + j);
          }
        });
        context.closePath();
        //context.fill();
        context.stroke();
      }
      odd = !odd;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
