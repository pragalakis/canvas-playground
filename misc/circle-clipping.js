const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.1;
    const rad = 6;

    // top circles
    const x1 = width / 2.8;
    const y1 = height / 5;
    context.beginPath();
    context.arc(x1, y1, rad, 0, Math.PI * 2, true);
    context.stroke();

    const x2 = width / 1.6;
    const y2 = height / 5;
    context.beginPath();
    context.arc(x2, y2, rad, 0, Math.PI * 2, true);
    context.stroke();

    // dots
    for (let j = 0; j < height; j += 0.5) {
      for (let i = 0; i < width; i += 0.5) {
        const dist1 = Math.hypot(i - x1, j - y1);
        const dist2 = Math.hypot(i - x2, j - y2);

        // cliping and drawing
        if (dist1 < rad && dist2 > rad) {
          context.beginPath();
          context.arc(i, j, 0.1, 0, Math.PI * 2, true);
          context.fill();
        }
      }
    }

    // middle circles
    const x3 = width / 2.8;
    const y3 = height / 2;
    context.beginPath();
    context.arc(x3, y3, rad, 0, Math.PI * 2, true);
    context.stroke();

    const x4 = width / 1.6;
    const y4 = height / 2;
    context.beginPath();
    context.arc(x4, y4, rad, 0, Math.PI * 2, true);
    context.stroke();

    // dots
    for (let j = 0; j < height; j += 0.5) {
      for (let i = 0; i < width; i += 0.5) {
        const dist1 = Math.hypot(i - x3, j - y3);
        const dist2 = Math.hypot(i - x4, j - y4);

        // cliping and drawing
        if (dist1 < rad && dist2 < rad) {
          context.beginPath();
          context.arc(i, j, 0.1, 0, Math.PI * 2, true);
          context.fill();
        }
      }
    }

    // bottom circles
    const x5 = width / 2.8;
    const y5 = height / 1.25;
    context.beginPath();
    context.arc(x5, y5, rad, 0, Math.PI * 2, true);
    context.stroke();

    const x6 = width / 1.6;
    const y6 = height / 1.25;
    context.beginPath();
    context.arc(x6, y6, rad, 0, Math.PI * 2, true);
    context.stroke();

    // dots
    for (let j = 0; j < height; j += 0.5) {
      for (let i = 0; i < width; i += 0.5) {
        const dist1 = Math.hypot(i - x5, j - y5);
        const dist2 = Math.hypot(i - x6, j - y6);

        // cliping and drawing
        if (dist1 > rad && dist2 < rad) {
          context.beginPath();
          context.arc(i, j, 0.1, 0, Math.PI * 2, true);
          context.fill();
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
