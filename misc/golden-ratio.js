const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: [30, 30],
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(0.0, 5.5);
    context.lineWidth = 0.07;
    context.strokeStyle = 'white';

    let steps = 5;

    for (i = 0; i < steps; i++) {
      let s = width * 0.382;
      let l = width - s;

      // draw large square
      context.fillStyle = `rgb(${255 - 10 * Math.exp(i)}, ${255 -
        10 * Math.exp(i)},${255 - 10 * Math.exp(i)})`;
      context.fillRect(0, 0, l, l);
      context.strokeRect(0, 0, l, l);

      // draw large curve
      context.fillStyle = `rgb(${255 - 30 * (i + 1)}, ${255 -
        30 * (i + 1)},${255 - 30 * (i + 1)})`;
      context.beginPath();
      context.moveTo(l, 0);
      context.quadraticCurveTo(0, 0, 0, l);

      context.moveTo(l, 0);
      context.lineTo(l, l);
      context.lineTo(0, l);
      context.fill();
      context.stroke();

      // draw small square
      context.fillStyle = `rgb(${255 - 30 * (i + 1)}, ${255 -
        30 * (i + 1)},${255 - 30 * (i + 1)})`;
      context.translate(l, 0);
      context.fillRect(0, 0, s, s);
      context.strokeRect(0, 0, s, s);

      // draw small curve
      context.fillStyle = `rgb(${255 - 10 * Math.exp(i)}, ${255 -
        10 * Math.exp(i)},${255 - 10 * Math.exp(i)})`;
      context.beginPath();
      context.moveTo(s, s);
      context.quadraticCurveTo(s, 0, 0, 0);

      context.moveTo(0, 0);
      context.lineTo(0, s);
      context.lineTo(s, s);
      context.fill();
      context.stroke();

      context.translate(s, l);
      context.scale(-1, -1);

      width = s;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
