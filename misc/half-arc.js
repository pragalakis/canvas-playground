const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,92%)';
    context.fillRect(0, 0, width, height);

    // arcs color
    context.strokeStyle = 'hsl(0,0%,3%)';

    let radius = 25;
    let linewidth = 4;
    let N = 25;

    for (let i = 0; i < N; i++) {
      linewidth = linewidth - linewidth * 0.2;
      radius = radius - linewidth - linewidth * 0.2;
      if (radius < 3) continue;

      context.lineWidth = linewidth;
      context.beginPath();
      context.translate(width / 2, height / 2);
      context.rotate(-0.3);
      context.translate(-width / 2, -height / 2);
      context.arc(width / 2, height / 2, radius, 0, Math.PI - 1, true);
      context.stroke();
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
