const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    context.strokeStyle = 'black';

    let total = 100;
    let rad = 14;
    for (let i = 0; i <= total; i++) {
      if (i <= total / 4) {
        context.translate(i / total, i / total);
      } else if (i <= total / 3) {
        context.translate(i / (2 * total), -i / (2 * total));
      } else if (i <= total / 2) {
        context.translate(-i / (3 * total), -i / (3 * total));
      } else {
        context.translate(-i / (5 * total), i / (5 * total));
      }
      context.beginPath();
      if (rad - i / 4.3 > 0) {
        context.arc(width / 2, height / 2, rad - i / 4.3, 0, Math.PI * 2, true);
      } else {
        continue;
      }
      context.stroke();
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
