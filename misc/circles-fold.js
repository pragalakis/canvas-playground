const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    // fix offset
    context.translate(0.3, 0.7);

    const circle = (x0, y0, radX, radY) => {
      const points = Array(50)
        .fill(0)
        .map((v, i) => {
          let phi = i + Math.PI * 2;
          let x = radX * Math.cos(phi);
          let y = radY * Math.sin(phi);
          return [x0 + x, y0 + y];
        });
      return points;
    };

    context.fillStyle = 'black';

    let offset = 0.5;
    let size = 2;
    let c = 1;
    let offsetY = size * Math.abs(Math.cos(c));
    for (let j = offset; j < height; j = j + offsetY) {
      c = c + 1 / 30;
      let radY = Math.abs(Math.cos(c));
      offsetY = size * 1.5 * radY;
      let radX = size / 2;
      for (let i = size; i < width - size; i = i + size + offset) {
        const points = circle(i, j, radX, radY);
        // draw circle
        context.beginPath();
        context.moveTo(points[0][0], points[0][1]);
        points.forEach(v => {
          context.lineTo(v[0], v[1]);
        });
        context.closePath();
        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
