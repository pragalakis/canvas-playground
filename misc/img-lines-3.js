const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    // get image data
    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 1;
    const bytes = 4;

    // pixels step
    let step = 10;

    for (let i = 0; i < width; i += step) {
      for (let j = 0; j < height; j += step) {
        const colorIndex = j * (width * bytes) + i * bytes;
        const r = data[colorIndex];
        const g = data[colorIndex + 1];
        const b = data[colorIndex + 2];
        context.strokeStyle = 'black';

        context.beginPath();
        context.moveTo(i, j);
        if (r + g + b >= 255 / 2) {
          context.lineTo(i, j + step);
        } else {
          context.lineTo(i + step, j);
        }
        context.stroke();
      }
    }
  };
});
