const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;

    const x = width / 2 - context.measureText('N').width / 2;
    const y = height / 2 + fontSize / 2;

    context.fillText('N', x, y);

    pointsOnCircle(6, 1).forEach(v => {
      context.fillText('N', x + v.x, y + v.y);
    });

    pointsOnCircle(10, 2).forEach(v => {
      context.fillText('N', x + v.x, y + v.y);
    });

    pointsOnCircle(20, 3).forEach(v => {
      context.fillText('U', x + v.x, y + v.y);
    });

    pointsOnCircle(26, 4).forEach(v => {
      context.fillText('U', x + v.x, y + v.y);
    });

    pointsOnCircle(30, 5).forEach(v => {
      context.fillText('U', x + v.x, y + v.y);
    });

    pointsOnCircle(36, 6).forEach(v => {
      context.fillText('S', x + v.x, y + v.y);
    });

    pointsOnCircle(40, 7).forEach(v => {
      context.fillText('S', x + v.x, y + v.y);
    });

    pointsOnCircle(20, 9).forEach((v, i) => {
      context.fillText('S', x + v.x, y + v.y);
    });

    pointsOnCircle(20, 10).forEach((v, i) => {
      context.fillText('S', x + v.x, y + v.y);
    });

    pointsOnCircle(20, 11).forEach((v, i) => {
      context.fillText('S', x + v.x, y + v.y);
    });

    pointsOnCircle(20, 12).forEach((v, i) => {
      context.fillText('S', x + v.x, y + v.y);
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
