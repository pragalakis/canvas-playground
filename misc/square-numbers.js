const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.font = '0.5pt Monospace';

    let txt = 1;
    const margin = 4;
    const size = (width - 2 * margin) / 3;
    for (let j = margin; j < width - margin; j += size) {
      for (let i = margin; i < width - margin; i += size) {
        // draw text
        for (let y = j; y < j + size - 0.7; y += 0.7) {
          for (let x = i - 0.5; x < i + size - 1; x += 0.5) {
            context.fillText(String(txt), x + 0.5, y + 0.7);
          }
        }
        txt++;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
