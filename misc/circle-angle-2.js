const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(49, 84.6%, 96.8%)';
    context.fillRect(0, 0, width, height);

    // fixing offset
    context.translate(0.85, 0.85);

    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    function circle(rad, x, y) {
      context.beginPath();
      context.arc(x, y, rad, 0, Math.PI * 2, true);
      context.stroke();
    }

    function inRange(x, min, max) {
      return x >= min && x <= max;
    }

    const rad = 1.4;
    const margin = 1.2 * rad;

    const midx = width / 2;
    const midy = height / 2;

    for (let j = margin; j < height - margin; j = j + rad + margin) {
      for (let i = margin; i < width - margin; i = i + rad + margin) {
        let diffx = midx - i;
        let diffy = midy - j;

        context.save();
        context.translate(i, j);

        // for the middle area
        if (inRange(diffx, -4, 4) && inRange(diffy, -4, 4)) {
          context.rotate(-Math.atan2(diffy, -diffx));
          var deviation = 3;
          var displacement = 0.05;
        } else {
          // where atan2 is the center where everything points to
          context.rotate(Math.atan2(diffy, diffx));
          var deviation = 0.9;
          var displacement = 0.09;
        }

        context.translate(-i, -j);

        // circle and sub-circles
        for (let z = 0; z < 1; z += displacement) {
          circle(rad - rad * z, i + z * deviation, j);
        }

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
