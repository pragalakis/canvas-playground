const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    const size = 1;
    //context.lineWidth = size / 2;
    context.lineWidth = size / 1.4;
    context.lineCap = 'square';

    const colors = ['white', 'black'];
    const lw = ['80%', '100%']; // white luminosity
    const lb = ['0%', '20%']; // black luminosity
    let lwIndx = 0; // luminosity black index
    let lbIndx = 0; // luminosity white index

    let cIndx = 0; // color index
    for (let j = 0; j < height + size; j += size) {
      let indx = 0;
      for (let i = 0; i < width; i += size) {
        // coloring - luminosity
        if (colors[cIndx] === 'white') {
          context.strokeStyle = `hsl(0,0%,${lw[lwIndx]})`;
          lwIndx = lwIndx === lw.length - 1 ? 0 : lwIndx + 1;
        } else {
          context.strokeStyle = `hsl(0,0%,${lb[lbIndx]})`;
          lbIndx = lbIndx === lb.length - 1 ? 0 : lbIndx + 1;
        }

        // cliping the area
        context.save();
        context.beginPath();
        context.rect(i, j - size, size, 2 * size);
        context.clip();

        // line
        context.beginPath();
        if (indx % 2 === 0) {
          context.moveTo(i, j + size / 2);
          context.lineTo(i + size, j - size / 2);
        } else {
          context.moveTo(i, j - size / 2);
          context.lineTo(i + size, j + size / 2);
        }
        context.stroke();
        context.restore();

        indx++;
      }
      // color index (different color on each line)
      cIndx = cIndx === colors.length - 1 ? 0 : cIndx + 1;
    }
  };
};
canvasSketch(sketch, settings);
