const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;
    const margin = 1.5;
    const lw = context.measureText('A').width;
    const text = 'EMPATHY ';
    const points = [];

    // calculate letter position points
    for (let j = 0; j < height + 2; j += fontSize + margin) {
      let indx = 0;
      for (let i = -2; i < width + 2; i += lw) {
        const x = i + Math.cos(Math.PI);
        const y = j + Math.sin(i);
        const letter = text[indx];
        points.push([x, y, letter]);
        indx = indx >= text.length - 1 ? 0 : indx + 1;
      }
    }

    // draw letters
    points.forEach(v => {
      context.fillText(v[2], v[0], v[1]);
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
