const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    const rad = 1.5;
    const marginx = 6;
    const marginy = 10;
    let phase = [3, 2.4, 1.8, 1.2, 0.6];
    let phaseIndx = 0;
    for (let j = marginy; j <= height - marginy; j += rad * 2.9) {
      for (let i = marginx; i < width - marginx; i += rad * 2.9) {
        // off-white arc
        context.fillStyle = 'hsl(0, 0%, 96%)';
        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.fill();

        // dark arc
        context.fillStyle = 'hsl(0, 0%, 8%)';
        context.beginPath();
        context.arc(i + phase[phaseIndx], j, rad, 0, Math.PI * 2, true);
        context.fill();

        phaseIndx = phaseIndx < phase.length - 1 ? phaseIndx + 1 : 0;
      }
      phaseIndx = phaseIndx < phase.length - 1 ? phaseIndx + 1 : 0;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
