const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // line settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    //offset fix
    context.translate(0.55, 0.2);

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);

    function circles(i, j, rad, step, signX, signY) {
      context.strokeStyle = 'hsl(0, 0%, 6%)';
      context.save();
      context.scale(signX, signY);

      while (rad > step) {
        context.beginPath();
        context.arc(signX * i, signY * j, rad, 0, -Math.PI / 2, true);
        context.stroke();
        rad -= step;
      }
      context.restore();
    }

    let rad = 1.3;
    for (let j = rad; j < height; j += 2 * rad) {
      for (let i = rad; i < width; i += 2 * rad) {
        // draw full circle
        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.stroke();
        context.strokeRect(i - rad, j - rad, 2 * rad, 2 * rad);

        // draw sub circles
        circles(i, j, rad, rad / 10, sign(), sign());
        circles(i, j, rad, rad / 10, sign(), sign());
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
