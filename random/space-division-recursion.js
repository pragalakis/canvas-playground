const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const randomInRange = (min, max) => min + Math.random() * (max - min);

    function line(wMin, wMax, hMin, hMax, o) {
      const x = randomInRange(wMin, wMax);
      const y = randomInRange(hMin, hMax);

      // horizontal line
      if (o === 'h') {
        context.beginPath();
        context.moveTo(wMin, y);
        context.lineTo(wMax, y);
        context.stroke();

        // recursion
        if (wMax - wMin > 1 && hMax - hMin > 1) {
          o = Math.random() >= 0.5 ? 'h' : 'v';
          line(wMin, wMax, hMin, y, o);

          o = Math.random() >= 0.5 ? 'h' : 'v';
          line(wMin, wMax, y, hMax, o);
        }
      } else {
        // vertical line
        context.beginPath();
        context.moveTo(x, hMin);
        context.lineTo(x, hMax);
        context.stroke();

        // recursion
        if (hMax - hMin > 1 && wMax - wMin > 1) {
          o = Math.random() >= 0.5 ? 'h' : 'v';
          line(wMin, x, hMin, hMax, o);

          o = Math.random() >= 0.5 ? 'h' : 'v';
          line(x, wMax, hMin, hMax, o);
        }
      }
    }

    // first function call
    const orientation = Math.random() >= 0.5 ? 'h' : 'v';
    line(0, width, 0, height, orientation);
  };
};
canvasSketch(sketch, settings);
