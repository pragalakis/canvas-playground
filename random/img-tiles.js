// inspired from @winkerVSbecks @twitter
const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

const settings = {
  animate: true,
  playbackRate: 'fixed',
  fps: 1
};

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  var gridIndex = -1;
  const gridData = [
    [120, 2], // 2x4
    [80, 3], // 3x6
    [60, 4], // 4x8
    [50, 5], // 5x10
    [40, 6], // 6x12
    [30, 8] // 8x14
  ];

  return ({ context, width, height, time }) => {
    // every x seconds draw a different grid
    if (time % 300 == 0) {
      draw();
    }

    function draw() {
      const margin = height / 10;

      gridIndex = gridIndex < gridData.length - 1 ? gridIndex + 1 : 0;
      let size = gridData[gridIndex][0];
      let grid = gridData[gridIndex][1];

      context.drawImage(image, 0, 0, width, height);

      const imgData = [];
      const points = [];

      // extract image tiles data
      // loops for grid columns
      for (let i = 0; i < 2 * grid; i++) {
        let y = 1.5 * i * size + 1.8 * margin;
        // loops for grid rows
        for (let j = 0; j < grid; j++) {
          let x = margin + size / 2 + j * 2 * size;
          imgData.push(context.getImageData(x, y, size, size));
          points.push([x, y]);
        }
      }

      function randomTile(arr, i) {
        let index = Math.floor(Math.random() * arr.length);
        // add a zero for every tile used
        // if random tile is the same as the current tile
        // and is not a zero -> run again
        return index != i && arr[index] != 0
          ? arr.splice(index, 1, 0)[0]
          : randomTile(arr, i);
      }

      // draw random image tiles on given points
      points.forEach((v, i) =>
        context.putImageData(randomTile(imgData, i), v[0], v[1])
      );
    }
  };
}, settings);
