const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;
    const size = 3;
    const margin = 1.09;

    for (let j = margin; j < height - margin; j += size + margin) {
      for (let i = margin; i < width - margin; i += size + margin) {
        // grid squares
        context.strokeRect(i, j, size, size);

        // subgrid squares
        const rep = [1, 2, 3, 4, 6, 8];
        let n = size / rep[Math.floor(Math.random() * rep.length)];
        for (let nj = j; nj < j + size; nj += n) {
          for (let ni = i; ni < i + size; ni += n) {
            context.strokeRect(ni, nj, n, n);
          }
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
