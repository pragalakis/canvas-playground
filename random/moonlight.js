const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  duration: 13,
  playbackRate: 'fixed',
  fps: 10
};

// Artwork function
const sketch = () => {
  // noise points generation
  const points = Array(1000)
    .fill()
    .map(v => {
      return [Math.random() * 29.7, Math.random() * 42];
    });

  return ({ context, width, height, time }) => {
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    // draw text
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.font = '1pt Comfortaa Light';
    context.fillText('Moonlight', 1, 3);

    // draw noise points
    context.fillStyle = 'hsl(0, 0%, 50%)';
    points.forEach(v => {
      context.fillRect(v[0], v[1], 0.03, 0.03);
    });

    context.lineWidth = 0.1;
    context.strokeStyle = 'hsl(0, 0%, 96%)';
    context.fillStyle = 'hsl(230, 43%, 56%)';

    const margin = 1;
    const size = (width - 4 * margin) / 3;
    let step = 0;
    for (let j = 4 * margin; j < height; j += size + margin) {
      for (let i = margin; i < width; i += size + margin) {
        // clip the drawing region
        context.save();
        context.beginPath();
        context.rect(i, j, size, size);
        context.clip();

        // draw circle
        context.beginPath();
        context.arc(
          i + step + time,
          j + size / 2 - Math.cos(time),
          size / 2.2,
          0,
          Math.PI * 2,
          true
        );
        context.fill();

        // draw frame
        context.strokeRect(i, j, size, size);
        context.restore();
        step += size / 12;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
