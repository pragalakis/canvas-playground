const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.strokeStyle = 'black';
    context.lineWidth = 0.03;

    const range = function(min, max) {
      return Math.random() * (max - min) + min;
    };

    const randomPoints = function(n, x1, x2, y1, y2) {
      for (let i = 0; i < n; i++) {
        context.fillRect(range(x1, x2), range(y1, y2), 0.05, 0.05);
      }
    };

    // draw points
    randomPoints(10000, 0, width, 0, height);

    const shape = function(x, y, w, h) {
      if (Math.random() >= 0.5) {
        // right side
        context.save();
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + w, y - w / 2);
        context.lineTo(x + w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();
        context.stroke();
        context.clip();
        randomPoints(300, x, x + w, y, y - h - w / 2);
        context.restore();
      } else {
        // left side
        context.save();
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w, y - w / 2);
        context.lineTo(x - w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();
        context.stroke();
        context.clip();
        randomPoints(300, x, x - w, y, y - h - w / 2);
        context.restore();
      }
    };
    const boxWidth = 1;
    const boxHeight = 2;
    const N = 28;
    const points = Array(N)
      .fill(0)
      .map((v, j) => {
        let arr = [];
        let offsetX = boxWidth;

        let total = j > N / 2 ? N - j : j;
        for (let i = 0; i < total; i++) {
          let x = offsetX - total;
          let y = j;

          arr.push([x, y]);
          offsetX += 2 * boxWidth;
        }

        return arr;
      });

    points.forEach(arr => {
      arr.forEach(v => {
        shape(width / 2 + v[0], height / 5 + v[1], boxWidth, boxHeight);
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
