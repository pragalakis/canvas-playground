const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    const size = 2;
    let rad = size / 2;

    // grid
    for (let j = rad; j < height; j += size) {
      for (let i = rad; i < width; i += size) {
        // multiple quadrants
        while (rad > 0) {
          const angle = Math.random() * Math.PI * 2;
          context.save();

          //rotation
          context.translate(i, j);
          context.rotate(angle);
          context.translate(-i, -j);

          //draw
          context.beginPath();
          context.arc(i, j, rad, 0, angle, true);
          context.stroke();

          context.restore();
          rad -= 0.2;
        }

        // reset radius
        rad = size / 2;

        if (Math.random() <= 0.3) {
          context.save();

          //rotation
          context.translate(i, j);
          context.rotate(Math.random() >= 0.5 ? Math.PI / 2 : 0);
          context.translate(-i, -j);

          // line
          context.beginPath();
          context.moveTo(i - rad, j);
          context.lineTo(i + rad, j);
          context.stroke();

          context.restore();
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
