// inspired from matt desl's work
const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');

const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'px'
};

const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    const symbols = ['▬', '‑', '‑', '-', '-', '‒', '—', '‒', '—', '▲', '●'];
    const palette = palettes[Math.floor(Math.random() * palettes.length)];

    const points = function() {
      const array = [];
      const N = 55;
      for (let i = 2; i < N; i++) {
        for (let j = 2; j < N; j++) {
          let x = i / (N - 1);
          let y = j / (N - 1);
          let radius = 0.03 + Math.abs(random.noise2D(x, y, 2) * 0.05);

          array.push([x, y, radius]);
        }
      }
      return array;
    };

    // remove points randomly
    const data = points().filter(() => Math.random() > 0.6);
    const margin = 200;

    data.forEach(v => {
      const rotation = random.noise2D(v[0], v[1]);
      const color = palette[Math.floor(Math.random() * palette.length)];
      const character = symbols[Math.floor(Math.random() * symbols.length)];
      const fontSize = v[2] * width;

      const x = lerp(margin, width - 2 * margin, v[0]);
      const y = lerp(margin, height - 1.5 * margin, v[1]);

      context.save();
      context.fillStyle = color;
      context.font = `${fontSize}px Arial`;
      context.translate(x, y);
      context.rotate(rotation);
      context.fillText(character, 0, 0);
      context.restore();
    });
  };
};

canvasSketch(sketch, settings);
