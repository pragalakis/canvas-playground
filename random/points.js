const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(0.7, 0.0);
    context.lineWidth = 0.06;

    let size = 1;

    for (let i = size + 0.5; i <= width - size; i = i + size * 2.3) {
      for (let j = size + 0.5; j <= height - size; j = j + size * 2.3) {
        let rnd = Math.random();
        let random = rnd > 0.6 ? rnd - 0.5 : rnd;
        // draw circles
        context.beginPath();
        context.arc(i, j, random, 0, Math.PI * 2, true);
        context.fillStyle = 'black';
        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
