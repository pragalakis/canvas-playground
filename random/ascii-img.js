const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background color
    context.fillStyle = 'hsl(0,0%,0%)';
    context.fillRect(0, 0, width, height);

    const bytes = 4;
    // alphabet
    const string = '!@#$%^&*()+~<?>{}';
    const word = string.split('');

    // font-size = step
    const step = 13;
    context.font = `${step}px Arial`;

    // fix offset
    context.translate(0, step / 2);

    for (let i = step; i < height - step; i += step) {
      for (let j = step; j < width - step; j += step) {
        const colorIndex = i * (width * bytes) + j * bytes;

        if (data[colorIndex] <= 20) {
          context.fillStyle = 'hsl(0,0%,20%)';
        } else if (data[colorIndex] > 20 && data[colorIndex] <= 40) {
          context.fillStyle = 'hsl(0,0%,30%)';
        } else if (data[colorIndex] > 40 && data[colorIndex] <= 60) {
          context.fillStyle = 'hsl(0,0%,40%)';
        } else if (data[colorIndex] > 60 && data[colorIndex] <= 80) {
          context.fillStyle = 'hsl(0,0%,50%)';
        } else if (data[colorIndex] > 80 && data[colorIndex] <= 100) {
          context.fillStyle = 'hsl(0,0%,60%)';
        } else if (data[colorIndex] > 100 && data[colorIndex] <= 120) {
          context.fillStyle = 'hsl(0,0%,70%)';
        } else if (data[colorIndex] > 120 && data[colorIndex] <= 140) {
          context.fillStyle = 'hsl(0,0%,80%)';
        } else if (data[colorIndex] > 140 && data[colorIndex] <= 160) {
          context.fillStyle = 'hsl(0,0%,90%)';
        } else {
          context.fillStyle = 'hsl(0,0%,100%)';
        }

        // random letter from alphabet
        const letter = word[Math.floor(Math.random() * word.length)];
        context.fillText(letter, j, i);
      }
    }
  };
});
