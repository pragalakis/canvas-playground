const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // points on a circle with noise
  const rad = 2.5;
  const N = 100;
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  const points = Array(N)
    .fill()
    .map((v, i) => {
      const phi = (i / N) * Math.PI * 2;
      const x = rad * Math.cos(phi) * Math.cos((Math.random() / 2) * sign());
      const y = rad * Math.sin(phi) * Math.cos((Math.random() / 2) * sign());
      return [x, y];
    });

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    // grid
    for (let x = rad; x < width; x += 2 * rad) {
      let noise = (Math.random() / 2) * sign();
      for (let y = rad; y < height - rad; y += rad / 7) {
        context.beginPath();
        context.moveTo(
          x + points[0][0] * Math.cos(noise),
          y + points[0][1] * Math.cos(noise)
        );
        points.forEach(v => {
          context.lineTo(
            x + v[0] * Math.cos(noise),
            y + v[1] * Math.cos(noise)
          );
        });
        context.closePath();
        context.fill();
        context.stroke();

        noise += (Math.random() / 4) * sign();
      }
    }
  };
};
canvasSketch(sketch, settings);
