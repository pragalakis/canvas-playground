const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const rad = 4;

    // grid
    for (let j = 1.8 * rad; j < height - rad; j += rad) {
      for (let i = 1.2 * rad; i < width - rad / 2; i += rad) {
        // full circle
        context.beginPath();
        context.arc(i, j, rad / 2, 0, Math.PI * 2, true);
        context.stroke();

        // half circle
        context.save();
        context.translate(i, j);
        context.rotate(Math.random() * Math.PI);
        context.translate(-i, -j);
        context.beginPath();
        context.arc(i, j, rad / 2, 0, Math.PI, true);
        context.fill();
        context.restore();
      }
    }
  };
};
canvasSketch(sketch, settings);
