const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);
    context.font = '0.5pt Monospace';
    const size = height / 9;
    let txt = 1;

    for (let j = 0; j < height; j += size) {
      // draw text
      for (let y = j; y < j + size - 0.7; y += 0.7) {
        for (let x = 0.5; x < width - 1.5; x += 0.5) {
          context.fillStyle = 'hsl(0, 0%, 8%)';
          context.fillText(String(txt), x + 0.5, y + 0.7);

          context.fillStyle = 'rgba(255,0,0,0.5)';
          context.fillText(
            String(txt),
            x + 0.5 + (sign() * Math.random()) / 10,
            y + 0.7 + (sign() * Math.random()) / 10
          );

          context.fillStyle = 'rgba(0,0,255,0.5)';
          context.fillText(
            String(txt),
            x + 0.5 + (sign() * Math.random()) / 10,
            y + 0.7 + (sign() * Math.random()) / 10
          );
        }
      }
      txt++;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
