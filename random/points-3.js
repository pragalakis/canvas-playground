const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const N = 100000;
  // calculate N points inside a ring
  const ring = Array(N)
    .fill(0)
    .map((_, i) => {
      const ring_thickness = 10;
      const random = Math.tan(i) / ring_thickness;
      const phi = Math.random() * Math.PI * 2;
      const theta = random * Math.PI;

      const x = (theta * Math.cos(phi)) / Math.log1p(random / 1.5);
      const y = (theta * Math.sin(phi)) / Math.log1p(random / 1.5);
      const rad = 2;
      const alpha = Math.random();
      return [rad * x, rad * y, alpha];
    });

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(40,90%,60%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,7%)';
    context.fillStyle = 'hsl(0,0%,7%)';
    context.lineWidth = 0.5;
    // draw points
    ring.forEach((v) => {
      context.fillStyle = `rgba(0,0,0,${v[2]})`;
      context.fillRect(width / 2 + v[0], height / 2 + v[1], 0.05, 0.05);
    });
  };
};

canvasSketch(sketch, settings);
