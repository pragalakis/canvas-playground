const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.03;

    const line = [];
    const step = 4;
    const N = 1000;
    // generate points for each line
    for (let j = -step; j < width + step; j += step) {
      const phi = Math.random() * Math.PI;
      const points = Array(N)
        .fill()
        .map((_, i) => {
          const x = Math.cos(phi + i * 0.05);
          const y = i * 0.05;
          return [j + x, y];
        });
      line.push(points);
    }

    //clip the drawing area
    context.beginPath();
    context.rect(1, 1, width - 2, height - 2);
    context.clip();

    // draw horizontal lines from a wave to another
    for (let i = 0; i < line.length - 1; i += 1) {
      let j = 0;
      while (j < N - 5) {
        context.beginPath();
        context.moveTo(line[i][j][0], line[i][j][1]);
        context.lineTo(line[i + 1][j + 5][0], line[i + 1][j + 5][1]);
        context.stroke();
        j += 2;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
