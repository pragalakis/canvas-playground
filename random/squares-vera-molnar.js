const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const r = x =>
    Math.random() >= 0.5 ? -Math.random() / x : Math.random() / x;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const square = (x, y, size) => {
      // top line
      context.beginPath();
      context.moveTo(x - size, y - size);
      for (let i = x - size; i <= x + size; i++) {
        context.lineTo(i, y - size + r(3));
      }
      context.fill();

      // bottom line
      context.beginPath();
      context.moveTo(x + size, y + size);
      for (let i = x - size; i <= x + size; i++) {
        context.lineTo(i, y + size + r(3));
      }
      context.fill();

      // left line
      context.beginPath();
      context.moveTo(x - size, y - size);
      for (let j = y - size; j <= y + size; j++) {
        context.lineTo(x - size + r(3), j);
      }
      context.fill();

      // right line
      context.beginPath();
      context.moveTo(x + size, y + size);
      for (let j = y - size; j <= y + size; j++) {
        context.lineTo(x + size + r(3), j);
      }
      context.fill();
    };

    // draw squares
    square(width / 2, height / 2, 10);
    square(width / 2, height / 2, 9);
    square(width / 2, height / 2, 8);
    square(width / 2, height / 2, 7);
    square(width / 2, height / 2, 6);
    square(width / 2, height / 2, 5);
    square(width / 2, height / 2, 4);
    square(width / 2, height / 2, 3);
    square(width / 2, height / 2, 2);
    square(width / 2, height / 2, 1);
  };
};
canvasSketch(sketch, settings);
