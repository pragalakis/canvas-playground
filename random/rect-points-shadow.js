const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    const range = (min, max) => Math.random() * (max - min) + min;

    const randomPoints = function(n, x1, x2, y1, y2) {
      for (let i = 0; i < n; i++) {
        context.fillRect(range(x1, x2), range(y1, y2), 0.05, 0.05);
      }
    };

    //draw points
    randomPoints(10000, 0, width, 0, height);

    // centering rect
    context.translate(1, 0);

    // draw top face
    randomPoints(10000, 6, 23, 15, 25);
    context.strokeRect(6, 15, 23 - 6, 25 - 15);

    // draw bottom face
    context.save();
    context.beginPath();
    context.moveTo(23, 25);
    context.lineTo(21, 27);
    context.lineTo(4, 27);
    context.lineTo(6, 25);
    context.closePath();
    context.stroke();
    context.clip();
    randomPoints(10000, 4, 23, 25, 27);
    context.restore();

    // draw left face
    context.save();
    context.beginPath();
    context.moveTo(4, 27);
    context.lineTo(6, 25);
    context.lineTo(6, 15);
    context.lineTo(4, 17);
    context.closePath();
    context.stroke();
    context.clip();
    randomPoints(6000, 4, 6, 15, 27);
    context.restore();

    const shadowPoints = function(n, x1, x2, y1, y2) {
      for (let i = 0; i < n; i++) {
        let x = range(x1, x2);
        let y = -Math.log(range(y1, y2)) + 25;
        console.log(y);
        context.fillRect(x, y, 0.05, 0.05);
      }
    };

    // draw left shadow
    const leftShadow = function(n, x1, x2, y1, y2) {
      for (let i = 0; i < n; i++) {
        let x = Math.random() * range(x1, x2);
        let y = range(y1, y2);
        context.fillRect(3 + Math.log(x), y, 0.05, 0.05);
      }
    };

    context.save();
    context.beginPath();
    context.moveTo(4, 17);
    context.lineTo(4, 27);
    context.lineTo(-1, 31);
    context.lineTo(-1, 21);
    context.closePath();
    context.clip();
    leftShadow(9000, 0, 4, 17, 31);
    context.restore();

    // draw bottom shadow
    const bottomShadow = function(n, x1, x2, y1, y2) {
      for (let i = 0; i < n; i++) {
        let x = range(x1, x2);
        let y = Math.random() * range(y1, y2);
        context.fillRect(x, 30.5 + -Math.log(y), 0.05, 0.05);
      }
    };

    context.save();
    context.beginPath();
    context.moveTo(21, 27);
    context.lineTo(4, 27);
    context.lineTo(-1, 31);
    context.lineTo(-1, height + 2);
    context.closePath();
    context.clip();
    randomPoints(10000, -2, 21, 27, height);
    bottomShadow(10000, 0, 21, 27, height);
    context.restore();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
