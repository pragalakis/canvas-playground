const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  const colors = [
    [255, 255, 255],
    [255, 0, 0],
    [0, 255, 0],
    [0, 0, 255],
    [0, 0, 0]
  ];

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.font = '0.5pt Monospace';

    for (let j = 1.2; j < height; j++) {
      for (let i = 0; i < width; i += 1.3) {
        let N = 10;
        // draw text
        while (N > 0) {
          const color = colors[Math.floor(Math.random() * colors.length)];
          context.fillStyle = `rgba(${color[0]},${color[1]},${color[2]},0.2)`;
          context.fillText(
            '404',
            i + (sign() * Math.random()) / 10,
            j + (sign() * Math.random()) / 10
          );
          N--;
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
