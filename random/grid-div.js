const canvasSketch = require('canvas-sketch');
// saw this from @Anemolito on twitter and recreated it

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    //background
    context.fillStyle = 'hsl(0,50%,60%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,6%)';
    context.strokeStyle = 'hsl(0,50%,60%)';
    context.lineWidth = 0.8;

    // grid
    const margin = 0.3;
    const size = (width - margin * 2) / 11;
    for (let j = margin * 2; j < height - margin * 2; j += size + margin) {
      for (let i = margin; i < width - margin * 2; i += size + margin) {
        context.save();

        // fill square
        context.beginPath();
        context.rect(i, j, size, size);
        context.clip();
        context.fill();

        // random line
        context.beginPath();
        if (Math.random() >= 0.5) {
          context.moveTo(i - 0.3, j + Math.random() * size);
          context.lineTo(i + 0.3 + size, j + Math.random() * size);
        } else {
          context.moveTo(i + Math.random() * size, j - 0.3);
          context.lineTo(i + Math.random() * size, j + 0.3 + size);
        }
        context.stroke();

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
