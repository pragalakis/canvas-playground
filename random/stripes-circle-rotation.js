const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);
    const drawStripes = () => {
      for (let j = 0; j < height; j += step) {
        context.beginPath();
        context.moveTo(0, j);
        context.lineTo(width, j);
        context.stroke();
      }
    };

    // settings
    let rad = 400;
    const step = 30;
    context.lineWidth = step / 2;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    // draw striped background
    drawStripes();

    while (rad >= 0) {
      context.save();

      // clip and fill circle area
      context.beginPath();
      context.arc(width / 2, height / 2, rad, 0, Math.PI * 2, true);
      context.clip();
      context.fill();

      // rotation
      context.translate(width / 2, height / 2);
      context.rotate(sign() * Math.random() * 0.2);
      context.translate(-width / 2, -height / 2);

      // draw stripes on cliped area
      drawStripes();

      context.restore();
      rad -= step;
    }
  };
};
canvasSketch(sketch, settings);
