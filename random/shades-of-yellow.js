const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.03;

    function range(min, max) {
      return Math.random() * (max - min) + min;
    }

    function randomColor() {
      return `hsl(60,${Math.floor(range(80, 100))}%,${Math.floor(
        range(15, 80)
      )}%)`;
    }

    function triangle(x, y, size) {
      context.strokeStyle = randomColor();
      context.beginPath();
      context.moveTo(x, y - (Math.sqrt(3) / 3) * size);
      context.lineTo(x - size / 2, y + (Math.sqrt(3) / 6) * size);
      context.lineTo(x + size / 2, y + (Math.sqrt(3) / 6) * size);
      context.closePath();
      context.stroke();
    }

    let N = 200000;
    const margin = 2;
    const size = 0.5;
    while (N > 0) {
      triangle(
        range(margin, width - margin - size),
        range(margin, height - margin - size),
        size
      );

      N--;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
