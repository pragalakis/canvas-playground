const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    const angles = [0, Math.PI / 2, Math.PI, -Math.PI / 2];
    const size = 2;
    const margin = 0.5;
    let rad = size / 2;

    // grid
    for (let j = margin; j < height; j += size) {
      for (let i = margin; i < width; i += size) {
        // multiple quadrants
        while (rad > 0) {
          const angle = angles[Math.floor(Math.random() * angles.length)];
          context.save();

          //rotation
          context.translate(i, j);
          context.rotate(angle);
          context.translate(-i, -j);

          //draw
          context.beginPath();
          context.arc(i, j, rad, 0, -Math.PI / 2, true);
          context.stroke();

          context.restore();
          rad -= 0.1;
        }
        rad = size / 2;
      }
    }
  };
};
canvasSketch(sketch, settings);
