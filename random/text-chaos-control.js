const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // draw background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    const text = 'CHAOS';
    const fontSize = 0.3;
    context.font = `${fontSize}pt Monospace`;
    context.fillStyle = 'hsl(0, 0%, 6%)';

    // draw chaos text
    for (let j = 0; j < height; j += 0.6) {
      for (let i = 0; i < width; i += 0.6) {
        context.save();

        // random rotation
        context.translate(i, j);
        context.rotate(Math.random() * Math.PI);
        context.translate(-i, -j);

        context.fillText(text, i, j);
        context.restore();
      }
    }

    // draw square in the center
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(width / 2 - 6, height / 2 - 6, 12, 12);

    // draw control text
    context.fillStyle = 'hsl(0, 0%, 7%)';
    const fontSize2 = 0.6;
    context.font = `${fontSize2}pt Monospace`;
    const text2 = 'CONTROL';
    const t2Width = context.measureText(text2).width;
    context.fillText(
      'CONTROL',
      width / 2 - t2Width / 2,
      height / 2 + fontSize2 / 2
    );
  };
};

// Start the sketch
canvasSketch(sketch, settings);
