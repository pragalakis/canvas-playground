// experimenting with matt desl's patchwork
const canvasSketch = require('canvas-sketch');
const clustering = require('density-clustering');
const convexHull = require('convex-hull');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 97%)';
    context.lineWidth = 0.05;

    // random point between min and max
    const range = (min, max) => Math.random() * (max - min) + min;

    // min and max values in an array of 2d points
    function getMinMaxOf2DIndex(arr, index) {
      let min = Math.min(...arr.map(v => v[index]));
      let max = Math.max(...arr.map(v => v[index]));
      return [min, max];
    }

    function draw(points, clustersNumber) {
      // k-means clustering
      const kmeans = new clustering.KMEANS();
      const clusters = kmeans.run(points, clustersNumber);

      for (let j = 0; j < clusters.length; j++) {
        const cluster = clusters[j];
        const position = cluster.map(v => points[v]);

        // find the hull of the cluster
        const edges = convexHull(position);

        // create a closed polyline from the hull
        let path = edges.map(c => position[c[0]]);

        if (path.length === 0) break;

        // cliping the shape
        context.save();
        context.beginPath();
        context.moveTo(path[0][0], path[0][1]);
        for (let i = 1; i < path.length; i++) {
          context.lineTo(path[i][0], path[i][1]);
        }
        context.closePath();
        context.stroke();
        context.clip();

        // min-max of x
        let x = getMinMaxOf2DIndex(path, 0);
        let xmin = x[0];
        let xmax = x[1];
        // min-max of y
        let y = getMinMaxOf2DIndex(path, 1);
        let ymin = y[0];
        let ymax = y[1];

        // circle generation inside the cliped area
        const N = Math.floor(range(5, 20));
        const rad = range(0.1, 1);
        for (let i = 0; i < N; i++) {
          context.beginPath();
          context.arc(
            (xmax + xmin) / 2,
            (ymax + ymin) / 2,
            rad + i * 0.2,
            0,
            Math.PI * 2,
            true
          );
          context.stroke();
        }
        context.restore();
      }
    }

    const margin = 3.5;
    const N = 2000;
    // random data points calculation
    const data = Array(N)
      .fill()
      .map(() => [
        range(margin, width - margin),
        range(margin, height - margin)
      ]);

    const clustersNumber = Math.floor(range(40, 50));
    // draw function for cluster circle generation
    draw(data, clustersNumber);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
