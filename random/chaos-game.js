const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;

    let rad = 6;
    let vertices = 2;
    let t = 0.5;

    let step = 14;
    // draw the 6 shapes
    for (let j = rad + 1; j < height; j += step) {
      for (let i = rad + 1.5; i < width; i += step) {
        // point calculation
        vertices++;
        let points = pointsOnCircle(vertices, rad);

        let x = i + points[0].x;
        let y = j + points[0].y;

        const N = 10000;
        for (let p = 0; p < N; p++) {
          // chaos game
          let r = Math.floor(Math.random() * points.length);
          x = lerp(x, i + points[r].x, t);
          y = lerp(y, j + points[r].y, t);

          // random color
          let color = `rgb(${Math.random() * 255},${Math.random() *
            255},${Math.random() * 255})`;
          context.strokeStyle = color;

          // draw rect points
          context.strokeRect(x, y, 0.01, 0.01);
        }

        t = vertices > 6 ? 0.7 : 0.6;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
