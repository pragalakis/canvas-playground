const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const offset = r =>
    Math.random() >= 0.5 ? -Math.random() / r : Math.random() / r;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;
    const size = 2;
    const margin = 0.3;

    // grid
    for (let j = 1.5 * margin; j < height - margin; j += size + margin) {
      for (let i = 0.1; i < width - margin; i += size + margin) {
        // rects
        let n = 30;
        while (n > 0) {
          // random color
          context.strokeStyle =
            palettes[Math.floor(Math.random() * palettes.length)][0];

          const r = [1, 2, 3, 4, 5][Math.floor(Math.random() * 5)];
          // draw rect
          context.beginPath();
          context.moveTo(i + offset(r), j + offset(r));
          context.lineTo(i + size + offset(r), j + offset(r));
          context.lineTo(i + size + offset(r), j + size + offset(r));
          context.lineTo(i + offset(r), j + size + offset(r));
          context.closePath(r);
          context.stroke();
          n--;
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
