const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.5;
    const text = '0123456789ABCDEF';
    context.font = `${fontSize}pt FreeMono`;
    const textW = context.measureText('0').width;

    context.fillText('HEX ', textW / 2, fontSize + 0.05);

    // random text grid
    for (let j = fontSize + 0.05; j < height; j += fontSize + 0.2) {
      let spaceIndx = 1;
      for (let i = textW / 2; i < width; i += textW) {
        if (j === fontSize + 0.05 && i <= 4 * textW + textW / 2) continue;

        if (spaceIndx % 5 === 0) {
          context.fillText(' ', i, j);
        } else {
          const t = text[Math.floor(Math.random() * (text.length - 1))];
          context.fillText(t, i, j);
        }

        spaceIndx++;
      }
    }
  };
};
canvasSketch(sketch, settings);
