const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    for (let i = 0; i < 100; i++) {
      let x = Math.floor(Math.random() * 20);
      let y = Math.floor(Math.random() * height);
      let sizeHeight = Math.floor(40 + Math.random() * 4);

      let data = context.getImageData(0, y, width, sizeHeight);
      context.putImageData(data, x, y);
    }
  };
});
