const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // steady angle
  //const angle = Math.random() >= 0.5 ? -Math.random() : Math.random();

  // different angle for each call
  const angle = () => (Math.random() >= 0.5 ? -Math.random() : Math.random());

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 100%)';
    context.fillRect(0, 0, width, height);

    // settings
    const text =
      'THERE👏 IS👏 NOT👏 SUCH👏 THING👏 AS👏 INTELLECTUAL👏 PROPERTY👏';
    const fontSize = 1.5;
    const margin = 2 * fontSize;
    context.font = `${fontSize}pt Monospace`;
    let ypos = 2.5;

    text.split(' ').forEach(t => {
      const textW = context.measureText(t).width;
      const xpos = width / 2 - textW / 2;

      context.save();

      // rotation
      context.translate(xpos + textW / 2, ypos * fontSize + 1.25);
      context.rotate(angle());
      context.translate(-xpos - textW / 2, -ypos * fontSize - 1.25);

      // text bg
      context.fillStyle = 'hsl(0, 0%, 6%)';
      context.fillRect(xpos - 0.5, ypos * fontSize, textW + 1, 2 * fontSize);

      // text
      context.fillStyle = 'hsl(0, 0%, 100%)';
      context.fillText(t, xpos, ypos * fontSize + 1.5 * fontSize);

      context.restore();

      ypos += margin;
    });
  };
};
canvasSketch(sketch, settings);
