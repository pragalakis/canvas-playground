const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,96%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.3, 0.2);
    context.fillStyle = 'rgba(0,0,0,0.8)';

    let stepX = 3;
    let stepY = 1;
    for (let i = stepX; i < width - stepX; i += stepX) {
      let p = Math.random();
      for (let j = stepY; j < height - stepY; j += stepY) {
        context.beginPath();
        context.moveTo(i, j);
        context.lineTo(i + stepX, j);
        context.lineTo(
          i + stepX / 2 + Math.sin(p + j / Math.log(i)),
          j + stepY
        );
        context.lineTo(i, j);
        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
