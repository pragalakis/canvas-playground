const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  fps: 30,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const N = 100;
  const points = Array(N)
    .fill()
    .map((v, i) => {
      const phi = Math.random() * i;
      const x = width / 2 + Math.cos(phi);
      const y = -2 + Math.sin(phi);
      const n = 4 + Math.random() * height;
      return [x, y, n];
    });

  return ({ context, width, height, time }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';

    const dot = (x, y, rad, z) => {
      context.beginPath();
      context.arc(
        x + Math.cos(5 * time + z),
        y + z + Math.cos(time),
        rad,
        0,
        Math.PI * 2,
        true
      );
      context.fill();
    };

    // draw points
    const rad = 0.5;
    points.forEach(v => {
      let n = v[2];
      while (n > 1) {
        dot(v[0], v[1], rad / 10, n);
        n--;
      }
    });
  };
};
canvasSketch(sketch, settings);
