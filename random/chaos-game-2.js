/*
https://en.wikipedia.org/wiki/Chaos_game
A point inside a pentagon repeatedly jumps half of the distance towards a randomly chosen vertex, but the currently chosen vertex cannot be the same as the previously chosen vertex.
*/
const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    let rad = 17;
    let vertices = 5;
    let points = pointsOnCircle(vertices, rad);

    let x = width / 2 + points[0].x;
    let y = height / 2 + points[0].y;

    const t = 0.5; // jumps half of the distance
    const N = 100000;
    let previous = -1;
    // chaos game
    for (let p = 0; p < N; p++) {
      let r = Math.floor(Math.random() * points.length);
      while (r == previous) {
        r = Math.floor(Math.random() * points.length);
      }
      previous = r;
      x = lerp(x, width / 2 + points[r].x, t);
      y = lerp(y, height / 2 + points[r].y, t);

      // random color
      let color = `rgb(${Math.random() * 255},${Math.random() *
        255},${Math.random() * 255})`;
      context.fillStyle = color;

      // draw rect points
      context.fillRect(x, y, 0.03, 0.03);
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
