const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';

    // random point between min and max
    const range = (min, max) => Math.random() * (max - min) + min;

    const margin = 3;
    const size = (width - 2 * margin) / 4;
    const lineSize = 0.5;
    for (let j = margin; j < height - margin * 2; j += size) {
      for (let i = margin; i < width - margin * 2; i += size) {
        context.lineWidth = '0.2';
        context.save();

        // draw tiles
        context.beginPath();
        context.rect(i, j, size, size);
        context.clip();
        context.stroke();

        // draw lines on each tile
        context.lineWidth = '0.05';
        let n = 100;
        while (n > 0) {
          context.beginPath();
          const x = range(i, i + size);
          const y = range(j, j + size);
          context.moveTo(x, y);
          context.lineTo(x + lineSize, y + lineSize);
          context.stroke();
          n--;
        }

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
