const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.03;

    function randomColor() {
      return `rgb(0,0,${Math.floor(Math.random() * 255)})`;
    }

    function range(min, max) {
      return Math.random() * (max - min) + min;
    }

    let N = 200000;
    const margin = 2;
    const size = 0.25;
    while (N > 0) {
      context.strokeStyle = randomColor();
      context.beginPath();
      context.arc(
        range(margin, width - margin - size),
        range(margin, height - margin - size),
        size,
        0,
        Math.PI * 2,
        true
      );
      context.stroke();

      N--;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
