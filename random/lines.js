const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.55, -0.6);
    context.strokeStyle = 'red';
    context.lineWidth = 0.05;

    let size = 1.5;
    let step = size / 6;
    let offset = 0.05;

    function lines(i, j, x) {
      for (let z = 0; z <= size / step; z++) {
        if (x === true) {
          // first half
          context.moveTo(i + z * step, j);
          context.lineTo(i, j + z * step);

          // second half
          context.moveTo(i + size, j + z * step);
          context.lineTo(i + z * step, j + size);
        } else {
          // first half
          context.moveTo(i + z * step, j);
          context.lineTo(i + size, j + size - z * step);

          // second half
          context.moveTo(i, j + z * step);
          context.lineTo(i + size - z * step, j + size);
        }
      }
    }

    for (let j = size; j <= height - size; j = j + size + offset) {
      for (let i = size; i <= width - size; i = i + size + offset) {
        context.beginPath();

        if (Math.random() >= 0.5) {
          lines(i, j, true);
        } else {
          lines(i, j, false);
        }

        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
