const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  fps: 30,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const palette = ['red', 'blue', 'yellow', 'green', 'pink', 'purple'];
  const data = Array(100)
    .fill()
    .map(v => {
      return {
        size: 0.5 + Math.random() * 0.5,
        x: Math.random() * width,
        y: Math.random() * height,
        rs: Math.random() * 2,
        lx: Math.random() * 4,
        ly: Math.random() * 2,
        sx: Math.random() * 2,
        sy: Math.random() * 4,
        c: palette[Math.floor(Math.random() * palette.length)]
      };
    });
  return ({ context, width, height, time }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const roundRect = (x, y, s, radius) => {
      const r = x + s;
      const b = y + s;
      context.beginPath();
      context.moveTo(x + radius, y);
      context.lineTo(r - radius, y);
      context.quadraticCurveTo(r, y, r, y + radius);
      context.lineTo(r, y + s - radius);
      context.quadraticCurveTo(r, b, r - radius, b);
      context.lineTo(x + radius, b);
      context.quadraticCurveTo(x, b, x, b - radius);
      context.lineTo(x, y + radius);
      context.quadraticCurveTo(x, y, x + radius, y);
      context.fill();
    };

    // draw
    for (let i = 0; i < data.length; i++) {
      const d = data[i];
      const x = d.x + d.lx * Math.cos(d.sx * time);
      const y = d.y + d.ly * Math.sin(d.sy * time);
      context.fillStyle = d.c;

      context.save();
      context.translate(x + data[i].size / 2, y + data[i].size / 2);
      context.rotate(data[i].rs * time);
      context.translate(-x - data[i].size / 2, -y - data[i].size / 2);
      roundRect(x, y, data[i].size, 0.2);
      context.restore();
    }
  };
};
canvasSketch(sketch, settings);
