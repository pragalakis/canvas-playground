const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    const sign = () => (Math.random() >= 0.5 ? -1 : 1);
    context.lineWidth = 60;

    // draw circles background
    let N = 40;
    while (N > 0) {
      if (N % 1 === 0) {
        context.strokeStyle = 'hsl(0, 0%, 6%)';
      } else {
        context.strokeStyle = 'hsl(0, 0%, 96%)';
      }
      context.beginPath();
      context.arc(width / 2, height / 2, N * 116, 0, Math.PI * 2, true);
      context.stroke();
      N -= 0.5;
    }

    // glitch
    let step = 400 * Math.random() + 200;
    for (let j = 0; j < height; j += step) {
      let imagedata = context.getImageData(0, j, width, step);
      context.putImageData(imagedata, 100 * Math.random() * sign(), j);
      step = 400 * Math.random() + 200;
    }

    // draw border
    context.lineWidth = 300;
    context.strokeStyle = 'hsl(0, 0%, 96%)';
    context.strokeRect(0, 0, width, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
