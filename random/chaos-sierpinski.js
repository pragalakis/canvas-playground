const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;

    // starting points
    const side = 28;
    // point A
    const x1 = width / 2;
    const y1 = height / 2 - (Math.sqrt(3) / 3) * side;
    // point B
    const x2 = width / 2 - side / 2;
    const y2 = height / 2 + (Math.sqrt(3) / 6) * side;
    // point C
    const x3 = width / 2 + side / 2;
    const y3 = height / 2 + (Math.sqrt(3) / 6) * side;

    let x = x1;
    let y = y1;

    const N = 10000;
    const t = 0.5; // half distance
    for (let i = 0; i < N; i++) {
      let r = Math.floor(Math.random() * 3);
      if (r == 0) {
        x = lerp(x, x1, t);
        y = lerp(y, y1, t);
      } else if (r == 1) {
        x = lerp(x, x2, t);
        y = lerp(y, y2, t);
      } else if (r == 2) {
        x = lerp(x, x3, t);
        y = lerp(y, y3, t);
      }

      // random color
      let color = `rgb(${Math.random() * 255},${Math.random() *
        255},${Math.random() * 255})`;
      context.strokeStyle = color;

      // draw rect points
      context.strokeRect(x, y, 0.01, 0.05);
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
