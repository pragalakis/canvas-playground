const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,94%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.4, -0.4);
    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    let size = 0.8;
    let offset = size - size / 2;
    let rad = size / 4;

    const lines = function() {
      for (let j = size; j < height - size; j += offset) {
        for (let i = size; i < width - size; i += offset) {
          // random point on given rad
          let angle = Math.random() * Math.PI * 2;
          let x = Math.cos(angle) * rad;
          let y = Math.sin(angle) * rad;

          let noise = random.noise2D(i * 0.5, j * 0.5);
          if (noise <= -0.47) {
            context.strokeStyle = '#F05837';
          } else if (noise > -0.47 && noise <= -0.2) {
            context.strokeStyle = '#F3E96B';
          } else if (noise > -0.2 && noise <= 0.1) {
            context.strokeStyle = '#6975A6';
          } else if (noise > 0.1 && noise <= 0.42) {
            context.strokeStyle = '#F28A30';
          } else {
            context.strokeStyle = '#6465A5';
          }

          // draw line
          context.beginPath();
          context.moveTo(i + size / 2 + x, j + size / 2 + y);
          context.lineTo(i + size / 2 - x, j + size / 2 - y);
          context.stroke();
        }
      }
    };

    lines();
  };
};
// Start the sketch
canvasSketch(sketch, settings);
