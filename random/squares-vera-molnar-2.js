const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const r = x =>
    Math.random() >= 0.5 ? -Math.random() / x : Math.random() / x;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const size = 2;
    const margin = 0.4;

    // grid
    for (let j = 2 * margin; j < height - size; j += size + margin) {
      for (let i = 1.8 * margin; i < width - margin; i += size + margin) {
        // draw square with random offset
        context.fillRect(i + r(2), j + r(2), size, size);
      }
    }
  };
};
canvasSketch(sketch, settings);
