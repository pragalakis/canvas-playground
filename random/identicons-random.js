const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const palette = [
    '#fc3a51',
    '#66b6ab',
    '#61ada0',
    '#2fb8ac',
    '#f85931',
    '#2a2829',
    '#e15e32',
    '#ff847c',
    '#b38184',
    '#64908a',
    '#f2e9e1',
    '#ef746f',
    '#aa00ff',
    '#412e28',
    '#93a42a',
    '#9d9d93',
    '#535233',
    '#8b7a52',
    '#f03c02',
    '#26ade4',
    '#4f2958',
    '#dc5b3e',
    '#363636',
    '#a82743',
    '#f10c49',
    '#cd8c52',
    '#30261c',
    '#fe4365',
    '#3a111c',
    '#a3a948',
    '#951f2b',
    '#e94e87',
    '#93577b',
    '#468944',
    '#a63c4a',
    '#4c3a37',
    '#637763',
    '#776363',
    '#8e4f89',
    '#4269a5',
    '#39825a',
    '#dc6141',
    '#5E0B15',
    '#90323D',
    '#D9CAB3',
    '#BC8034',
    '#59005C',
    '#8D0E8D',
    '#C20A3A'
  ];
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    const size = 2;
    const size2 = size / 2 / 4;
    const margin = 0.44;

    const drawSquares = (squares, x, y) => {
      squares.forEach(v => {
        context.fillRect(x + v[0], y + v[1], size2, size2);
        context.strokeRect(x + v[0], y + v[1], size2, size2);
      });
    };

    for (let j = size / 2 + margin; j < height; j += size + margin) {
      for (let i = size / 2 + margin; i < width; i += size + margin) {
        // random color
        let color = palette[Math.floor(Math.random() * palette.length)];
        context.fillStyle = color;
        context.strokeStyle = color;

        // all possible squares positions
        const grid = [
          [0, 0],
          [size2, 0],
          [2 * size2, 0],
          [3 * size2, 0],
          [0, size2],
          [0, 2 * size2],
          [0, 3 * size2],
          [size2, size2],
          [2 * size2, size2],
          [3 * size2, size2],
          [size2, 2 * size2],
          [2 * size2, 2 * size2],
          [3 * size2, 2 * size2],
          [size2, 3 * size2],
          [2 * size2, 3 * size2],
          [3 * size2, 3 * size2]
        ];

        // calculate random squares positions
        let numberOfSquares = Math.floor(1 + Math.random() * (grid.length - 1));
        const squares = [];
        while (numberOfSquares > 0) {
          const squareIndx = Math.floor(Math.random() * grid.length);
          squares.push(grid[squareIndx]);
          grid.splice(squareIndx, 1);
          numberOfSquares--;
        }

        /* draw and mirror square areas */
        // bottom right
        drawSquares(squares, i, j);

        // top right
        context.save();
        context.translate(i, j);
        context.scale(1, -1);
        context.translate(-i, -j);
        drawSquares(squares, i, j);
        context.restore();

        // bottom left
        context.save();
        context.translate(i, j);
        context.scale(-1, 1);
        context.translate(-i, -j);
        drawSquares(squares, i, j);
        context.restore();

        // top left
        context.save();
        context.translate(i, j);
        context.scale(-1, -1);
        context.translate(-i, -j);
        drawSquares(squares, i, j);
        context.restore();
      }
    }
  };
};
canvasSketch(sketch, settings);
