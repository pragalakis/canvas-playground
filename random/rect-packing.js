const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 97%)';
    context.lineWidth = 0.05;

    // distance calculation
    const dist = function(x1, x2, y1, y2) {
      return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    };

    // random point between min and max
    const range = (min, max) => Math.random() * (max - min) + min;

    const margin = 1;
    const maxSize = 3;
    const minSize = 0.1;
    let points = [];

    const N = 300000;
    for (let i = 0; i < N; i++) {
      let intersects = false;

      let size = range(minSize, maxSize);
      let x = range(0 + size + margin, width - size - margin);

      let y = range(0 + size + margin, height - size - margin);
      for (let j = 0; j < points.length; j++) {
        // check for every rect in the points array
        // if the current rect intersects
        let d = dist(x, points[j][0], y, points[j][1]);
        if (d < size + points[j][2]) {
          intersects = true;
        }
      }

      if (!intersects) points.push([x, y, size]);
    }

    // draw rects
    points.forEach(v => {
      if (v[2] < maxSize / 5) {
        let size = v[2];
        context.save();
        context.translate(v[0] + size / 2, v[1] + size / 2);
        context.strokeRect(-size / 2, -size / 2, size, size);
        context.restore();
      }
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
