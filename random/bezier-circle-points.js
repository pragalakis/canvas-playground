const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const x = width / 2;
    const y = height / 2;
    const p = pointsOnCircle(200);
    const rnd = () => (Math.random() >= 0.5 ? Math.random() : -Math.random());
    let prx = 0;
    let pry = 0;

    // draw bezier curves from each point of a circle to another
    for (let i = 1; i < p.length - 1; i++) {
      let rad = 12;
      context.beginPath();
      context.moveTo(x + p[i].x * rad + prx, y + p[i].y * rad + pry);
      while (rad > 2) {
        let rx = rnd();
        let ry = rnd();

        context.bezierCurveTo(
          x + p[i + 1].x * (rad - 1) + rnd(),
          y + p[i + 1].y * (rad - 1) + rnd(),
          x + p[i - 1].x * (rad - 2) + rnd(),
          y + p[i - 1].y * (rad - 2) + rnd(),
          x + p[i].x * (rad - 3) + rx,
          y + p[i].y * (rad - 3) + ry
        );

        prx = rx;
        pry = ry;
        rad -= 3;
      }
      context.stroke();
    }
  };
};
canvasSketch(sketch, settings);
