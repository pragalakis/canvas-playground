const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const points = [];
  const size = 3;
  const margin = 1.1;
  const step = size + margin;
  // point generation
  for (let j = margin; j < 42; j += step) {
    for (let i = margin; i < 29.7; i += step) {
      points.push([i, j]);
    }
  }

  const sign = () => (Math.random() >= 0.5 ? -1 : 1);

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    // line settings
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;

    points.forEach((v, i) => {
      // same color per line
      if (i % 7 === 0) {
        context.strokeStyle = `hsl(0, 50%, ${60 + i * 0.5}%)`;
      }

      context.save();

      // rotation
      context.translate(v[0] + size / 2, v[1] + size / 2);
      context.rotate(Math.random() * 0.1 * sign());
      context.translate(-v[0] - size / 2, -v[1] - size / 2);

      // draw rect
      context.strokeRect(v[0], v[1], size, size);

      context.restore();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
