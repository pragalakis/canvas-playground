const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);

    for (let j = 0; j <= height; j++) {
      let y = Math.random() * 5;
      for (let i = 0; i <= width; i++) {
        let x = Math.random() * 5;
        context.fillStyle = `rgb(${Math.random() * 255},0,0,1)`;
        context.fillRect(i, j, x, y);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
