const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';

    const corner = x => {
      switch (x) {
        case 0:
          return [0, 0];
          break;
        case 1:
          return [size, 0];
          break;
        case 2:
          return [0, size];
          break;
        case 3:
          return [size, size];
          break;
      }
    };

    function draw(i, j, rad, step) {
      context.save();

      // clip rect area
      context.beginPath();
      context.rect(i, j, rad, rad);
      context.stroke();
      context.clip();

      // draw circles on random corner
      let co = corner(Math.floor(Math.random() * 4));
      while (rad > step) {
        context.beginPath();
        context.arc(i + co[0], j + co[1], rad, 0, Math.PI * 2, true);

        // random color
        if (Math.random() >= 0.5) {
          context.fillStyle = 'hsl(0, 0%, 6%)';
          context.strokeStyle = 'hsl(0, 0%, 96%)';
        } else {
          context.fillStyle = 'hsl(0, 0%, 96%)';
          context.strokeStyle = 'hsl(0, 0%, 6%)';
        }
        context.fill();
        context.stroke();
        rad -= step;
      }
      context.restore();
    }

    let size = 3;
    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        draw(i, j, size, size / 10);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
