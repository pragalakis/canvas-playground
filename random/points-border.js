const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,3%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'white';
    context.fillStyle = 'white';
    context.lineWidth = 0.05;

    // draw border
    const margin = 1;
    context.rect(margin, margin, width - 2 * margin, height - 2 * margin);
    context.stroke();
    context.clip();

    // draw N points
    const N = 10000;
    for (let i = 0; i < N; i++) {
      context.beginPath();
      context.arc(
        Math.random() * width,
        Math.random() * height,
        0.03,
        0,
        Math.PI * 2,
        true
      );
      context.fill();
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
