const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const angles = [0, Math.PI, Math.PI / 2, -Math.PI / 2];
    const size = width / 12;

    // grid
    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        const rnd = Math.random();

        if (rnd <= 0.2) {
          // full circle
          context.beginPath();
          context.arc(
            i + size / 2,
            j + size / 2,
            size / 2,
            0,
            Math.PI * 2,
            true
          );
          context.stroke();
        } else if (rnd > 0.2 && rnd <= 0.4) {
          // square
          context.strokeRect(i, j, size, size);

          // cross line
          if (Math.random() >= 0.5) {
            context.beginPath();
            if (Math.random() >= 0.5) {
              context.moveTo(i, j);
              context.lineTo(i + size, j + size);
            } else {
              context.moveTo(i + size, j);
              context.lineTo(i, j + size);
            }
            context.stroke();
          }
        } else if (rnd > 0.4 && rnd <= 0.6) {
          // forth of a circle
          const centers = [
            [i, j],
            [i + size, j],
            [i + size, j + size],
            [i, j + size]
          ];
          const cCenter = centers[Math.floor(Math.random() * centers.length)];
          context.save();
          context.beginPath();
          context.rect(i, j, size, size);
          context.clip();

          context.beginPath();
          context.arc(cCenter[0], cCenter[1], size, 0, Math.PI * 2, true);
          context.stroke();
          context.restore();
        } else {
          // rotation
          const angle = angles[Math.floor(Math.random() * angles.length)];
          context.save();
          context.translate(i + size / 2, j + size / 2);
          context.rotate(angle);
          context.translate(-i - size / 2, -j - size / 2);

          if (Math.random() >= 0.5) {
            // half circle
            context.beginPath();
            context.arc(i + size / 2, j + size / 2, size / 2, 0, Math.PI, true);
          } else {
            // triangle
            context.moveTo(i, j);
            context.lineTo(i + size, j + size);
            context.lineTo(i, j + size);
          }

          context.closePath();
          context.stroke();
          context.restore();
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
