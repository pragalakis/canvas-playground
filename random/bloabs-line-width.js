const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';

    function points(rad) {
      context.lineWidth = lw;
      // bloaby circle-points generation
      const N = 127;
      const data = Array(N)
        .fill()
        .map((v, i) => {
          const point = 0.05 * i;
          const noise = random.noise1D(point) * rad * 0.05;
          const phi = point + Math.PI * 2;

          const x = rad * Math.cos(phi) + noise;
          const y = rad * Math.sin(phi) + noise;
          const p = Math.random() < 0.02 ? true : false;
          return [x, y, p];
        })
        .slice(0, N - 3);

      // draw circle-bloab
      context.beginPath();
      data.forEach(v => {
        context.lineTo(width / 1.4 + v[0], height / 1.4 + v[1]);
      });

      context.closePath();
      context.stroke();
    }

    let rad = 37;
    let lw = 1.1;
    // draw circles-bloabs with different rad and line width
    while (rad > 0) {
      points(rad, lw);
      rad -= 2;
      lw -= 0.05;
    }
  };
};
canvasSketch(sketch, settings);
