const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const r = () =>
    Math.random() >= 0.5 ? -Math.random() / 4 : Math.random() / 4;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    let stepx = 0.3;
    let stepy = 0.5;

    // noise vertical lines
    for (let i = 0; i < width; i += 2 * stepx) {
      stepy = 0.5;
      context.beginPath();
      context.moveTo(i, 0);
      for (let j = 0; j <= height + 0.5; j += stepy) {
        if (j > height / 2 + 2) {
          stepy = 0.1;
        }
        if (j > height / 2 + 10) {
          stepy = 0.5;
        }

        context.lineTo(i + r(), j);
      }
      context.stroke();
      context.fill();
    }

    // horizontal separator lines
    context.lineWidth = 0.4;
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.beginPath();
    context.moveTo(0, height / 2 + 2.5);
    context.lineTo(width, height / 2 + 2.5);
    context.stroke();

    context.beginPath();
    context.moveTo(0, height / 2 + 10.5);
    context.lineTo(width, height / 2 + 10.5);
    context.stroke();
  };
};
canvasSketch(sketch, settings);
