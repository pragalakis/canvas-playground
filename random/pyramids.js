const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.3, 0);
    context.lineWidth = 0.08;

    let size = 2;

    function steps() {
      return Math.floor(Math.random() * 6);
    }

    function lines(x, i, j) {
      switch (x) {
        case 0:
          // up
          context.moveTo(i + size / 2, j + size / 4);
          // left
          context.lineTo(i + size / 4, j + size - size / 4);
          break;
        case 1:
          // up
          context.moveTo(i + size / 2, j + size / 4);
          // right
          context.lineTo(i + size - size / 4, j + size - size / 4);
          break;
        case 2:
          // up
          context.moveTo(i + size / 2, j + size / 4);
          // middle
          context.lineTo(i + size / 2, j + size - size / 2.5);
          break;
        case 3:
          // middle
          context.moveTo(i + size / 2, j + size - size / 2.5);
          // left
          context.lineTo(i + size / 4, j + size - size / 4);
          break;
        case 4:
          // middle
          context.moveTo(i + size / 2, j + size - size / 2.5);
          // right
          context.lineTo(i + size - size / 4, j + size - size / 4);
          break;
        case 5:
          // left
          context.moveTo(i + size / 4, j + size - size / 4);
          // right
          context.lineTo(i + size - size / 4, j + size - size / 4);
          break;
      }
    }

    for (let i = size; i <= width - size; i += size) {
      for (let j = size; j < height - size; j += size) {
        context.beginPath();
        let x = steps();
        for (let z = 0; z <= x; z++) {
          lines(steps(), i, j);
        }
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
