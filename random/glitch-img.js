const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// Artwork function
canvasSketch(async ({ update }) => {
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: 'A3',
    pixelsPerInch: 300,
    units: 'px'
  });

  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    // glitch
    const glitch = (n, size) => {
      while (n > 0) {
        const imagedata = context.getImageData(
          Math.random() * (size + width - 2 * size),
          Math.random() * (size + height - 2 * size),
          size,
          size
        );
        context.putImageData(
          imagedata,
          Math.random() * width,
          Math.random() * height
        );
        n--;
      }
    };

    glitch(5, 800);
    glitch(15, 600);
    glitch(20, 400);
    glitch(15, 200);
    glitch(30, 100);
    glitch(40, 50);
    glitch(80, 10);

    // draw border
    context.lineWidth = 300;
    context.strokeStyle = 'hsl(0, 0%, 96%)';
    context.strokeRect(0, 0, width, height);
  };
});
