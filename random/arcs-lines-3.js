const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 08%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'rgba(255,255,255,0.3)';
    context.lineWidth = 0.03;

    function randomRange(min, max) {
      return Math.random() * (max - min) + min;
    }

    const N = 50;
    // generate points inside a random arc
    const data = Array(N)
      .fill(0)
      .map(v => {
        let arc = [];
        let random = randomRange(0.3, 1.0);
        let rad = random * 3;

        const lines = [2, 10];
        const indx = Math.floor(Math.random() * lines.length);

        for (let i = 0; i < lines[indx]; i += 0.2) {
          let phi = Math.log(i) + randomRange(0.3, 0.31) * Math.PI * 2;
          let x = Math.cos(phi);
          let y = Math.sin(phi);

          arc.push([x * rad, y * rad]);
        }

        return arc;
      });

    // draw the arcs
    for (let i = 0; i < data.length; i++) {
      let x = randomRange(7, width - 7);
      let y = randomRange(7, height - 7);
      let random = Math.random();

      // draw the lines
      data[i].forEach(v => {
        context.beginPath();
        context.moveTo(x + v[0], y + v[1]);

        // this is where magic happens
        if (random >= 0.5) {
          context.lineTo(x + v[0], y - v[1]);
        } else {
          context.lineTo(x - v[0], y + v[1]);
        }
        context.stroke();
      });
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
