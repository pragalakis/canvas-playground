const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.01;

    function randomColor(c) {
      if (c === 'r') {
        return `rgba(${Math.floor(Math.random() * 255)}, 0, 0,1)`;
      } else if (c === 'g') {
        return `rgba(0,${Math.floor(Math.random() * 255)}, 0, 1)`;
      } else if (c === 'b') {
        return `rgba(0,0,${Math.floor(Math.random() * 255)}, 1)`;
      }
    }

    function range(min, max) {
      return Math.random() * (max - min) + min;
    }

    function triangle(x, y, size) {
      context.beginPath();
      context.moveTo(x, y - (Math.sqrt(3) / 3) * size);
      context.lineTo(x - size / 2, y + (Math.sqrt(3) / 6) * size);
      context.lineTo(x + size / 2, y + (Math.sqrt(3) / 6) * size);
      context.closePath();
      context.stroke();
    }

    let N = 200000;
    const margin = 2;
    const size = 0.1;
    while (N > 0) {
      // square
      context.strokeStyle = randomColor('r');
      context.strokeRect(
        range(margin, width - margin - size),
        range(margin, height - margin - size),
        size,
        size
      );

      // circle
      context.strokeStyle = randomColor('g');
      context.beginPath();
      context.arc(
        range(margin, width - margin - size),
        range(margin, height - margin - size),
        size,
        0,
        Math.PI * 2,
        true
      );
      context.stroke();

      // triangle
      context.strokeStyle = randomColor('b');
      triangle(
        range(margin, width - margin - size),
        range(margin, height - margin - size),
        size
      );

      N--;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
