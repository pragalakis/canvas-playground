const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const range = (min, max) => Math.random() * (max - min) + min;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,6%)';
    context.lineWidth = 0.05;

    const drawTile = (i, j, size) => {
      let x = range(i, i + size);
      context.beginPath();
      context.moveTo(x, j);
      context.lineTo(x, j + size);
      context.stroke();

      x = range(i, i + size);
      context.beginPath();
      context.moveTo(x, j);
      context.lineTo(x, j + size);
      context.stroke();

      let y = range(j, j + size);
      context.beginPath();
      context.moveTo(i, y);
      context.lineTo(i + size, y);
      context.stroke();

      y = range(j, j + size);
      context.beginPath();
      context.moveTo(i, y);
      context.lineTo(i + size, y);
      context.stroke();
    };

    let size = 5;
    const stepX = width / 2;
    const stepY = height / 2;
    // 4x4 grid and each tile is a size x size grid
    for (let y = 0; y < height; y += stepY) {
      for (let x = 0; x < width; x += stepX) {
        size -= 1.15;
        for (let j = y; j < y + stepY; j += size) {
          for (let i = x; i < x + stepX; i += size) {
            drawTile(i, j, size);
          }
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
