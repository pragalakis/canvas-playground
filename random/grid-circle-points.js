const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const margin = 0.5;
    const size = (width - 13 * margin) / 12;
    const rad = size - size / 3;

    // random circle points function
    const points = angle => {
      return Array(1500)
        .fill()
        .map(v => {
          const phi = Math.random() * angle;
          const rnd = Math.random();
          const x = Math.cos(phi) * rnd;
          const y = Math.sin(phi) * rnd;
          return [rad * x, rad * y];
        });
    };

    // grid
    for (let j = margin; j < height - 2 * margin; j += size + margin) {
      for (let i = margin; i < width - 2 * margin; i += size + margin) {
        context.save();

        // draw and clip square
        context.beginPath();
        context.rect(i, j, size, size);
        context.clip();
        context.stroke();

        // draw random circle points
        points(Math.random() * Math.PI * 2).forEach(v => {
          context.beginPath();
          context.arc(
            i + size / 2 + v[0],
            j + size / 2 + v[1],
            0.01,
            0,
            Math.PI * 2,
            true
          );
          context.fill();
        });

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
