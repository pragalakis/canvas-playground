const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.1;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const angles = [0, Math.PI, Math.PI / 2, -Math.PI / 2];
    const size = 2;

    // grid
    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        // tile
        context.strokeRect(i, j, size, size);

        let angle = angles[Math.floor(Math.random() * angles.length)];
        // lines
        for (let l = i; l < i + size; l += 0.2) {
          context.save();

          // rotation
          context.translate(i + size / 2, j + size / 2);
          context.rotate(angle);
          context.translate(-i - size / 2, -j - size / 2);

          // line
          context.beginPath();
          context.moveTo(l, j);
          context.lineTo(l, j + l - i);
          context.stroke();

          context.restore();
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
