const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(0.4, 0.5);
    context.lineWidth = 0.05;

    // draw circle
    function circle() {
      context.beginPath();
      context.arc(
        width * Math.random(),
        height * Math.random(),
        2.75 + Math.random(),
        0,
        Math.PI * 2,
        true
      );
      context.fillStyle = 'red';
      context.fill();
    }

    circle();
    circle();

    let size = 0.5;
    let offset = 0.1;

    function random() {
      let rnd = Math.random();
      return rnd < 0.5 ? 0.5 : rnd;
    }

    // draw lines
    for (let i = offset; i <= width - size; i = i + size + offset) {
      for (let j = offset; j <= height - size; j = j + size + offset) {
        context.beginPath();
        context.moveTo(i, j);

        let rnd = Math.random();

        if (rnd >= 0.5) {
          context.lineTo(i - random(), j);
        } else {
          context.lineTo(i + random(), j);
        }
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
