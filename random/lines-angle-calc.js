const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const angle = (cx, cy, ex, ey) => {
    const dx = ex - cx;
    const dy = ey - cy;
    const theta = Math.atan2(dy, dx);
    return theta;
  };

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    // grid
    const step = 2;
    for (let j = 0; j < height; j += step) {
      for (let i = 0; i < width; i += step) {
        // clip square area
        context.save();
        context.beginPath();
        context.rect(i, j, step, step);
        context.clip();

        let n = 20; // number of lines
        let o = Math.random() >= 0.5 ? true : false; // orientation
        const cx = i + step / 2; // mid point x
        const cy = j + step / 2; // mid point y

        /* draw lines
         * given a starting point and a mid point
         * we calculate the last point using the angle function
          */
        while (n > 0) {
          let x, y;
          if (o) {
            x = i + Math.random() * step;
            y = j;
          } else {
            x = i;
            y = j + Math.random() * step;
          }

          const a = angle(cx, cy, x, y);

          // line
          context.beginPath();
          context.moveTo(x, y);
          context.lineTo(
            x + 2 * step * -Math.cos(a),
            y + 2 * step * -Math.sin(a)
          );
          context.stroke();

          n--;
        }
        context.restore();
      }
    }
  };
};
canvasSketch(sketch, settings);
