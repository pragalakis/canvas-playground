const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;
    const margin = 0.0;
    const size = 1;

    let k = 0;
    for (let i = 0; i < width; i += size) {
      context.beginPath();
      context.moveTo(i, 0);
      context.lineTo(i, height);
      context.stroke();
      k = k + 0.1;
      for (let j = 0; j < height; j += 0.05 + margin) {
        let n = Math.random();
        context.beginPath();
        context.moveTo(i - (size * Math.cos(n * k)) / 2, j);
        context.lineTo(i + (size * Math.cos(n * k)) / 2, j);
        context.stroke();
      }
    }
  };
};
canvasSketch(sketch, settings);
