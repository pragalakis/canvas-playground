const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);
    context.translate(-0.13, 0);

    let size = 1;
    let colors = ['black', 'white'];

    function circles(i, j) {
      let rnd = Math.random();
      let angle = 1.57;
      let color = Math.floor(Math.random() * colors.length);

      // background color
      context.fillStyle = `${colors[color]}`;
      context.fillRect(i, j, size, size);

      // circle
      context.beginPath();
      context.save();
      if (rnd < 0.25) {
        context.translate(0, size / 2);
        context.arc(i, j, size * 0.5, angle, angle + Math.PI, true);
      } else if (rnd >= 0.25 && rnd < 0.5) {
        context.translate(size / 2, 0);
        context.arc(i, j, size * 0.5, 2 * angle, 2 * angle + Math.PI, true);
      } else if (rnd >= 0.5 && rnd < 0.75) {
        context.translate(size, size / 2);
        context.arc(i, j, size * 0.5, 3 * angle, 3 * angle + Math.PI, true);
      } else {
        context.translate(size / 2, size);
        context.arc(i, j, size * 0.5, 0, Math.PI, true);
      }

      context.restore();

      // reverse color
      color = color == 1 ? 0 : 1;
      context.fillStyle = `${colors[color]}`;
      context.fill();
    }

    for (let j = size; j < height - size; j += size) {
      for (let i = size; i <= width - size; i += size) {
        circles(i, j);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
