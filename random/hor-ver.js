const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.2, 0);
    context.strokeStyle = 'black';
    context.lineWidth = 0.1;

    function lines(i, j, rotate) {
      if (rotate) {
        context.moveTo(i + 0, j);
        context.lineTo(i + 0, j + 1.5);

        context.moveTo(i + 0.5, j);
        context.lineTo(i + 0.5, j + 1.5);

        context.moveTo(i + 1, j);
        context.lineTo(i + 1, j + 1.5);

        context.moveTo(i + 1.5, j);
        context.lineTo(i + 1.5, j + 1.5);
      } else {
        context.moveTo(i, j + 0);
        context.lineTo(i + 1.5, j + 0);

        context.moveTo(i, j + 0.5);
        context.lineTo(i + 1.5, j + 0.5);

        context.moveTo(i, j + 1);
        context.lineTo(i + 1.5, j + 1);

        context.moveTo(i, j + 1.5);
        context.lineTo(i + 1.5, j + 1.5);
      }
    }

    let offset = 1.5;
    for (let j = offset; j < height - offset; j += offset) {
      for (let i = offset; i <= width - offset; i += offset) {
        context.beginPath();
        let rotate = Math.random() > 0.5 ? true : false;
        lines(i, j, rotate);
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
