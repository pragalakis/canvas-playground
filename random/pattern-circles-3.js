const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 1;

    // fix offset
    context.translate(11, 11);

    const rad = 8;

    // draw circle
    function circle(x, y) {
      context.beginPath();
      context.arc(x, y, rad, 0, Math.PI * 2, true);
      context.fill();
    }

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);

    let d = 10;
    for (let j = 0; j < height; j += 50) {
      for (let i = 0; i < width; i += 50) {
        let noise = random.noise2D(i, j);
        if (noise > 0) {
          context.fillStyle = 'rgba(255, 0, 0, 0.8)';
          circle(
            i + Math.random() * sign() * d,
            j + Math.random() * sign() * d
          );
          context.fillStyle = 'rgba(0, 0, 255, 0.8)';
          circle(
            i + Math.random() * sign() * d,
            j + Math.random() * sign() * d
          );
          context.fillStyle = 'rgba(255, 255, 0, 0.8)';
          circle(
            i + Math.random() * sign() * d,
            j + Math.random() * sign() * d
          );
          context.fillStyle = 'rgba(0, 0, 0, 0.8)';
          circle(i + 2 * d, j - d);
        } else {
          context.fillStyle = 'rgba(255, 0, 0, 0.8)';
          circle(i, j + d);

          context.fillStyle = 'rgba(0, 0, 255, 0.8)';
          circle(i + d, j + d);

          context.fillStyle = 'rgba(255, 255, 0, 0.8)';
          circle(i + 2 * d, j);

          context.fillStyle = 'rgba(0, 0, 0, 0.8)';
          circle(i + 2 * d, j - d);
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
