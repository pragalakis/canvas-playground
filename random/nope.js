const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 64
};

// Artwork function
const sketch = () => {
  const circles = [];
  circles.push(pointsOnCircle(4, 1));
  circles.push(pointsOnCircle(5, 2));
  circles.push(pointsOnCircle(1, 3));
  circles.push(pointsOnCircle(15, 4));
  circles.push(pointsOnCircle(2, 5));
  circles.push(pointsOnCircle(25, 6));
  circles.push(pointsOnCircle(3, 7));
  circles.push(pointsOnCircle(35, 8));
  circles.push(pointsOnCircle(4, 9));
  circles.push(pointsOnCircle(45, 10));
  circles.push(pointsOnCircle(5, 11));
  circles.push(pointsOnCircle(55, 12));
  circles.push(pointsOnCircle(6, 13));
  circles.push(pointsOnCircle(65, 14));
  circles.push(pointsOnCircle(7, 15));
  circles.push(pointsOnCircle(75, 16));
  circles.push(pointsOnCircle(8, 17));
  circles.push(pointsOnCircle(85, 18));
  circles.push(pointsOnCircle(9, 19));

  return ({ context, width, height, time }) => {
    if (time % 0.5 === 0) {
      // draw background
      context.fillStyle = 'hsl(0, 0%, 6%)';
      context.fillRect(0, 0, width, height);

      // draw text
      context.fillStyle = 'hsl(0, 0%, 96%)';
      context.font = '2pt Bitstream Vera Sans';
      context.fillText('NOPE', 0.2, height - 3);
      context.fillText('11/4', 23.5, height - 3);

      // draw borders
      context.lineWidth = 0.5;
      context.strokeStyle = 'hsl(0, 0%, 96%)';
      context.moveTo(0, height - 5.5);
      context.lineTo(width, height - 5.5);
      context.stroke();

      context.moveTo(0, height - 2.5);
      context.lineTo(width, height - 2.5);
      context.stroke();
    }

    context.fillStyle = 'hsl(0, 0%, 97%)';

    let rad = 0.1;
    circles.forEach((circle, i) => {
      rad += 0.01;
      context.save();
      context.translate(width / 2, height / 3);
      context.rotate(Math.PI * time);
      context.translate(-width / 2, -height / 3);
      circle.forEach(v => {
        context.beginPath();
        context.arc(
          width / 2 + v.x,
          height / 3 + v.y,
          rad / 3,
          0,
          Math.PI * 2,
          true
        );
        context.fill();
      });
      context.restore();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
