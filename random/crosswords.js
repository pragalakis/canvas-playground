const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    context.translate(-0.13, 0);
    context.fillStyle = 'black';
    context.strokeStyle = 'black';
    context.lineWidth = 0.1;

    // prettier-ignore
    const alphabet = ['Α','Β','Γ','Δ','Ε','Ζ','Η','Θ','Ι','Κ','Λ','Μ','Ν','Ξ','Ο','Π','Ρ','Σ','Τ','Υ','Φ','Χ','Ψ','Ω']

    let size = 1;
    context.font = `${size - size / 4}px Arial`;

    for (let j = size; j < height - size; j += size) {
      for (let i = size; i <= width - size; i += size) {
        let letter = Math.floor(Math.random() * alphabet.length);

        if (Math.random() < 0.3) {
          context.fillRect(i, j, size, size);
          context.strokeRect(i, j, size, size);
        } else {
          context.fillText(`${alphabet[letter]}`, i + size / 4, j + size / 1.3);
          context.strokeRect(i, j, size, size);
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
