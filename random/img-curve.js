const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    // get image data
    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background
    context.fillStyle = 'hsl(0,0%,0%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 1;
    const bytes = 4;
    let step = 15;

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);

    for (let i = step; i < width - step; i += step) {
      for (let j = step; j < height - step; j += step) {
        let colorIndex = j * (width * bytes) + i * bytes;

        // colors on each step
        const r = data[colorIndex];
        const g = data[colorIndex + 1];
        const b = data[colorIndex + 2];
        context.strokeStyle = `rgb(${r},${g},${b})`;

        // draw curve
        context.beginPath();
        context.moveTo(i, j);
        context.bezierCurveTo(
          i + step * sign() * Math.random(),
          j + sign() * step + step * sign() * Math.random(),
          i + step * sign() * Math.random(),
          j + sign() * step + step * sign() * Math.random(),
          i + step,
          j + step * Math.random() * sign()
        );
        context.stroke();
      }
    }
  };
});
