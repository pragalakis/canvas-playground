const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.4;

    // random value range
    const range = (min, max) => Math.random() * (max - min) + min;

    // random array value pick
    const pick = array => array[Math.floor(Math.random() * array.length)];

    const randomWidth = function(i) {
      if (i > width / 2) {
        return range(0.6, 0.9);
      } else {
        return range(0.2, 0.4);
      }
    };

    let sizeW = 0;
    let sizeH = 0;
    let colorChance = 0;

    for (let j = 0; j < height; j += sizeH) {
      sizeH = Math.floor(height * range(0.1, 0.3));
      for (let i = 0; i < width; i += sizeW) {
        sizeW = Math.floor(width * randomWidth(i));
        if (i + sizeW < width && j + sizeH < height) {
          // color chance
          colorChance += 0.1;
          if (Math.random() < colorChance) {
            context.fillStyle = `${pick(['red', 'blue', 'yellow'])}`;
            context.fillRect(i, j, sizeW, sizeH);
            colorChance -= 0.3;
          }
          context.strokeRect(i, j, sizeW, sizeH);
        } else {
          context.strokeRect(i, j, width - i, height - j);
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
