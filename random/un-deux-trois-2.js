const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,7%)';
    context.lineWidth = 0.05;

    let size = 1.5;
    let margin = 0;
    for (let j = 0; j < height; j += size + margin) {
      for (let i = 0; i < width; i += size + margin) {
        context.save();
        // random rotation
        context.translate(i, j);
        context.rotate(Math.random() * Math.PI);
        context.translate(-i, -j);

        // draw lines
        if (j <= height / 3) {
          context.beginPath();
          context.moveTo(i, j - size / 2);
          context.lineTo(i, j + size / 2);
          context.stroke();
        } else if (j > height / 3 && j < height - height / 3) {
          size = 1;
          margin = -0.15;

          context.beginPath();
          context.moveTo(i - size / 4, j - size / 2);
          context.lineTo(i - size / 4, j + size / 2);
          context.stroke();

          context.beginPath();
          context.moveTo(i + size / 4, j - size / 2);
          context.lineTo(i + size / 4, j + size / 2);
          context.stroke();
        } else {
          size = 0.5;
          margin = -0.1;

          context.beginPath();
          context.moveTo(i - size / 3, j - size / 2);
          context.lineTo(i - size / 3, j + size / 2);
          context.stroke();

          context.beginPath();
          context.moveTo(i, j - size / 2);
          context.lineTo(i, j + size / 2);
          context.stroke();

          context.beginPath();
          context.moveTo(i + size / 3, j - size / 2);
          context.lineTo(i + size / 3, j + size / 2);
          context.stroke();
        }
        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
