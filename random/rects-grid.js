const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // draw background
    context.fillStyle = 'hsl(0, 0%, 87%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    const palette = [
      ['#f1d4d4', '#ddb6c6', '#ac8daf', '#484c7f'],
      ['#fafdcb', '#aee7e8', '#28c3d4', '#248ea9'],
      ['#17223b', '#263859', '#6b778d', '#ff6768'],
      ['#f2f4d1', '#b2d3be', '#89a3b2', '#5e6073'],
      ['#2d132c', '#801336', '#c72c41', '#ee4540'],
      ['#d2f3e0', '#feb9c8', '#f6a7ba', '#f5fbf1'],
      ['#deecfc', '#b9ceeb', '#87a8d0', '#c3b4d2'],
      ['#f5efe3', '#e6e7e5', '#f7d3ba', '#a6aa9c'],
      ['#e7e6e1', '#f7f6e7', '#c1c0b9', '#537791'],
      ['#302939', '#50595c', '#e99b9b', '#ffd8d8'],
      ['#e3d9ca', '#95a792', '#596c68', '#403f48']
    ];
    const sizex = width / 5;
    const sizey = height / 16;
    //const colors = palette[Math.floor(Math.random() * palette.length)];

    for (let j = 0; j < height; j += sizey) {
      //const colors = palette[Math.floor(Math.random() * palette.length)];
      for (let i = 0; i < width; i += sizex) {
        const colors = palette[Math.floor(Math.random() * palette.length)];
        context.fillStyle = colors[Math.floor(Math.random() * colors.length)];
        context.fillRect(i, j, sizex, sizey);
        //context.strokeRect(i, j, sizex, sizey);

        // random subgrid on each tile
        const sizex2 = sizex / Math.floor(1 + Math.random() * 10);
        const sizey2 = sizey / Math.floor(1 + Math.random() * 10);
        for (let y = j; y < j + sizey; y += sizey2) {
          for (let x = i; x < i + sizex; x += sizex2) {
            context.fillStyle =
              colors[Math.floor(Math.random() * colors.length)];
            context.fillRect(x, y, sizex2, sizey2);
            //context.strokeRect(x, y, sizex2, sizey2);
          }
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
