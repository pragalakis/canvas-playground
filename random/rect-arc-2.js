const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    const range = (min, max) => Math.random() * (max - min) + min;

    const colors = [
      'rgba(255,0,0,0.5)',
      'rgba(0,255,0,0.5)',
      'rgba(0,0,255,0.5)'
    ];

    const N = 15; // The number of the shapes
    const offset = 5;
    for (let i = 0; i < N; i++) {
      // random size
      let rad = range(1.8, 3.2);
      let size = 3 * rad;

      // random color
      context.fillStyle = `${
        colors[Math.floor(Math.random() * colors.length)]
      }`;

      if (Math.random() >= 0.5) {
        // draw circles
        let x = range(offset, width - offset - rad);
        let y = range(offset, height - offset - rad);
        context.beginPath();
        context.arc(x, y, rad, 0, Math.PI * 2, true);
        context.fill();
      } else {
        // draw rects
        let x = range(offset - offset / 5, width - offset - size);
        let y = range(offset, height - offset - size);
        context.fillRect(x, y, size, size);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
