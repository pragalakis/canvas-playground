const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,7%)';
    context.lineWidth = 0.05;

    let size = 1;
    let margin = 0;
    for (let j = 0; j < height; j += size + margin) {
      // different margin and size every height/3
      if (j >= height / 3) {
        size = 0.5;
        margin = -0.15;
      }
      if (j >= height - height / 3) {
        size = 0.25;
        margin = -0.1;
      }

      for (let i = 0; i < width; i += size + margin) {
        context.save();
        // random rotation
        context.translate(i, j);
        context.rotate(Math.random() * Math.PI);
        context.translate(-i, -j);

        // draw line
        context.beginPath();
        context.moveTo(i - size / 2, j);
        context.lineTo(i + size / 2, j);
        context.stroke();

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
