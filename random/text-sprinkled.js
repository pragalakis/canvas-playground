const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 95%)';
    context.fillRect(0, 0, width, height);

    const margin = 1;
    const lh = 0.5; // line height
    const fontSize = 0.76;
    const text = 'SPRINKLED ';
    let textIndex = -1;

    context.font = `bold ${fontSize}pt Roboto`;
    context.fillStyle = 'rgba(0,0,0,0.8)';

    const range = (min, max) => Math.random() * (max - min) + min;

    // draw text - a letter on each repetition
    for (let j = margin + fontSize; j < height - margin; j += fontSize + lh) {
      for (let i = margin; i < width - margin; i += textLength) {
        textIndex = textIndex >= text.length - 1 ? 0 : textIndex + 1;
        let letter = text[textIndex];
        textLength = context.measureText(letter).width;

        // skip drawing
        if (range(0.2, 1.5) + Math.log(j) > 3.8) continue;

        // draw letter
        context.fillText(letter, i, j);
      }
      // start again from the first letter
      textIndex = -1;
    }

    // off-white right margin
    context.fillStyle = 'hsl(0, 0%, 95%)';
    context.fillRect(width - margin, 0, margin, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
