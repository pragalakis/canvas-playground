const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    const points = (n, size = 1, x0 = 0, y0 = 0) => {
      // sides starting points
      const sides = [
        [-size / 2, -size / 2],
        [size / 2, -size / 2],
        [size / 2, size / 2],
        [-size / 2, size / 2]
      ];

      const p = [];
      let margin = (4 * size) / n;
      let rem = 0;
      let px, py;

      // for each side
      for (let i = 0; i < 4; i++) {
        // for each point
        for (var j = rem; j <= size; j += margin) {
          // top side
          if (i === 0) {
            px = sides[i][0] + j;
            py = sides[i][1];
          }

          // right side
          if (i === 1) {
            px = sides[i][0];
            py = sides[i][1] + j;
          }

          // bottom side
          if (i === 2) {
            px = sides[i][0] - j;
            py = sides[i][1];
          }

          // left side
          if (i === 3) {
            px = sides[i][0];
            py = sides[i][1] - j;
          }

          // reset margin
          margin = (4 * size) / n;

          // save the data
          p.push([x0 + px, y0 + py]);
        }

        // calculate the starting margin
        // depending on the remainder
        rem = size - (j - margin);
        if (rem > 0) {
          margin = margin - rem;
        } else {
          margin = (4 * size) / n;
          rem = 0;
        }
      }
      return p;
    };

    const margin = 0.5;
    const size = (width - 13 * margin) / 12;
    const rad = size - size / 3;
    const rnd = () => (Math.random() >= 0.5 ? Math.random() : -Math.random());
    const p = points(50);

    // grid
    for (let j = margin; j < height - 2 * margin; j += size + margin) {
      for (let i = margin; i < width - 2 * margin; i += size + margin) {
        const x = rnd() / 2;
        const y = rnd() / 2;

        // draw lines in square points
        p.forEach(v => {
          context.beginPath();
          // big square
          context.moveTo(
            i + size / 2 + v[0] * size,
            j + size / 2 + v[1] * size
          );
          // small square
          context.lineTo(
            i + size / 2 + x + v[0] * (size / 4),
            j + size / 2 + y + v[1] * (size / 4)
          );
          context.stroke();
        });
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
