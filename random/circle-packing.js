const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 97%)';
    context.lineWidth = 0.05;

    // distance calculation
    const dist = function(x1, x2, y1, y2) {
      return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    };

    // random point between min and max
    const range = (min, max) => Math.random() * (max - min) + min;

    const margin = 1;
    const maxRad = 2;
    const minRad = 0.1;
    let points = [];

    const N = 200000;
    for (let i = 0; i < N; i++) {
      let intersects = false;

      let rad = range(minRad, maxRad);
      let x = range(0 + rad + margin, width - rad - margin);
      let y = range(0 + rad + margin, height - rad - margin);

      for (let j = 0; j < points.length; j++) {
        // check for every circle in the points array
        // if the current circle intersects
        let d = dist(x, points[j][0], y, points[j][1]);
        if (d < rad + points[j][2]) {
          intersects = true;
        }
      }

      if (!intersects) points.push([x, y, rad]);
    }

    // draw arcs
    points.forEach(v => {
      context.beginPath();
      context.arc(v[0], v[1], v[2], 0, Math.PI * 2, true);
      context.stroke();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
