const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.001;

    const drawLines = (x, y, sx, sy) => {
      for (let i = 0; i <= 1000; i++) {
        context.beginPath();
        context.moveTo(x + sx * Math.random(), y + sy * Math.random());
        context.lineTo(sx + sx * Math.random(), sy + sy * Math.random());
        context.stroke();
      }
    };

    context.fillStyle = 'hsl(130, 80%, 80%)';
    for (let i = 0; i <= 250; i++) {
      const x = Math.random() * width;
      const y = Math.random() * height;
      const sizeX = Math.random() * 4;
      const sizeY = Math.random() * 4;

      context.save();
      context.beginPath();
      context.rect(x, y, sizeX, sizeY);
      context.fill();
      context.clip();
      drawLines(x, y, sizeX, sizeY);
      context.restore();
    }

    context.fillStyle = 'hsl(30, 80%, 80%)';
    for (let i = 0; i <= 250; i++) {
      const x = Math.random() * width;
      const y = Math.random() * height;
      const sizeX = Math.random() * 4;
      const sizeY = Math.random() * 4;

      context.save();
      context.beginPath();
      context.rect(x, y, sizeX, sizeY);
      context.clip();
      context.fill();
      drawLines(x, y, sizeX, sizeY);
      context.restore();
    }

    context.fillStyle = 'hsl(230, 80%, 80%)';
    for (let i = 0; i <= 250; i++) {
      const x = Math.random() * width;
      const y = Math.random() * height;
      const sizeX = Math.random() * 4;
      const sizeY = Math.random() * 4;

      context.save();
      context.beginPath();
      context.rect(x, y, sizeX, sizeY);
      context.clip();
      context.fill();
      drawLines(x, y, sizeX, sizeY);
      context.restore();
    }
  };
};
canvasSketch(sketch, settings);
