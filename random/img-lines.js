const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    // get image data
    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    // center the image
    context.translate(2.5, 2.5);

    context.lineWidth = 1;
    const bytes = 4;
    let step = 5;

    for (let i = step; i < width - step; i += step) {
      for (let j = step; j < height - step; j += step) {
        let colorIndex = j * (width * bytes) + i * bytes;

        // colors on each step
        const r = data[colorIndex];
        const g = data[colorIndex + 1];
        const b = data[colorIndex + 2];

        context.save();
        // random rotation
        context.translate(i, j);
        context.rotate(Math.random() * Math.PI);
        context.translate(-i, -j);

        // color
        if (r + g + b > 150) {
          context.strokeStyle = 'hsl(0,0%,6%)';
          //context.strokeStyle = `rgb(${r},${g},${b})`;
        } else {
          context.strokeStyle = 'hsl(0,0%,76%)';
          //context.strokeStyle = `rgb(${r},${g},${b})`;
        }

        // draw lines
        context.beginPath();
        context.moveTo(i - step, j);
        context.lineTo(i + step, j);
        context.stroke();
        context.restore();
      }
    }
  };
});
