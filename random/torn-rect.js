const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'black';
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.lineWidth = 0.05;

    const randomPoints = function(n) {
      for (let i = 0; i < n; i++) {
        context.fillRect(
          Math.random() * width,
          Math.random() * height,
          0.05,
          0.05
        );
      }
    };

    // draw points
    randomPoints(10000);

    // draw torn rect
    context.beginPath();
    context.moveTo(4.5, 25);

    for (let i = 0; i < 35; i++) {
      let noise = random.noise1D(i);
      context.lineTo(4.5 + i * 0.6, 25 + noise);
    }

    context.lineTo(4.5 + 34 * 0.6, 10);
    context.lineTo(4.5, 10);
    context.closePath();
    context.clip();

    // draw points in torn rect
    randomPoints(120000);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
