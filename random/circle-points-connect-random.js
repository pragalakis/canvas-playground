const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const x = width / 2;
    const y = height / 2;
    const p = pointsOnCircle(500);
    const rnd = () => (Math.random() >= 0.5 ? Math.random() : -Math.random());

    // random circle lines from point to point
    p.forEach(v => {
      let rad = 14;
      context.beginPath();
      context.moveTo(x + v.x * rad, y + v.y * rad);
      // different radius
      while (rad >= 3) {
        rad -= 2;
        context.lineTo(x + v.x * rad + rnd(), y + v.y * rad + rnd());
      }
      context.lineTo(x + v.x, y + v.y);
      context.stroke();
    });
  };
};
canvasSketch(sketch, settings);
