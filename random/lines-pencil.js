const canvasSketch = require('canvas-sketch');
const smooth = require('chaikin-smooth');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

const N = 100;
const linePoints = lines => {
  const points = [];

  const r = Array(N)
    .fill()
    .map(v => (Math.random() > 0.5 ? Math.random() : -Math.random()));

  let i = 0.1 * lines;
  while (i > 0) {
    const rnd = () => (Math.random() >= 0.5 ? -i : i);
    const line = Array(N)
      .fill()
      .map((v, i) => {
        const x = i + rnd();
        const y = r[i] + Math.cos(i);
        return [x, y / 4];
      });

    points.push(smooth(line));
    i -= 0.1;
  }
  return points;
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 3%)';

    const drawLines = y => {
      // draw lines
      linePoints(2 + Math.floor(Math.random() * 3)).forEach(p => {
        p.forEach((v, i) => {
          context.beginPath();
          context.moveTo((v[0] / N) * width, y + v[1]);
          if (i < p.length - 1) {
            context.lineTo((p[i + 1][0] / N) * width, y + p[i + 1][1]);
          }
          context.stroke();
        });
      });

      // draw line noise
      let n = 100000;
      while (n > 0) {
        context.beginPath();
        context.arc(
          Math.random() * width,
          y - 0.5 + 1 * Math.random(),
          0.001,
          0,
          Math.PI * 2,
          true
        );
        context.fill();
        n--;
      }
    };

    const margin = 1.1;
    const size = 4;
    for (let j = margin; j < height - margin; j += size + margin) {
      context.lineWidth = 0.1;
      context.save();
      context.beginPath();
      context.rect(margin, j, width - margin * 2, size);
      context.stroke();
      context.clip();
      context.lineWidth = 0.03;
      drawLines(j + size / 2);
      context.restore();
    }
  };
};
canvasSketch(sketch, settings);
