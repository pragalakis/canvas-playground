const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(0, 0);
    context.fillStyle = 'black';

    function triangle(i, j) {
      context.beginPath();
      let rnd = Math.random();
      if (rnd < 0.25) {
        context.moveTo(i, j);
        context.lineTo(i + size, j);
        context.lineTo(i, j + size);
        context.lineTo(i, j);
      } else if (rnd >= 0.25 && rnd < 0.5) {
        context.moveTo(i + size, j);
        context.lineTo(i + size, j + size);
        context.lineTo(i, j);
        context.lineTo(i + size, j);
      } else if (rnd >= 0.5 && rnd < 0.75) {
        context.moveTo(i, j);
        context.lineTo(i, j + size);
        context.lineTo(i + size, j + size);
        context.lineTo(i, j);
      } else {
        context.moveTo(i + size, j);
        context.lineTo(i + size, j + size);
        context.lineTo(i, j + size);
        context.lineTo(i + size, j);
      }
      context.fill();
    }

    function square(i, j) {
      context.beginPath();
      let rnd = Math.random();
      if (rnd < 0.25) {
        context.fillRect(i, j, size / 2, size / 2);
      } else if (rnd >= 0.25 && rnd < 0.5) {
        context.fillRect(i + size / 2, j, size / 2, size / 2);
      } else if (rnd >= 0.5 && rnd < 0.75) {
        context.fillRect(i, j + size / 2, size / 2, size / 2);
      } else {
        context.fillRect(i + size / 2, j + size / 2, size / 2, size / 2);
      }
    }

    let size = 1;
    for (let j = size; j < height - size; j += size) {
      for (let i = size; i <= width - size; i += size) {
        let rnd = Math.random();

        if (rnd < 0.3) {
          triangle(i, j);
        } else if (rnd >= 0.3 && rnd < 0.6) {
          context.save();
          context.beginPath();
          context.translate(size / 2, size / 2);
          context.arc(i, j, size / 3, 0, Math.PI * 2, true);
          context.fill();
          context.restore();
        } else {
          square(i, j);
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
