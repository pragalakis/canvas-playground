const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'px'
};

// Artwork function
const sketch = () => {
  // calculate points
  let N = 600;
  let points = Array(N)
    .fill(0)
    .map((v, i) => {
      let x = Math.sin(i) * i;
      let y = Math.cos(i) * i;
      return [5 * x, 5 * y];
    });

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    // draw points
    points.forEach((v, i) => {
      if (i % 2 === 0) {
        context.fillStyle = 'black';
      } else {
        context.fillStyle = 'white';
      }

      // square
      context.fillRect(width / 2 + v[0], height / 2 + v[1], 300, 300);
    });

    // draw border
    context.lineWidth = 300;
    context.strokeStyle = 'white';
    context.strokeRect(0, 0, width, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
