const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    //background
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    // settings
    const fontSize = 1;
    const margin = 2;
    context.font = `${fontSize}pt Monospace`;

    // draw text with random rotation
    const drawText = (text, i, j) => {
      context.save();
      context.translate(i, j);
      context.rotate(Math.random() * Math.PI * 2);
      context.translate(-i, -j);
      context.fillText(text, i, j);
      context.restore();
    };

    // grid
    for (let j = margin / 1.5; j < height; j += fontSize + margin) {
      for (let i = margin / 1.5; i < width; i += fontSize + margin) {
        context.fillStyle = 'rgb(255,0,0)';
        drawText('A', i, j);

        context.fillStyle = 'rgb(255,255,0)';
        drawText('B', i, j);

        context.fillStyle = 'rgb(0,0,255)';
        drawText('C', i, j);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
