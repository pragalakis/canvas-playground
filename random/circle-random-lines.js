const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const margin = 0.5;
    const size = (width - 13 * margin) / 12;
    const rad = size - size / 3;

    // grid
    for (let j = margin; j < height - 2 * margin; j += size + margin) {
      for (let i = margin; i < width - 2 * margin; i += size + margin) {
        context.save();

        // draw and clip square
        context.beginPath();
        context.rect(i, j, size, size);
        context.clip();

        // draw random lines
        const p = pointsOnCircle(30, size);
        p.forEach(v => {
          const x = i + Math.random() * size;
          const y = j + Math.random() * size;

          context.beginPath();
          context.moveTo(x, y);
          context.lineTo(i + size / 2 + v.x, j + size / 2 + v.y);
          context.stroke();
        });

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
