const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 96%)';

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);

    let n = 0;
    while (n < 1000) {
      let h = height / 2 + Math.random() * 100 * sign();
      context.beginPath();
      context.moveTo(0, h);
      context.bezierCurveTo(
        12,
        height / 2 + 20,
        18,
        height / 2 + 20 * -1,
        width,
        h
      );

      context.stroke();
      n++;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
