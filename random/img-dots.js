const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background
    context.fillStyle = 'hsl(0,0%,0%)';
    context.fillRect(0, 0, width, height);

    const bytes = 4;
    const size = 38;
    for (let j = size / 2; j < height; j += size) {
      for (let i = size / 2; i < width; i += size) {
        // color
        const colorIndex = j * (width * bytes) + i * bytes;
        let r = data[colorIndex];
        let g = data[colorIndex + 1];
        let b = data[colorIndex + 2];
        let a = data[colorIndex + 3];
        context.fillStyle = `rgb(${r},${g},${b})`;

        // random radius - depending on red color
        let rad = size / 4;
        const c = r;
        if (c < 80) {
          rad += 2.5 * Math.random();
        } else if (c < 160) {
          rad += 7.5 * Math.random();
        } else if (c < 240) {
          rad -= 5 * Math.random();
        } else {
          rad -= 10 * Math.random();
        }

        // draw circle
        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.fill();
      }
    }
  };
});
