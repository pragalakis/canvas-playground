const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    const fontSize = 0.5;
    context.fillStyle = 'hsl(0,0%,6%)';
    context.font = `${fontSize}pt Courier`;
    context.lineWidth = 0.05;
    const text = 'abcdefghijklmnopqrstuvwxyz'.split('');
    const randomHeight = height / 2 + (Math.random() * height) / 3;

    // draw
    for (let j = -2; j < height + fontSize; j += fontSize + 0.1) {
      for (let i = 0; i < width; i += fontSize) {
        // random letter
        const letter = text[Math.floor(Math.random() * text.length)];

        const noise = random.noise1D(i * 0.1);

        if (randomHeight > j) {
          context.fillText(letter, i, j + Math.sin(noise));
        } else {
          context.fillText(
            letter,
            i + Math.random() * random.sign(),
            j + Math.sin(noise)
          );
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
