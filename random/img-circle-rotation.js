const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);
    let rad = 600;
    const step = 50;

    while (rad >= 0) {
      context.save();

      // clip circle area
      context.beginPath();
      context.arc(width / 2, height / 2, rad, 0, Math.PI * 2, true);
      context.clip();

      // rotation
      context.translate(width / 2, height / 2);
      context.rotate(sign() * Math.random() * 0.2);
      context.translate(-width / 2, -height / 2);

      // draw on cliped area
      context.drawImage(image, 0, 0, width, height);

      context.restore();
      rad -= step;
    }
  };
});
