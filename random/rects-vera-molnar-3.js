const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;
    const size = 1.5;
    const margin = 0.58;

    // grid
    for (let j = margin; j < height - margin; j += size + margin) {
      for (let i = margin; i < width - margin; i += size + margin) {
        // big square
        context.fillStyle = 'hsl(0, 0%, 6%)';
        context.fillRect(i, j, size, size);

        // middle small square
        context.fillStyle = 'hsl(0, 0%, 98%)';
        context.beginPath();
        context.rect(i + size / 3, j + size / 3, size / 3, size / 3);
        context.fill();
        context.stroke();

        // random smal square
        context.beginPath();
        context.rect(
          i + (Math.floor(Math.random() * 3) * size) / 3,
          j + (Math.floor(Math.random() * 3) * size) / 3,
          size / 3,
          size / 3
        );
        context.fill();
        context.stroke();
      }
    }
  };
};
canvasSketch(sketch, settings);
