const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const r = x =>
    Math.random() >= 0.5 ? -Math.random() / x : Math.random() / x;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineCap = 'round';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    let lw = 0.05;
    context.lineWidth = lw;
    const sizex = 0.8;
    const sizey = 0.4;
    const margin = 0.3;

    // grid
    for (let j = 0; j < height + sizey; j += sizey + margin) {
      for (let i = 0; i < width + sizex; i += sizex + margin) {
        let angle = 0;
        const distanceFromCenter = Math.abs(j - height / 2);

        // line width and angle
        if (distanceFromCenter > 0) {
          angle = r(1);
          lw = 0.45;
        }

        if (distanceFromCenter > 3) {
          angle = r(2);
          lw = 0.4;
        }

        if (distanceFromCenter > 5) {
          angle = r(3);
          lw = 0.35;
        }

        if (distanceFromCenter > 8) {
          angle = r(4);
          lw = 0.3;
        }

        if (distanceFromCenter > 11) {
          angle = r(5);
          lw = 0.25;
        }

        if (distanceFromCenter > 14) {
          angle = r(6);
          lw = 0.2;
        }

        if (distanceFromCenter > 16) {
          angle = r(7);
          lw = 0.15;
        }

        if (distanceFromCenter > 18) {
          angle = r(8);
          lw = 0.1;
        }

        if (distanceFromCenter > 20) {
          angle = r(9);
          lw = 0.05;
        }
        context.lineWidth = lw;

        // line
        context.beginPath();
        context.moveTo(i, j);
        context.lineTo(i - sizex + angle, j - sizey - angle);
        context.stroke();
      }
    }
  };
};
canvasSketch(sketch, settings);
