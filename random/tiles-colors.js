const canvasSketch = require('canvas-sketch');
const palette = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,3%)';
    context.lineWidth = 0.05;

    const size = 3;
    for (let i = 0; i < width; i += size) {
      for (let j = 0; j < height; j += size) {
        const color = palette[Math.floor(Math.random() * palette.length)];

        context.fillStyle = color[4];
        context.fillRect(i, j, size, size);

        context.fillStyle = color[0];
        context.beginPath();
        context.moveTo(i + size / 2, j);
        context.lineTo(i + size / 2, j + size / 2);
        context.lineTo(i, j + size / 2);
        context.closePath();
        context.fill();
        context.stroke();

        context.fillStyle = color[1];
        context.beginPath();
        context.moveTo(i + size / 2, j);
        context.lineTo(i + size / 2, j + size / 2);
        context.lineTo(i + size, j + size / 2);
        context.closePath();
        context.fill();
        context.stroke();

        context.fillStyle = color[2];
        context.beginPath();
        context.moveTo(i + size / 2, j + size / 2);
        context.lineTo(i, j + size / 2);
        context.lineTo(i + size / 2, j + size);
        context.closePath();
        context.fill();
        context.stroke();

        context.fillStyle = color[3];
        context.beginPath();
        context.moveTo(i + size / 2, j + size / 2);
        context.lineTo(i + size, j + size / 2);
        context.lineTo(i + size / 2, j + size);
        context.closePath();
        context.fill();
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
