const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // draw background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    function isolines(x, y, w, h, c) {
      // pixel size
      const pixel = 0.02;

      for (let j = y; j < h; j += pixel) {
        for (let i = x; i < w; i += pixel) {
          // simplex noise
          const noise = random.noise2D(0.2 * i, 0.2 * j);

          // line color depending on noise value
          if (noise > -0.97 && noise < -0.99) {
            context.fillStyle = c;
          } else if (noise > -0.9 && noise < -0.91) {
            context.fillStyle = c;
          } else if (noise > -0.85 && noise < -0.83) {
            context.fillStyle = c;
          } else if (noise > -0.55 && noise < -0.53) {
            context.fillStyle = c;
          } else if (noise > -0.25 && noise < -0.23) {
            context.fillStyle = c;
          } else if (noise > -0.05 && noise < -0.03) {
            context.fillStyle = c;
          } else if (noise > 0.2 && noise < 0.23) {
            context.fillStyle = c;
          } else if (noise > 0.5 && noise < 0.53) {
            context.fillStyle = c;
          } else if (noise > 0.6 && noise < 0.61) {
            context.fillStyle = c;
          } else if (noise > 0.7 && noise < 0.71) {
            context.fillStyle = c;
          } else if (noise > 0.84 && noise < 0.85) {
            context.fillStyle = c;
          } else if (noise > 0.95 && noise < 0.96) {
            context.fillStyle = c;
          } else {
            context.fillStyle = 'hsl(0, 0%, 97%)';
          }

          // draw lines
          context.fillRect(i, j, pixel, pixel);
        }
      }
    }

    const margin = 1;
    // function calls
    isolines(margin, margin, width - margin, height / 3 - margin, 'black');
    isolines(
      margin,
      height / 3,
      width - margin,
      (2 * height) / 3 - margin,
      'blue'
    );
    isolines(margin, (2 * height) / 3, width - margin, height - margin, 'red');
  };
};

// Start the sketch
canvasSketch(sketch, settings);
