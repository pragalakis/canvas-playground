const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // vertical lines
    let lineW = Math.random() * 0.5;
    for (let i = 0; i < width; i += lineW) {
      const color = palettes[Math.floor(Math.random() * palettes.length)][0];
      context.strokeStyle = color;
      context.lineWidth = lineW;

      context.beginPath();
      context.moveTo(i, 0);
      context.lineTo(i, height);
      context.stroke();

      lineW = Math.random() * 0.5;
    }

    // horizontal lines
    lineW = Math.random() * 0.5;
    for (let j = 0; j < height; j += 2 * lineW) {
      const color = palettes[Math.floor(Math.random() * palettes.length)][0];
      context.strokeStyle = color;
      context.lineWidth = lineW;

      context.beginPath();
      context.moveTo(0, j);
      context.lineTo(width, j);
      context.stroke();

      lineW = Math.random() * 0.5;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
