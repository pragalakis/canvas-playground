const canvasSketch = require('canvas-sketch');
const palette = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    let x = 0;
    let y = 0;

    for (let j = 0; j <= height; j += y) {
      y = Math.random() * 3;
      for (let i = 0; i <= width; i += x) {
        let color = palette[Math.floor(Math.random() * 100)][1];
        context.fillStyle = `${color}`;
        x = Math.random() * 3;
        context.fillRect(i, j, x, y);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
