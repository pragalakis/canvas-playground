const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    const bytes = 4;
    let step = 1;

    for (let j = step; j < width - step; j += step) {
      // skip
      if (Math.random() > 0.8) continue;

      for (let i = step; i < height - step; i += step) {
        // skip
        if (Math.random() > 0.9) continue;

        let newJ = Math.floor(Math.random() * j);
        let oldIndex = i * (width * bytes) + j * bytes;
        let newIndex = i * (width * bytes) + newJ * bytes;

        data[oldIndex] = data[newIndex];
        data[oldIndex + 1] = data[newIndex + 1];
        data[oldIndex + 2] = data[newIndex + 2];
      }
    }
    context.putImageData(pixels, 0, 0);
  };
});
