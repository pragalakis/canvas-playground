const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;

    const step = 2;
    const rad = step;
    const margin = 2 * rad;
    for (let j = margin; j < height - margin / 2; j += step) {
      for (let i = margin; i < width - margin / 2; i += step) {
        if (Math.random() >= 0.5) {
          let n = 0;
          const flip = Math.random() >= 0.5 ? true : false;
          while (n < 628) {
            context.strokeStyle = `hsla(0, 0%, ${n / 6.28}%, 30%)`;
            context.save();
            context.beginPath();
            context.translate(i, j);
            if (flip) {
              context.scale(-1, -1);
            }
            context.rotate(n * 0.01);
            context.translate(-i, -j);
            context.moveTo(i, j);
            context.lineTo(i, j - rad);
            context.stroke();
            context.restore();
            n++;
          }
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
