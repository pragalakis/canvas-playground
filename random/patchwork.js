// experimenting with matt desl's patchwork
const canvasSketch = require('canvas-sketch');
const clustering = require('density-clustering');
const convexHull = require('convex-hull');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

var colorIndex = -1;
// Artwork function
const sketch = update => {
  // starting color - green palette
  let colors = ['#0B6623', '#9DC183', '#3F704D', '#2E8B57', '#50C878'];
  document.body.onkeyup = function(e) {
    // pressing space changes the color
    if (e.keyCode == 32) {
      colorIndex = colorIndex >= 2 ? 0 : colorIndex + 1;
      switch (colorIndex) {
        case 0:
          // blue palette
          colors = ['#005073', '#107dac', '#189ad3', '1ebbd7', '71c7ec'];
          break;
        case 1:
          // random palette
          let palette = palettes[Math.floor(Math.random() * palettes.length)];
          colors = [palette[0], palette[1], palette[2], palette[3]];
          break;
        case 2:
          // starting color - green palette
          colors = ['#0B6623', '#9DC183', '#3F704D', '#2E8B57', '#50C878'];
      }
      update.render();
    }
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    // random point between min and max
    const range = (min, max) => Math.random() * (max - min) + min;

    function draw(points, clustersNumber, depth) {
      const positions = [];

      // k-means clustering
      const kmeans = new clustering.KMEANS();
      const clusters = kmeans.run(points, clustersNumber);

      for (let j = 0; j < clusters.length; j++) {
        const cluster = clusters[j];
        const position = cluster.map(v => points[v]);
        positions.push(position);

        // find the hull of the cluster
        const edges = convexHull(position);

        // create a closed polyline from the hull
        let path = edges.map(c => position[c[0]]);

        if (path.length === 0) break;

        // drawing the shape
        context.beginPath();
        context.moveTo(path[0][0], path[0][1]);
        for (let i = 1; i < path.length; i++) {
          context.lineTo(path[i][0], path[i][1]);
        }
        context.closePath();

        // random color
        let color = colors[Math.floor(Math.random() * colors.length)];
        context.fillStyle = color;

        //if (depth === 1) context.fill();
        //if (depth < 2) context.fill();
        context.fill();
        context.stroke();
      }

      // recursion
      if (depth > 0) {
        clustersNumber = Math.floor(range(1, 6));
        positions.forEach(v => draw(v, clustersNumber, depth - 1));
      }
    }

    const margin = 1.5;
    const N = 40000;
    // random data points calculation
    const data = Array(N)
      .fill()
      .map(() => [
        range(margin, width - margin),
        range(margin, height - margin)
      ]);

    const recursionDepth = 3;
    const clustersNumber = Math.floor(range(5, 10));
    // calling the function for the first time
    draw(data, clustersNumber, recursionDepth);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
