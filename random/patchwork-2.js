// experimenting with matt desl's patchwork
const canvasSketch = require('canvas-sketch');
const clustering = require('density-clustering');
const convexHull = require('convex-hull');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 97%)';

    // random point between min and max
    const range = (min, max) => Math.random() * (max - min) + min;

    // min and max values in an array of 2d points
    function getMinMaxOf2DIndex(arr, index) {
      let min = Math.min(...arr.map(v => v[index]));
      let max = Math.max(...arr.map(v => v[index]));
      return [min, max];
    }

    function draw(points, clustersNumber) {
      // k-means clustering
      const kmeans = new clustering.KMEANS();
      const clusters = kmeans.run(points, clustersNumber);

      for (let j = 0; j < clusters.length; j++) {
        const cluster = clusters[j];
        const position = cluster.map(v => points[v]);

        // find the hull of the cluster
        const edges = convexHull(position);

        // create a closed polyline from the hull
        let path = edges.map(c => position[c[0]]);

        if (path.length === 0) break;

        // cliping the shape
        context.save();
        context.beginPath();
        context.moveTo(path[0][0], path[0][1]);
        for (let i = 1; i < path.length; i++) {
          context.lineTo(path[i][0], path[i][1]);
        }
        context.closePath();
        context.clip();

        // min-max of x
        let x = getMinMaxOf2DIndex(path, 0);
        // min-max of y
        let y = getMinMaxOf2DIndex(path, 1);
        // points generation inside the cliped area
        const N = Math.floor(range(5000, 30000));
        console.log(N);
        const randomPoints = Array(N)
          .fill()
          .map(() => {
            context.fillRect(range(x[0], x[1]), range(y[0], y[1]), 0.03, 0.03);
          });
        context.restore();
      }
    }

    const margin = 1.5;
    const N = 1000;
    // random data points calculation
    const data = Array(N)
      .fill()
      .map(() => [
        range(margin, width - margin),
        range(margin, height - margin)
      ]);

    const clustersNumber = Math.floor(range(3, 15));
    // draw function for cluster points generation
    draw(data, clustersNumber);

    // point generation for the whole canvas
    const randomPoints = Array(30000)
      .fill()
      .map(() => {
        context.fillRect(range(0, width), range(0, height), 0.03, 0.03);
      });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
