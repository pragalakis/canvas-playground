const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'px'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  const colors = ['7', '97'];
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,67%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 28;

    // draw pattern
    let indx = 0;
    for (let i = 0; i < height + width; i += 40) {
      context.strokeStyle = `hsl(0,0%,${colors[indx]}%)`;
      context.beginPath();
      context.moveTo(0, i);
      context.lineTo(i, 0);
      context.stroke();
      indx = indx >= colors.length - 1 ? 0 : indx + 1;
    }

    // horizontal glitch
    let step = 400 * Math.random() + 200;
    for (let j = 0; j < height; j += step) {
      let imagedata = context.getImageData(0, j, width, step);
      context.putImageData(imagedata, 100 * Math.random() * sign(), j);
      step = 400 * Math.random() + 200;
    }

    // vertical glitch
    for (let i = 0; i < width; i += step) {
      let imagedata = context.getImageData(i, 0, step, height);
      context.putImageData(imagedata, i, 100 * Math.random() * sign());
      step = 400 * Math.random() + 200;
    }

    // draw border
    context.lineWidth = 300;
    context.strokeStyle = 'hsl(0, 0%, 96%)';
    context.strokeRect(0, 0, width, height);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
