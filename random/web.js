const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  //dimensions: [window.innerWidth, window.innerHeight],
  dimensions: 'A3',
  units: 'px',
  animate: true
};

// Artwork function
const sketch = () => {
  return ({ context, width, height, time }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 1;

    const sign = () => (Math.random() >= 0.5 ? 1 : -1);
    let size = 250;
    let prevx = width / 2;
    let prevy = height / 2;
    let N = 1000;

    context.beginPath();
    context.moveTo(prevx, prevy);
    for (let i = 0; i < N; i++) {
      let x = prevx + Math.random() * size * sign();
      let y = prevy + Math.random() * size * sign();
      context.lineTo(x, y);
    }
    context.stroke();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
