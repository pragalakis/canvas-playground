const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const drawBalls = (x, y, length) => {
      const margin = 2;
      const rad = 0.6;
      // draw circles
      for (let i = x; i < length; i += 2 * rad) {
        const p = pointsOnCircle(50, rad, i, y);
        context.beginPath();
        context.moveTo(p[0].x, p[0].y);
        p.forEach(_ => {
          const pIndx = Math.floor(Math.random() * p.length);
          const px = p[pIndx].x;
          const py = p[pIndx].y;
          p.splice(pIndx, 1);
          context.lineTo(px, py);
        });
        context.closePath();
        context.stroke();
      }

      // recursion
      if (length > 2 * rad) {
        return drawBalls(x + 1 * rad, y - 2 * rad, length - 1 * rad);
      } else {
        return;
      }
    };

    const drawTrunk = (x, y, w, h) => {
      let n = 10;
      while (n > 0) {
        context.beginPath();
        context.moveTo(x + Math.random() * w, y);
        context.lineTo(x, y + Math.random() * h);
        context.stroke();

        context.beginPath();
        context.moveTo(x + Math.random() * w, y);
        context.lineTo(x + w, y + Math.random() * h);
        context.stroke();

        context.beginPath();
        context.moveTo(x + Math.random() * w, y + h);
        context.lineTo(x + w, y + Math.random() * h);
        context.stroke();

        context.beginPath();
        context.moveTo(x + Math.random() * w, y + h);
        context.lineTo(x, y + Math.random() * h);
        context.stroke();

        context.beginPath();
        context.moveTo(x, y + Math.random() * h);
        context.lineTo(x + w, y + Math.random() * h);
        context.stroke();

        context.beginPath();
        context.moveTo(x + Math.random() * w, y);
        context.lineTo(x + Math.random() * w, y + h);
        context.stroke();
        n--;
      }
    };

    const drawStar = (x, y, size) => {
      context.save();
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x + size, y);
      context.lineTo(x + size / 2, y - size);
      context.closePath();
      context.clip();
      drawTrunk(x, y - size, 1.5 * size, 1.5 * size);
      context.restore();

      context.save();
      context.beginPath();
      context.moveTo(x, y - size + size / 3);
      context.lineTo(x + size, y - size + size / 3);
      context.lineTo(x + size / 2, y + size / 3);
      context.closePath();
      context.clip();
      drawTrunk(x, y - size, 1.5 * size, 1.5 * size);
      context.restore();
    };

    // draw
    let x = 4;
    let y = height - height / 4;
    let length = width - 4;
    drawBalls(x, y, length, false);
    drawTrunk(width / 2 - 1, y + 0.6, 2, 6);
    drawStar(width / 2 - 1, 8.5, 2);
  };
};
canvasSketch(sketch, settings);
