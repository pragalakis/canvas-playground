const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 3%)';
    context.fillRect(0, 0, width, height);

    // calculate points
    const stepy = 0.3;
    const points = [];
    let p = Math.random();
    for (let j = 0; j < height; j += stepy) {
      points.push({ x: p, y: j });
      p += Math.random() >= 0.5 ? -1 * Math.random() : Math.random();
    }

    const dot = (x, y, r) => {
      /*
      context.beginPath();
      context.arc(x, y, r, 0, Math.PI * 2, true);
      context.fill();
      */
      context.fillRect(x, y, 2 * r, 2 * r);
    };

    // draw points
    const stepx = 0.1;
    const rad = 0.01;
    const rnd = () => 2 * rad + Math.random();
    let opacity = 1;
    let hue = 250;
    for (let i = width / 2; i < width / 2 + 10; i += stepx) {
      context.fillStyle = `hsla(${hue},70%,50%,${opacity})`;

      let n = 10 - Math.floor(Math.abs(width - i - 10));
      while (n > 0) {
        points.forEach(v => {
          dot(v.x + i, v.y + rnd(), rad);
          dot(v.x + width - i, v.y + rnd(), rad);
        });
        n--;
      }

      opacity -= stepx / 10;
      //hue += 3; // OIL SPILL COLOR
      hue += 1; // FINAL COLOR
      //rad -= 0.0005;
    }
  };
};
canvasSketch(sketch, settings);
