/*
https://en.wikipedia.org/wiki/Chaos_game
A point inside a square repeatedly jumps half of the distance towards a randomly chosen vertex, but the currently chosen vertex cannot be 2 places away from the previously chosen vertex.
*/
const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    let rad = 14;
    let vertices = 4;
    let points = pointsOnCircle(vertices, rad, width / 2, height / 2);

    let x = points[0].x;
    let y = points[0].y;

    const t = 0.5; // jumps half of the distance
    const N = 100000;
    let previous = -1;

    // chaos game
    for (let p = 0; p < N; p++) {
      let r = Math.floor(Math.random() * points.length);
      while (r == previous + 2 || r == previous - 2) {
        r = Math.floor(Math.random() * points.length);
      }

      previous = r;
      x = lerp(x, points[r].x, t);
      y = lerp(y, points[r].y, t);

      // random color
      let color = `rgb(${Math.random() * 255},${Math.random() *
        255},${Math.random() * 255})`;
      context.fillStyle = color;

      // draw rect points
      context.fillRect(x, y, 0.03, 0.03);
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
