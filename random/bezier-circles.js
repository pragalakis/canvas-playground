const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // calculate random points on a circle
  const points = n => {
    return Array(n)
      .fill()
      .map(v => {
        const phi = Math.random() * Math.PI * 2;
        const x = Math.cos(phi);
        const y = Math.sin(phi);
        return [x * 2, y * 2];
      });
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'rgba(255,255,255,0.8)';
    context.fillStyle = 'rgba(0,0,0,0.8)';

    // grid
    const step = 5;
    for (let y = step / 1.5; y < height; y += step) {
      for (let x = step / 2; x < width; x += step) {
        // random circle points
        const p = points(Math.floor(10 + Math.random() * 50));

        // draw bezier curves on circle points
        context.beginPath();
        context.moveTo(x + p[0][0], y + p[0][1]);
        for (let i = 1; i < p.length; i += 4) {
          if (p[i + 2] === undefined) break;

          context.bezierCurveTo(
            x + p[i][0],
            y + p[i][1],
            x + p[i + 1][0],
            y + p[i + 1][1],
            x + p[i + 2][0],
            y + p[i + 2][1]
          );
        }
        context.closePath();
        context.fill();
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
