const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const rnd = () =>
    Math.random() >= 0.5 ? Math.random() / 0.8 : -Math.random() / 0.8;

  const pointsOnCircle = (n, rad = 1, x0 = 0, y0 = 0) => {
    const points = Array(n)
      .fill()
      .map((v, i) => {
        let phi = (i * Math.PI * 2) / n;
        let x = x0 + rad * Math.cos(phi);
        let y = y0 + 1.3 * rad * Math.sin(phi);
        return { x: x, y: y };
      });

    return points;
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,7%)';
    context.fillRect(0, 0, width, height);

    const draw = (x, y, rad, color) => {
      // data
      const polygon = pointsOnCircle(50, rad, x, y);
      const [r, g, b] = color;

      context.fillStyle = `rgba(${r},${g},${b},0.2)`;

      // add noise to the points
      const noisePolygon = polygon.map(v => {
        const noisex = random.noise1D(v.x);
        const noisey = random.noise1D(v.y);
        return [v.x + noisex, v.y + noisey];
      });

      // deform an array multiple times
      const deform = (a, n) => {
        while (n > 0) {
          a = a.map(v => [v[0] + rnd() / 2, v[1] + rnd() / 2]);
          n--;
        }

        return a;
      };

      // deform the polygon
      const points = deform(noisePolygon, 4);

      // draw points
      context.beginPath();
      context.moveTo(points[0][0], points[0][1]);
      points.forEach(v => {
        context.lineTo(v[0], v[1]);
      });
      context.closePath();
      context.fill();

      let n = 50;
      while (n > 0) {
        context.fillStyle = `rgba(${r},${g},${b},0.05)`;

        // draw points & save data
        context.beginPath();
        context.moveTo(points[0][0], polygon[0][1]);
        points.map(v => {
          let x1 = v[0] + rnd();
          let x2 = v[1] + rnd();
          context.lineTo(x1, x2);
          return [x1, x2];
        });
        context.closePath();
        context.fill();
        n--;
      }
    };

    for (let j = 0; j < height; j += 8) {
      const r = Math.floor(Math.random() * 255);
      const g = Math.floor(Math.random() * 255);
      const b = Math.floor(Math.random() * 255);
      draw(width / 2 + Math.cos(j), j, 5, [r, g, b]);
    }
  };
};
canvasSketch(sketch, settings);
