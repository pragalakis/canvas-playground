const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // grid
    const rad = 4;
    for (let j = 1.8 * rad; j < height - rad; j += rad) {
      let angle = 0.1 + Math.random() / 3;
      let angleIndx = 1;
      for (let i = 1.2 * rad; i < width - rad / 2; i += rad) {
        // full circle
        context.fillStyle = 'red';
        context.beginPath();
        context.arc(i, j, rad / 2, 0, Math.PI * 2, true);
        context.fill();

        // angle
        if (angleIndx % 1 === 0) {
          angle = angle * -1;
        }
        angleIndx++;

        // half circle
        context.fillStyle = 'black';
        context.save();
        context.translate(i, j);
        context.rotate(angle);
        context.translate(-i, -j);
        context.beginPath();
        context.arc(i, j, rad / 2, 0, Math.PI, true);
        context.fill();
        context.restore();
      }
    }
  };
};
canvasSketch(sketch, settings);
