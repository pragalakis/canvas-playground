const canvasSketch = require('canvas-sketch');
var random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(0.3, 0.5);

    let rad = 2;
    let N = 127;

    // bloaby circle-points generation
    function points() {
      // new random utility
      random = random.createRandom();
      let data = Array(N)
        .fill()
        .map((v, i) => {
          let point = 0.05 * i;
          let noise = random.noise1D(point) / 6;
          let phi = point + Math.PI * 2;

          let x = rad * Math.cos(phi) + noise;
          let y = rad * Math.sin(phi) + noise;

          return [x, y];
        })
        .slice(0, N - 1);

      return data;
    }

    // draw 5 bloabs on each row
    for (let j = 5.5; j < height - 3; j += 5) {
      let index = 0;
      for (let i = 4.5; i < width - 3; i += 5) {
        let array = points();
        let colors = palettes[Math.floor(Math.random() * 100)];
        context.fillStyle = `${colors[index]}`;

        context.beginPath();
        context.moveTo(array[0][0] + i, array[0][1] + j);
        array.forEach(v => {
          context.lineTo(v[0] + i, v[1] + j);
        });
        context.lineTo(array[0][0] + i, array[0][1] + j);

        context.fill();
        index++;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
