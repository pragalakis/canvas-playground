const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    const fontSize = 0.5;
    context.fillStyle = 'hsl(0,0%,6%)';
    context.font = `${fontSize}pt Courier`;
    context.lineWidth = 0.05;
    const text = 'abcdefghijklmnopqrstuvwxyz'.split('');

    // draw
    for (let j = 0; j < height + fontSize; j += fontSize + 0.1) {
      for (let i = 0; i < width; i += fontSize) {
        // random letter
        const letter = text[Math.floor(Math.random() * text.length)];
        // noise
        const noise = random.noise2D(i * 0.05, j * 0.05);
        if (noise < 0.5) {
          context.fillText(letter, i, j);
        } else {
          context.fillText(
            letter,
            i + Math.random() * random.sign(),
            j + Math.random() * random.sign()
          );
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
