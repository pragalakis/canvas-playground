const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // calculate random points on a circle
  const points = (n, rad) => {
    return Array(n)
      .fill()
      .map(v => {
        const phi = Math.random() * Math.PI * 2;
        const x = Math.cos(phi);
        const y = Math.sin(phi);
        return [rad * x, rad * y];
      });
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 10%)';

    // grid
    const step = 3;
    const radius = step / 3;
    let numOfPoints = 4;
    for (let y = step / 2; y < height; y += step) {
      for (let x = step / 2; x < width; x += step) {
        // circle points
        const p = points(numOfPoints, radius);

        // draw quadratic curves on circle points
        context.beginPath();
        context.moveTo(x + p[0][0], y + p[0][1]);
        for (let i = 1; i < p.length; i += 2) {
          if (p[i + 1] === undefined) break;

          context.quadraticCurveTo(
            x + p[i][0],
            y + p[i][1],
            x + p[i + 1][0],
            y + p[i + 1][1]
          );
        }
        context.closePath();
        context.stroke();
        numOfPoints++;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
