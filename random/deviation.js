const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.1;

    let offX = 2;
    let offY = 3.5;
    context.strokeRect(offX, offY, width - 2 * offX, height / 2 + 2 * offX);

    let size = 2;
    let offset = size / 4;

    function deviation() {
      let rnd = Math.random();
      let random = rnd > 0.4 ? 0.4 : rnd;
      let signX = Math.random() > 0.5 ? -1 : 1;
      let signY = Math.random() > 0.5 ? -1 : 1;
      return [signX * random, signY * random];
    }

    for (let j = 5; j < height / 2 + 5; j = j + offset + size) {
      for (let i = 3.8; i <= width - 2 - size; i = i + offset + size) {
        context.fillStyle = 'black';
        context.fillRect(i, j + Math.sin(i / 2) / 2, size, size);

        let dev = deviation();
        context.fillStyle = 'white';
        context.beginPath();
        context.arc(
          i + size / 2 + dev[0],
          j + size / 2 + Math.sin(i / 2) / 2 + dev[1],
          size / 6,
          0,
          Math.PI * 2,
          true
        );
        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
