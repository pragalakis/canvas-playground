const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    const size = 0.4;
    let N = 15000;

    while (N > 0) {
      context.strokeStyle = `rgb(${Math.floor(Math.random() * 255)}, 0, 0)`;
      const x = Math.random() * width;
      const y = Math.random() * height;
      const flow = (x + y) * 0.04 * Math.PI * 2;

      context.save();
      context.translate(x, y);
      context.rotate(flow);
      context.translate(-x, -y);
      context.beginPath();
      context.moveTo(x - size / 2, y - size / 2);
      context.lineTo(x + size / 2, y + size / 2);
      context.stroke();
      context.restore();
      N--;
    }
  };
};
canvasSketch(sketch, settings);
