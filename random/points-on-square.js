const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';

    const size = 20;
    //context.strokeRect(width / 2 - size / 2, height / 2 - size / 2, size, size);

    const points = (n, size = 1, x0 = 0, y0 = 0) => {
      // sides starting points
      const sides = [
        [-size / 2, -size / 2],
        [size / 2, -size / 2],
        [size / 2, size / 2],
        [-size / 2, size / 2]
      ];

      const p = [];
      let margin = (4 * size) / n;
      let rem = 0;
      let px, py;

      // for each side
      for (let i = 0; i < 4; i++) {
        // for each point
        for (var j = rem; j <= size; j += margin) {
          // top side
          if (i === 0) {
            px = sides[i][0] + j;
            py = sides[i][1];
          }

          // right side
          if (i === 1) {
            px = sides[i][0];
            py = sides[i][1] + j;
          }

          // bottom side
          if (i === 2) {
            px = sides[i][0] - j;
            py = sides[i][1];
          }

          // left side
          if (i === 3) {
            px = sides[i][0];
            py = sides[i][1] - j;
          }

          // reset margin
          margin = (4 * size) / n;

          // save the data
          p.push([x0 + px, y0 + py]);
        }

        // calculate the starting margin
        // depending on the remainder
        rem = size - (j - margin);
        if (rem > 0) {
          margin = margin - rem;
        } else {
          margin = (4 * size) / n;
          rem = 0;
        }
      }
      return p;
    };

    // center of squares
    const x0 = width / 2;
    const y0 = height / 2;

    // points on each square
    const points1 = points(500, size, x0, y0);
    const points2 = points(500, size / 1.5, x0, y0);
    const points3 = points(500, size / 2, x0, y0);
    const points4 = points(500, size / 3, x0, y0);

    // draw lines
    points1.forEach((v, i) => {
      context.beginPath();
      context.moveTo(v[0], v[1]);
      context.lineTo(
        points2[i][0] + Math.random(),
        points2[i][1] + Math.random()
      );
      context.lineTo(
        points3[i][0] + Math.random(),
        points3[i][1] + Math.random()
      );
      context.lineTo(points4[i][0], points4[i][1]);
      context.stroke();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
