const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'rgba(0,0,0,0.8)';
    context.lineWidth = 0.05;

    function randomRange(min, max) {
      return Math.random() * (max - min) + min;
    }

    const lines = 8;
    const N = 100;

    // generate points inside a random arc
    const data = Array(N)
      .fill(0)
      .map(v => {
        let arc = [];
        let random = randomRange(0.3, 1.0);
        let rad = random * 3;

        for (let i = 0; i < lines; i++) {
          let phi = i + random * Math.PI * 2;
          let x = Math.cos(phi);
          let y = Math.sin(phi);

          arc.push([x * rad, y * rad]);
        }

        return arc;
      });

    // draw the arcs
    for (let i = 0; i < data.length; i++) {
      let x = randomRange(0.1, width);
      let y = randomRange(0.1, height);

      // draw the lines
      for (let j = 0; j < lines; j++) {
        context.beginPath();
        context.moveTo(x + data[i][j][0], y + data[i][j][1]);

        // this is where magic happens
        if (Math.random() >= 0.5) {
          context.lineTo(x - data[i][j][0], y + data[i][j][1]);
        } else {
          context.lineTo(x + data[i][j][0], y - data[i][j][1]);
        }
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
