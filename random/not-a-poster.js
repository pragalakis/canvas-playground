const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    // grid
    const margin = 0.5;
    const rad = (width - margin * 2) / 10;
    for (let j = rad; j < height; j += rad + margin) {
      for (let i = rad; i < width; i += rad + margin) {
        // main circle
        context.fillStyle = 'hsl(0,0%,6%)';
        context.beginPath();
        context.arc(i, j, rad / 2, 0, Math.PI * 2, true);
        context.fill();

        // random sub circle
        const x = i + (sign() * Math.random()) / 2;
        const y = j + (sign() * Math.random()) / 2;
        context.fillStyle = 'hsl(0,0%,97%)';
        context.beginPath();
        context.arc(x, y, rad / 4.5, 0, Math.PI * 2, true);
        context.fill();
      }
    }

    // title box
    context.fillStyle = 'hsl(0,0%,6%)';
    context.fillRect(0, height - height / 3.05, width, 6.5);

    // title
    context.fillStyle = 'hsl(0,0%,97%)';
    context.font = '1.5pt Monospace';
    context.fillText('THIS IS NOT A POSTER', 2.5, height - height / 4.3);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
