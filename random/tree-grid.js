const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // draw background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    function branch(len, x, y, last) {
      const angle1 = Math.random();
      const angle2 = Math.random();
      //len = len / (1.05 + Math.random());
      //len = len / 1.2;
      len = len / (1.08 + 1 / Math.log(x * y));

      // right branch
      context.save();
      context.translate(x, y);
      context.rotate(angle1);
      context.translate(-x, -y);
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x, y - len);
      context.stroke();
      context.restore();

      // left branch
      context.save();
      context.translate(x, y);
      context.rotate(-angle2);
      context.translate(-x, -y);
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x, y - len);
      context.stroke();
      context.restore();

      // recursion
      if (len > 0.2) {
        last = last - len;
        branch(
          len,
          x + len * Math.cos(Math.PI / 2 - angle1),
          y - len * Math.sin(Math.PI / 2 - angle1)
        );
        branch(
          len,
          x - len * Math.cos(Math.PI / 2 - angle2),
          y - len * Math.sin(Math.PI / 2 - angle2)
        );
      }
    }

    // draw tree grid
    const size = width / 9;
    for (let j = size / 3; j < height - size / 2; j += size) {
      let len = size / 5;
      let last = size - len;
      for (let i = size / 2; i < width - size / 2; i += size) {
        context.strokeRect(i, j, size, size);

        // tree trunk
        context.beginPath();
        context.moveTo(i + size / 2, j + size);
        context.lineTo(i + size / 2, j + last);
        context.stroke();

        // recursive branches
        branch(len, i + size / 2, j + last, last);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
