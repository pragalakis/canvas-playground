const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    const N = 100000;
    // calculate N points inside a circle
    const sphere = Array(N)
      .fill(0)
      .map(() => {
        let random = Math.random();
        let phi = Math.random() * Math.PI * 2;
        let theta = random * Math.PI;

        // the -theta*0.6 changes the light center of the sphere
        let x = theta * Math.cos(phi) - theta * 0.65;
        let y = theta * Math.sin(phi) - theta * 0.65;
        let rad = 3;

        // big distance from the light center = darker color
        let alpha = 1 - random;
        return [rad * x, rad * y, alpha];
      });

    // draw points
    sphere.forEach(v => {
      context.fillStyle = `rgba(255,255,255,${v[2]})`;
      context.fillRect(width / 1.45 + v[0], height / 1.6 + v[1], 0.05, 0.05);
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
