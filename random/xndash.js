const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    context.translate(-0.35, 0.2);
    context.strokeStyle = 'black';
    context.lineWidth = 0.1;

    let size = 1;
    let offset = size / 4;

    for (let j = size; j < height - size; j = j + offset + size) {
      for (let i = size; i <= width - size; i = i + offset + size) {
        context.beginPath();
        if (Math.random() < 0.7) {
          context.moveTo(i, j + size / 2);
          context.lineTo(i + size, j + size / 2);
        } else {
          if (Math.random() < 0.05) {
            context.strokeStyle = 'red';
          }
          context.moveTo(i, j);
          context.lineTo(i + size, j + size);
          context.moveTo(i + size, j);
          context.lineTo(i, j + size);
        }
        context.stroke();
        context.strokeStyle = 'black';
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
