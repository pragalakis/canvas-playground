const canvasSketch = require('canvas-sketch');
const palette = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,3%)';
    context.lineWidth = 0.05;

    const size = 3;
    for (let i = 0; i < width; i += size) {
      for (let j = 0; j < height; j += size) {
        const color = palette[Math.floor(Math.random() * palette.length)];
        const center = [
          [i, j],
          [i + size, j],
          [i, j + size],
          [i + size, j + size]
        ];
        const rnd = Math.floor(Math.random() * center.length);

        context.save();

        //draw and clip rect
        context.fillStyle = color[0];
        context.beginPath();
        context.rect(i, j, size, size);
        context.fill();
        context.clip();

        // draw big arc
        context.fillStyle = color[1];
        context.beginPath();
        context.arc(center[rnd][0], center[rnd][1], size, 0, Math.PI * 2, true);
        context.fill();

        // draw small arc
        if (rnd === 0) {
          if (Math.random >= 0.5) {
            var x = center[rnd][0] + size / 2;
            var y = center[rnd][1];
          } else {
            var x = center[rnd][0];
            var y = center[rnd][1] + size / 2;
          }
        } else if (rnd === 1) {
          if (Math.random >= 0.5) {
            var x = center[rnd][0] - size / 2;
            var y = center[rnd][1];
          } else {
            var x = center[rnd][0];
            var y = center[rnd][1] + size / 2;
          }
        } else if (rnd === 2) {
          if (Math.random >= 0.5) {
            var x = center[rnd][0];
            var y = center[rnd][1] - size / 2;
          } else {
            var x = center[rnd][0] + size / 2;
            var y = center[rnd][1];
          }
        } else {
          if (Math.random >= 0.5) {
            var x = center[rnd][0];
            var y = center[rnd][1] - size / 2;
          } else {
            var x = center[rnd][0] - size / 2;
            var y = center[rnd][1];
          }
        }
        context.fillStyle = color[2];
        context.beginPath();
        context.arc(x, y, size / 2, 0, Math.PI * 2, true);
        context.fill();

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
