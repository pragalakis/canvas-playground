const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const offset = () =>
    Math.random() >= 0.5 ? -Math.random() / 3 : Math.random() / 3;
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;
    const size = 2;
    const margin = 0.9;

    // grid
    for (let j = 1.5 * margin; j < height - margin; j += size + margin) {
      for (let i = margin; i < width - margin; i += size + margin) {
        // rects
        let n = 10;
        while (n > 0) {
          // random color
          context.strokeStyle =
            palettes[Math.floor(Math.random() * palettes.length)][0];

          // draw rect
          context.beginPath();
          context.moveTo(i + offset(), j + offset());
          context.lineTo(i + size + offset(), j + offset());
          context.lineTo(i + size + offset(), j + size + offset());
          context.lineTo(i + offset(), j + size + offset());
          context.closePath();
          context.stroke();
          n--;
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
