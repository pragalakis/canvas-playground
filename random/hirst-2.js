const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    function rangeFloor(min, max) {
      return Math.floor(Math.random() * (max - min) + min);
    }

    function range(min, max) {
      return Math.random() * (max - min) + min;
    }

    const rad = 0.25;
    for (let i = 0; i < 5000; i++) {
      let r = rangeFloor(50, 255);
      let g = rangeFloor(50, 255);
      let b = rangeFloor(50, 255);
      let a = range(0.9, 1);
      context.fillStyle = `rgba(${r},${g},${b},${a})`;

      context.beginPath();
      context.arc(
        Math.random() * width,
        Math.random() * height,
        rad,
        0,
        2 * Math.PI,
        true
      );
      context.fill();
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
