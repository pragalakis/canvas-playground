//http://www.fubiz.net/2016/06/30/portraits-with-random-colors-hiding-their-faces/?fbclid=IwAR16uinMzjxPcYMeEJ8rW-YduCKBBufu61X2KVeRTv_YvYnF29D_QI2GCLs
const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,96%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.5, 0.5);
    context.lineWidth = 0.05;

    let size = 2;

    function steps() {
      return Math.floor(Math.random() * 12);
    }

    function lines(x, i, j) {
      // cube
      switch (x) {
        // first square
        case 0:
          context.moveTo(i + size / 5, j + size / 5);
          context.lineTo(i + size / 5, j + size - size / 5);
          break;
        case 1:
          context.moveTo(i + size / 5, j + size / 5);
          context.lineTo(i + size - size / 5, j + size / 5);
          break;
        case 2:
          context.moveTo(i + size - size / 5, j + size / 5);
          context.lineTo(i + size - size / 5, j + size - size / 5);
          break;
        case 3:
          context.moveTo(i + size - size / 5, j + size - size / 5);
          context.lineTo(i + size / 5, j + size - size / 5);
          break;

        // second square
        case 4:
          context.moveTo(i + size / 2.5, j + size / 2.5);
          context.lineTo(i + size / 2.5, j + size);
          break;
        case 5:
          context.moveTo(i + size / 2.5, j + size / 2.5);
          context.lineTo(i + size, j + size / 2.5);
          break;
        case 6:
          context.moveTo(i + size, j + size / 2.5);
          context.lineTo(i + size, j + size);
          break;
        case 7:
          context.moveTo(i + size / 2.5, j + size);
          context.lineTo(i + size, j + size);
          break;

        // connections
        case 8:
          context.moveTo(i + size / 5, j + size / 5);
          context.lineTo(i + size / 2.5, j + size / 2.5);
          break;
        case 9:
          context.moveTo(i + size - size / 5, j + size / 5);
          context.lineTo(i + size, j + size / 2.5);
          break;
        case 10:
          context.moveTo(i + size - size / 5, j + size - size / 5);
          context.lineTo(i + size, j + size);
          break;
        case 11:
          context.moveTo(i + size / 5, j + size - size / 5);
          context.lineTo(i + size / 2.5, j + size);
          break;
      }
    }

    let offset = size + 1.5;

    for (let i = offset; i <= width - offset; i += offset) {
      for (let j = offset; j < height - offset; j += offset) {
        context.beginPath();
        let x = steps();
        for (let z = 0; z <= x; z++) {
          lines(steps(), i, j);
        }
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
