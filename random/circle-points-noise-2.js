const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // points on a circle with noise
  const rad = 2.5;
  const N = 50;
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  const points = () =>
    Array(N)
      .fill()
      .map((v, i) => {
        const phi = (i / N) * Math.PI * 2;
        const x = Math.cos(phi) * Math.cos((Math.random() / 2) * sign());
        const y = Math.sin(phi) * Math.cos((Math.random() / 2) * sign());
        return [x, y];
      });

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    // grid
    for (let x = rad; x < width; x += 2 * rad) {
      for (let y = rad; y < height - rad; y += rad / 7) {
        const p = points();
        const r = (rad - 0.5) * Math.random();

        context.beginPath();
        context.moveTo(x + p[0][0] * r, y + p[0][1] * r);
        p.forEach(v => {
          context.lineTo(x + v[0] * r, y + v[1] * r);
        });
        context.closePath();
        context.fill();
        context.stroke();
      }
    }
  };
};
canvasSketch(sketch, settings);
