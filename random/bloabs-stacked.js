const canvasSketch = require('canvas-sketch');
var random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;

    let N = 127;
    // bloaby circle-points generation
    function points(rad) {
      // new random utility
      random = random.createRandom();
      let data = Array(N)
        .fill()
        .map((v, i) => {
          let point = 0.05 * i;
          let noise = random.noise1D(point) / 6;
          let phi = point + Math.PI * 2;

          let x = rad * Math.cos(phi) + noise;
          let y = rad * Math.sin(phi) + noise;

          return [x, y];
        })
        .slice(0, N - 1);

      return data;
    }

    let rad = 10;
    // draw bloabs with differend rad and color
    while (rad > 0) {
      context.fillStyle = `hsl(0,0%,${2 * rad}%)`;
      context.strokeStyle = `hsl(0,0%,${3 * rad}%)`;

      const array = points(rad);
      context.beginPath();
      context.moveTo(width / 2 + array[0][0], height / 2 + array[0][1]);
      array.forEach(v => {
        context.lineTo(width / 2 + v[0], height / 2 + v[1]);
      });
      context.closePath();

      context.fill();
      context.stroke();
      rad--;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
