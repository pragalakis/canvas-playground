const canvasSketch = require('canvas-sketch');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    const stepX = width / 13;
    const stepY = height / 20;
    const imgData = [];
    const points = [];

    function randomTile(arr, i) {
      let index = Math.floor(Math.random() * arr.length);
      // add a zero for every tile used
      // if random tile is the same as the current tile
      // and is not a zero -> run again
      return index != i && arr[index] != 0
        ? arr.splice(index, 1, 0)[0]
        : randomTile(arr, i);
    }

    // get image tiles data
    for (let j = 0; j < height; j += stepY) {
      for (let i = 0; i < width; i += stepX) {
        imgData.push(context.getImageData(i, j, stepX, stepY));
        points.push([i, j]);
      }
    }

    // draw - tile shuffle
    points.forEach((tile, i) => {
      context.putImageData(randomTile(imgData, i), tile[0], tile[1]);
    });
  };
});
