const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  return ({ context, width, height }) => {
    // draw background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    for (let j = 0; j < height; j += 0.05) {
      for (let i = 0; i < width; i += 0.05) {
        const noise = random.noise2D(0.1 * i, 0.1 * j);

        // color depending on noise
        if (noise < -0.15 && noise > -0.2) {
          context.fillStyle = 'hsl(0, 0%, 6%)';
        } else if (noise > 0.4 && noise < 0.45) {
          context.fillStyle = 'hsl(0, 0%, 6%)';
        } else {
          context.fillStyle = 'hsl(0, 0%, 96%)';
        }

        // draw square pixel
        context.fillRect(i, j, 0.05, 0.05);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
