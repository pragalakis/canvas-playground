const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const r = () =>
    Math.random() >= 0.5 ? -Math.random() / 3 : Math.random() / 3;

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const margin = 0.551;
    const sizex = (width - 21 * margin) / 20;
    const sizey = (height - 11 * margin) / 10;

    // grid
    for (let j = margin; j < height; j += sizey + margin) {
      for (let i = margin; i < width; i += sizex + margin) {
        // uneven rects
        context.beginPath();
        context.moveTo(i + r(), j + r());
        context.lineTo(i + sizex + r(), j + r());
        context.lineTo(i + sizex + r(), j + sizey + r());
        context.lineTo(i + r(), j + sizey + r());
        context.closePath();
        context.fill();
      }
    }
  };
};
canvasSketch(sketch, settings);
