const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  const size = 1.1;
  const lines = [];
  // calculate grid points for each line
  for (let j = size / 2; j < 42; j += size) {
    const line = [];
    const noise = j * 0.02;
    for (let i = size; i < 29.7; i += size) {
      const x = i + (noise * (sign() * Math.random())) / 2;
      const y = j + (noise * (sign() * Math.random())) / 2;
      line.push([x, y]);
    }
    lines.push(line);
  }

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,5%)';
    context.lineWidth = 0.05;

    let flag = true;
    // draw grid
    lines.forEach((line, i) => {
      line.forEach((points, j) => {
        if (flag) {
          if (j < line.length - 1 && i < lines.length - 1) {
            context.beginPath();
            context.moveTo(lines[i + 1][j + 1][0], lines[i + 1][j + 1][1]);
            context.lineTo(lines[i + 1][j][0], lines[i + 1][j][1]);
            context.lineTo(points[0], points[1]);
            context.lineTo(line[j + 1][0], line[j + 1][1]);
            context.closePath();
            context.stroke();
          }
        } else {
          if (j <= line.length - 1 && i < lines.length - 1) {
            context.beginPath();
            context.moveTo(points[0], points[1]);
            context.lineTo(lines[i + 1][j][0], lines[i + 1][j][1]);
            context.stroke();
          }
        }
      });
      flag = !flag;
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
