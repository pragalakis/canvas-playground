const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;
const color = require('canvas-sketch-util').color.names;
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // hexagon radius
  const size = 0.41;
  // hexagon coords
  const points = pointsOnCircle(6, size);

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,3%)';
    context.lineWidth = 0.05;

    let odd = true;
    // draw
    for (let j = -size; j < height + size; j = j + (Math.sqrt(3) * size) / 2) {
      for (let i = 0; i < width + size; i += 3 * size) {
        // random color
        context.fillStyle = `${
          Object.values(color)[
            Math.floor(Math.random() * Object.keys(color).length)
          ]
        }`;

        // noise
        const noise = random.noise2D(i, j);
        if (noise > 0) {
          // hexagon
          context.beginPath();
          points.forEach(v => {
            if (odd) {
              context.lineTo(v.x + i + size + size / 2, v.y + j);
            } else {
              context.lineTo(v.x + i, v.y + j);
            }
          });
          context.closePath();
          context.fill();
          context.stroke();
        }
      }
      odd = !odd;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
