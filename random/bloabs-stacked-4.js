const canvasSketch = require('canvas-sketch');
var random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 8%)';
    context.lineWidth = 0.05;

    function points(rad) {
      // bloaby circle-points generation
      const N = 127;
      const data = Array(N)
        .fill()
        .map((v, i) => {
          const point = 0.05 * i;
          const noise = random.noise1D(point) * rad * 0.05;
          const phi = point + Math.PI * 2;

          const x = rad * Math.cos(phi) + noise;
          const y = rad * Math.sin(phi) + noise;
          const p = Math.random() < 0.02 ? true : false;
          return [x, y, p];
        })
        .slice(0, N - 3);

      // draw circle
      context.beginPath();
      data.forEach(v => {
        context.lineTo(width / 2 + v[0], height / 2 + v[1]);
      });

      context.closePath();
      context.stroke();

      // draw sub circles
      data.forEach(v => {
        if (v[2]) {
          context.beginPath();
          context.arc(
            width / 2 + v[0],
            height / 2 + v[1],
            0.2,
            0,
            Math.PI * 2,
            true
          );
          if (Math.random() >= 0.5) {
            context.fillStyle = 'hsl(0, 0%, 98%)';
            context.fill();
            context.stroke();
          } else {
            context.fillStyle = 'hsl(0, 0%, 8%)';
            context.fill();
          }
        }
      });
    }

    let rad = 1;
    while (rad < 13) {
      points(rad);
      rad += 1;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
