const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 8%)';
    context.lineWidth = 0.05;

    const range = (min, max) => Math.random() * (max - min) + min;

    let size = 20;
    while (size > 0) {
      // squares
      context.strokeRect(
        width / 2 - size / 2,
        height / 2 - size / 2,
        size,
        size
      );

      // sub squares
      const x = range(width / 2 - size / 2, width / 2 + size / 2) - 0.3 / 2;
      const x1 = width / 2 - size / 2 - 0.3 / 2;
      const x2 = width / 2 + size / 2 - 0.3 / 2;
      const y = range(height / 2 - size / 2, height / 2 + size / 2 - 0.3 / 2);
      const y1 = height / 2 - size / 2 - 0.3 / 2;
      const y2 = height / 2 + size / 2 - 0.3 / 2;

      // color
      if (Math.random() >= 0.5) {
        context.fillStyle = 'hsl(0, 0%, 8%)';
      } else {
        context.fillStyle = 'hsl(0, 0%, 98%)';
      }

      // horizontal sub-square
      context.beginPath();
      if (Math.random() >= 0.5) {
        context.rect(x, y1, 0.3, 0.3);
      } else {
        context.rect(x, y2, 0.3, 0.3);
      }
      context.fill();
      context.stroke();

      // vertical sub-square
      context.beginPath();
      if (Math.random() >= 0.5) {
        context.rect(x1, y, 0.3, 0.3);
      } else {
        context.rect(x2, y - 0.3 / 2, 0.3, 0.3);
      }
      context.fill();
      context.stroke();

      size -= 2;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
