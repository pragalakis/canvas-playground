const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.1;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;
    const size = 15;

    // draw square
    context.strokeRect(width / 2 - size / 2, height / 2 - size / 2, size, size);

    // line points
    const points = [
      [Math.random() * size, 0, 0, Math.random() * size],
      [size, Math.random() * size, 0, Math.random() * size],
      [Math.random() * size, 0, size, Math.random() * size],
      [Math.random() * size, 0, Math.random() * size, size],
      [0, Math.random() * size, Math.random() * size, size],
      [size, Math.random() * size, Math.random() * size, size]
    ];

    // change the canvas area from A3 to the drawn rect size
    context.translate(width / 2 - size / 2, height / 2 - size / 2);

    const drawText = (x, y) => {
      let text;
      if (x !== size && x !== 0) {
        text = x;
      } else {
        text = y;
      }

      if (x === 0) x -= fontSize * 2;
      if (x === size) x += fontSize / 2;
      if (y === 0) y -= fontSize * 0.5;
      if (y === size) y += fontSize * 1.5;
      context.fillText(text.toFixed(), x, y);
    };

    points.forEach(point => {
      // draw line
      context.beginPath();
      context.moveTo(point[0], point[1]);
      context.lineTo(point[2], point[3]);
      context.stroke();

      //draw text
      drawText(point[0], point[1]);
      drawText(point[2], point[3]);
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
