const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.1;

    let size = 1;
    const lines = function(i, j) {
      let chance = 0.2;
      if (Math.random() <= chance) {
        context.beginPath();
        context.moveTo(i, j);
        context.lineTo(i + size, j);
        context.stroke();
      }
      if (Math.random() <= chance) {
        context.beginPath();
        context.moveTo(i, j);
        context.lineTo(i, j + size);
        context.stroke();
      }
      if (Math.random() <= chance) {
        context.beginPath();
        context.moveTo(i, j);
        context.lineTo(i + size, j + size);
        context.stroke();
      }
      if (Math.random() <= chance) {
        context.beginPath();
        context.moveTo(i + size, j);
        context.lineTo(i, j + size);
        context.stroke();
      }
    };

    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        lines(i, j);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
