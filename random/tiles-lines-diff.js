const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    //background
    context.fillStyle = 'hsl(0,0%,6%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,97%)';

    // draw rect grid
    const sizeX = 0.5;
    const sizeY = 1;
    for (let j = 0; j < height; j += sizeY) {
      for (let i = 0; i < width; i += sizeX) {
        if (Math.random() >= 0.5) {
          context.fillRect(i, j, sizeX, sizeY);
        }
      }
    }

    // where the magic happens
    context.globalCompositeOperation = 'difference';
    context.strokeStyle = 'hsl(0,0%,97%)';
    context.lineWidth = 0.5;

    // draw random lines
    let n = 15;
    while (n > 0) {
      context.beginPath();
      context.moveTo(Math.random() * width, 0);
      context.lineTo(Math.random() * width, height);
      context.stroke();

      context.beginPath();
      context.moveTo(0, Math.random() * height);
      context.lineTo(width, Math.random() * height);
      context.stroke();
      n--;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
