const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');
const palette = require('nice-color-palettes');

// pick number color from the palette
const color = Math.floor(Math.random() * 100);

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,94%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.5, -0.5);
    context.strokeStyle = '#836054';
    context.lineWidth = 0.05;

    let size = 0.9;
    let offset = size - size / 2;
    let rad = size / 4;

    const circles = function() {
      for (let j = size; j < height - size; j += offset) {
        for (let i = size; i < width - size; i += offset) {
          // random point on given rad
          let angle = Math.random() * Math.PI * 2;
          let x = Math.cos(angle) * rad;
          let y = Math.sin(angle) * rad;

          let noise = random.noise2D(i * 0.5, j * 0.5);
          if (noise <= -0.47) {
            context.strokeStyle = palette[color][0];
          } else if (noise > -0.47 && noise <= -0.2) {
            context.strokeStyle = palette[color][1];
          } else if (noise > -0.2 && noise <= 0.1) {
            context.strokeStyle = palette[color][2];
          } else if (noise > 0.1 && noise <= 0.42) {
            context.strokeStyle = palette[color][3];
          } else {
            context.strokeStyle = palette[color][4];
          }

          context.beginPath();
          context.arc(
            i + size / 2 + x,
            j + size / 2 + y,
            size / 6,
            0,
            Math.PI * 2,
            true
          );
          context.stroke();

          context.beginPath();
          context.arc(
            i + size / 2 - x,
            j + size / 2 - y,
            size / 6,
            0,
            Math.PI * 2,
            true
          );
          context.stroke();
        }
      }
    };

    circles();
  };
};
// Start the sketch
canvasSketch(sketch, settings);
