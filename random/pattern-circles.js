const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 1;

    // fix offset
    context.translate(11, 11);

    const rad = 8;
    let r = true;

    // draw circle
    function circle(x, y) {
      context.beginPath();
      context.arc(x, y, rad, 0, Math.PI * 2, true);
      context.fill();
    }

    for (let j = 0; j < height; j += 50) {
      for (let i = 0; i < width; i += 50) {
        let d = 10;
        if (Math.random() > 0.99 && r) {
          r = false;
          d = Math.random() * 5 + 4;

          context.fillStyle = 'black';
          circle(i + 3 * d, j);

          context.fillStyle = 'green';
          circle(i, j + 3 * d);

          context.fillStyle = 'brown';
          circle(i + 3 * d, j + 3 * d);
        }

        context.fillStyle = 'rgba(255, 0, 0, 0.8)';
        circle(i, j);

        context.fillStyle = 'rgba(0, 0, 255, 0.8)';
        circle(i + d, j + d);

        context.fillStyle = 'rgba(255, 255, 0, 0.8)';
        circle(i + 2 * d, j + 2 * d);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
