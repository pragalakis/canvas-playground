const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -Math.random() : Math.random());
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'black';
    context.fillRect(0, 0, width, height);

    let iterations = 15;
    for (let i = 0; i < iterations; i++) {
      context.fillStyle = `rgb(${15 * i}, 0, ${15 * i})`;
      let size = iterations + 1 - i;

      let n = 100 + 5 * i;
      while (n > 0) {
        const x = Math.random() * width;
        const y = Math.random() * height;

        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + sign() * size, y + sign() * size);
        context.lineTo(x + sign() * size, y + sign() * size);
        context.closePath();
        context.fill();
        n--;
      }
    }
  };
};
canvasSketch(sketch, settings);
