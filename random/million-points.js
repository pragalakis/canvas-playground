const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'black';

    let N = 1000000;
    let data = Array(N)
      .fill(0)
      .map((v, i) => {
        const phi = Math.random() * Math.PI;
        const alpha = random.range(0.3, 1);
        const x = Math.cos(i) * Math.cos(phi);
        const y = Math.tan(i);
        const rad = random.rangeFloor(1, 23);
        return [rad * x, rad * y, alpha];
      });

    let size = 0.03;
    data.forEach(v => {
      context.beginPath();
      context.fillStyle = `rgba(0,0,0,${v[2]})`;
      context.fillRect(v[0] + width / 2, v[1] + height / 2, size, size);
    });
  };
};
// Start the sketch
canvasSketch(sketch, settings);
