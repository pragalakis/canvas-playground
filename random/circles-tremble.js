const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  duration: 0.5,
  fps: 4,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const points = [];
  const size = 3;
  const margin = 1.1;
  const step = size + margin;
  // point generation
  for (let j = 2.3 * margin; j < 42; j += step) {
    for (let i = 2.3 * margin; i < 29.7; i += step) {
      points.push([i, j]);
    }
  }

  const sign = () => (Math.random() >= 0.5 ? -1 : 1);

  return ({ context, width, height, playhead }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;

    // draw circles
    points.forEach((v, i) => {
      context.beginPath();
      if (i % 2 === 0) {
        context.arc(
          v[0] + (sign() * playhead) / Math.PI,
          v[1] + (sign() * playhead) / Math.PI,
          size / 2,
          0,
          Math.PI * 2,
          true
        );
        context.fill();
      } else {
        context.arc(v[0], v[1], size / 2, 0, Math.PI * 2, true);
        context.stroke();
      }
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
