const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

var colorIndex = 0;
// Artwork function
const sketch = update => {
  // starting color
  let colors = ['#6b5c7b', '#f8b195', '#c06c84', 'yellow'];
  document.body.onkeyup = function(e) {
    // pressing space changes the color
    if (e.keyCode == 32) {
      colorIndex = colorIndex >= 3 ? 0 : colorIndex + 1;
      switch (colorIndex) {
        case 0:
          // grey color
          colors = ['#999999', '#777777', '#555555', 'yellow'];
          break;
        case 1:
          // white color
          colors = ['#ffffff', '#ffffff', '#ffffff', '#ffffff'];
          break;
        case 2:
          // random color
          let palette = palettes[Math.floor(Math.random() * palettes.length)];
          colors = [palette[0], palette[1], palette[2], palette[3]];
          break;
        case 3:
          // starting color
          colors = ['#6b5c7b', '#f8b195', '#c06c84', 'yellow'];
      }
      update.render();
    }
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,5%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    // draw stars
    context.fillStyle = 'white';
    for (let i = 0; i < 10000; i++) {
      context.fillRect(Math.random() * width, Math.random() * 6, 0.01, 0.01);
    }

    // fixing the position of the city
    context.translate(15, 8);

    const boxWidth = 1;
    const boxHeight = 2;
    const lightChance = 0.3;

    const leftWindows = function(x, y, w, h) {
      h = h / 3;
      w = w / 5;

      if (Math.random() > 0.3) {
        // left window
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w, y - w / 2);
        context.lineTo(x - w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();
        if (Math.random() <= lightChance) {
          context.fillStyle = colors[3];
          context.fill();
        } else {
          context.stroke();
        }
      }

      if (Math.random() > 0.3) {
        // right window
        x = x + 0.35;
        y = y + 0.15;
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w, y - w / 2);
        context.lineTo(x - w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();

        if (Math.random() <= lightChance) {
          context.fillStyle = colors[3];
          context.fill();
        } else {
          context.stroke();
        }
      }
    };

    const rightWindows = function(x, y, w, h) {
      h = h / 3;
      w = w / 5;

      if (Math.random() > 0.3) {
        // right window
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + w, y - w / 2);
        context.lineTo(x + w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();

        if (Math.random() <= lightChance) {
          context.fillStyle = colors[3];
          context.fill();
        } else {
          context.stroke();
        }
      }

      if (Math.random() > 0.3) {
        // left window
        x = x - 0.35;
        y = y + 0.15;
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + w, y - w / 2);
        context.lineTo(x + w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();

        if (Math.random() <= lightChance) {
          context.fillStyle = colors[3];
          context.fill();
        } else {
          context.stroke();
        }
      }
    };

    const door = function(x, y, w, h) {
      h = h / 3;
      w = w / 3;

      if (Math.random() >= 0.5) {
        x = x + 0.35;
        y = y - 0.17;
        // right side
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + w, y - w / 2);
        context.lineTo(x + w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();
        context.stroke();
      } else {
        x = x - 0.35;
        y = y - 0.17;
        // left side
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w, y - w / 2);
        context.lineTo(x - w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();
        context.stroke();
      }
    };

    const roof = function(x, y, w, h) {
      let random = Math.random();
      y = y - h;

      if (random <= 0.3) {
        // right direction
        context.beginPath();
        context.fillStyle = colors[0];
        context.moveTo(x, y);
        context.lineTo(x + w / 2, y - w);
        context.lineTo(x + w, y - w / 2);
        context.closePath();
        context.fill();
        context.stroke();

        context.fillStyle = colors[1];
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + w / 2, y - w);
        context.lineTo(x - w / 2, y - 1.5 * w);
        context.lineTo(x - w, y - w / 2);
        context.closePath();
        context.fill();
        context.stroke();
      } else if (random > 0.3 && random <= 0.6) {
        // left direction
        context.fillStyle = colors[1];
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w / 2, y - w);
        context.lineTo(x - w, y - w / 2);
        context.closePath();
        context.fill();
        context.stroke();

        context.fillStyle = colors[0];
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w / 2, y - w);
        context.lineTo(x + w / 2, y - 1.5 * w);
        context.lineTo(x + w, y - w / 2);
        context.closePath();
        context.fill();
        context.stroke();
      } else {
        // top side
        context.fillStyle = colors[2];
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w, y - w / 2);
        context.lineTo(x, y - 2 * (w / 2));
        context.lineTo(x + w, y - w / 2);
        context.closePath();
        context.fill();
        context.stroke();
      }
    };

    const floor = function(x, y, w, h, fn, groundFloor) {
      while (fn >= 0) {
        // right side
        context.fillStyle = colors[0];
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + w, y - w / 2);
        context.lineTo(x + w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();
        context.fill();

        // left side
        context.fillStyle = colors[1];
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - w, y - w / 2);
        context.lineTo(x - w, y - h - w / 2);
        context.lineTo(x, y - h);
        context.closePath();
        context.fill();

        // stroke lines
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x, y - h);
        context.stroke();

        context.beginPath();
        context.moveTo(x - w, y - w / 2);
        context.lineTo(x - w, y - h - w / 2);
        context.stroke();

        context.beginPath();
        context.lineTo(x + w, y - w / 2);
        context.lineTo(x + w, y - h - w / 2);
        context.stroke();

        if (groundFloor) {
          // draw door
          door(x, y, w, boxHeight);
        } else {
          // draw windows
          rightWindows(x + 0.6, y - 0.2, w, boxHeight);
          leftWindows(x - 0.6, y - 0.2, w, boxHeight);
        }
        groundFloor = false;

        fn -= 1;
        y -= h;
        h = boxHeight / 2;
      }

      // draw roof
      if (!groundFloor) roof(x, y + h, w, h);
    };

    const N = 16;
    const points = Array(N)
      .fill(0)
      .map((v, j) => {
        let arr = [];
        let offsetX = boxWidth;

        let total = j % 2 == 0 ? N : N - 1;
        for (let i = 0; i < total; i++) {
          let x = offsetX - total;
          let y = j * 2.5;

          arr.push([x, y]);
          offsetX += 2 * boxWidth;
        }

        return arr;
      });

    points.forEach(arr => {
      arr.forEach(v => {
        // random number of max floors
        let floorsNum = Math.floor(Math.random() * 4) + 1;

        // draw building
        let groundFloor = true;
        floor(v[0], v[1], boxWidth, boxHeight / 2, floorsNum, groundFloor);
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
