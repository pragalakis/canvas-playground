const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  return ({ context, width, height }) => {
    //background
    context.fillStyle = 'hsl(0,50%,60%)';
    context.fillRect(0, 0, width, height);

    // grid
    const margin = 0.3;
    const size = (width - margin * 2) / 23.5;
    for (let j = margin; j < height - margin * 2; j += size + margin) {
      for (let i = margin * 1.3; i < width - margin * 2; i += size + margin) {
        context.save();

        // fill square
        context.fillStyle = 'hsl(0,0%,6%)';
        context.strokeStyle = 'hsl(0,50%,60%)';
        context.beginPath();
        context.rect(i, j, size, size);
        context.clip();
        context.fill();

        // fill random curve
        context.fillStyle = 'hsl(0,50%,60%)';
        context.beginPath();
        if (Math.random() >= 0.5) {
          context.moveTo(i - 0.3, j + size / 2 + Math.random() * sign());
          context.bezierCurveTo(
            i + size / 4 + Math.random() * sign(),
            j + Math.random() * sign(),
            i + (3 * size) / 4 + Math.random() * sign(),
            j + size + Math.random() * sign(),
            i + size + Math.random(),
            j + size / 2 + Math.random() * sign()
          );

          // closing path
          if (Math.random() >= 0.5) {
            context.lineTo(i + size, j);
            context.lineTo(i, j);
          } else {
            context.lineTo(i + size, j + size);
            context.lineTo(i, j + size);
          }

          context.closePath();
          context.fill();
        } else {
          context.moveTo(i + size / 2 + Math.random() * sign(), j - 0.3);
          context.bezierCurveTo(
            i + (3 * size) / 4 + Math.random() * sign(),
            j + size / 4 + Math.random() * sign(),
            i + size / 4,
            j + (3 * size) / 4 + Math.random() * sign(),
            i + size / 2 + Math.random() * sign(),
            j + size + Math.random()
          );

          // closing path
          if (Math.random() >= 0.5) {
            context.lineTo(i + size, j + size);
            context.lineTo(i + size, j);
          } else {
            context.lineTo(i, j + size);
            context.lineTo(i, j);
          }
          context.closePath();

          context.fill();
        }

        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
