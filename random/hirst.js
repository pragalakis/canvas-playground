const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // fix offset
    context.translate(-0.15, 0.0);

    function rangeFloor(min, max) {
      return Math.floor(Math.random() * (max - min) + min);
    }

    const offset = 1;
    const rad = 0.25;
    for (let i = offset; i < width; i += offset) {
      for (let j = offset; j < height; j += offset) {
        let r = rangeFloor(50, 250);
        let g = rangeFloor(50, 250);
        let b = rangeFloor(50, 250);
        context.fillStyle = `rgba(${r},${g},${b},1)`;

        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
