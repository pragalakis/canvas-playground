const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const offset = () =>
    Math.random() >= 0.5 ? -Math.random() / 4 : Math.random() / 4;

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;
    const size = 2;
    const margin = 0.88;

    // grid
    for (let j = 1.5 * margin; j < height - margin; j += size + margin) {
      for (let i = margin; i < width - margin; i += size + margin) {
        const clip = Math.random() >= 0.5 ? true : false;
        context.save();

        // draw outside polygon
        context.beginPath();
        context.moveTo(i + offset(), j + offset());
        context.lineTo(i + size + offset(), j + offset());
        context.lineTo(i + size + offset(), j + size + offset());
        context.lineTo(i + offset(), j + size + offset());
        context.closePath();
        if (!clip) {
          context.clip();
          lines();
        }
        context.stroke();

        // draw inside polygon
        context.beginPath();
        context.moveTo(i + Math.random() * size, j);
        context.lineTo(i + size, j + Math.random() * size);
        context.lineTo(i + Math.random() * size, j + size);
        context.lineTo(i, j + Math.random() * size);
        context.closePath();
        if (clip) {
          context.clip();
          lines();
        } else {
          context.fill();
        }
        context.stroke();

        // draw lines
        function lines() {
          // random rotation
          const angle = Math.random() * Math.PI;
          context.save();
          context.translate(i + size / 2, j + size / 2);
          context.rotate(angle);
          context.translate(-i - size / 2, -j - size / 2);
          for (let z = i - 1; z < i + size + 1; z += 0.2) {
            context.beginPath();
            context.moveTo(z, j - 0.2);
            context.lineTo(z, j + size + 0.2);
            context.stroke();
          }
          context.restore();
        }

        context.restore();
      }
    }
  };
};
canvasSketch(sketch, settings);
