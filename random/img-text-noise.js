const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');
const load = require('load-asset');

// async sketch
canvasSketch(async ({ update }) => {
  // await the image loader, returns loaded <img>
  const image = await load('./mona-lisa.jpg');

  // when image is loaded -> update the output
  update({
    // Sketch parameters
    dimensions: [image.width, image.height],
    pixelsPerInch: 300,
    units: 'px'
  });

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;

    // clear drawed image
    context.clearRect(0, 0, width, height);

    // background color
    context.fillStyle = 'hsl(0,0%,0%)';
    context.fillRect(0, 0, width, height);

    const bytes = 4;
    // alphabet
    const string = '01';
    const word = string.split('');

    // font-size = step
    const step = 15;
    context.font = `${step}px Arial`;
    context.fillStyle = 'white';

    // fix offset
    context.translate(step / 10, step / 2);

    for (let i = step; i < height - step; i += step) {
      for (let j = step; j < width - step; j += step) {
        const colorIndex = i * (width * bytes) + j * bytes;

        // color depending on random noise
        const noise = random.noise2D(i, j);
        if (noise > 0) {
          let r = data[colorIndex];
          let g = data[colorIndex + 1];
          let b = data[colorIndex + 2];
          let a = data[colorIndex + 3];

          context.fillStyle = `rgb(${r},${g},${b})`;
        }

        context.fillText(word[Math.floor(Math.random() * word.length)], j, i);
      }
    }
  };
});
