const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const points = [];
  const rad = 0.2;
  const step = 1;
  for (let i = 2 * rad; i < 29.7; i += step) {
    for (let j = 2 * rad; j < 42; j += step) {
      points.push([i, j]);
    }
  }

  // point siblings
  const sib = [[0, 0], [step, 0], [0, step], [step, step]];

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    // line settings
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;

    points.forEach(v => {
      // draw circles
      context.beginPath();
      context.arc(v[0], v[1], rad, 0, Math.PI * 2, true);
      context.stroke();

      // draw lines
      const indx = Math.floor(Math.random() * sib.length);
      context.beginPath();
      context.moveTo(v[0], v[1]);
      context.lineTo(sib[indx][0] + v[0], sib[indx][1] + v[1]);
      context.stroke();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
