const canvasSketch = require('canvas-sketch');
var random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.05;

    function points(rad) {
      // bloaby circle-points generation
      const N = 127;
      const data = Array(N)
        .fill()
        .map((v, i) => {
          // new random utility
          const point = 0.05 * i;
          const noise = random.noise1D(point * 2) * rad * 0.1;
          const phi = point + Math.PI * 2;

          const x = rad * Math.cos(phi) + noise;
          const y = rad * Math.sin(phi) + noise;

          return [x, y];
        })
        .slice(0, N - 3);

      //draw
      context.beginPath();
      data.forEach(v => {
        context.lineTo(width / 2 + v[0], height / 2 + v[1]);
      });

      context.closePath();
      context.stroke();
    }

    let rad = 0.5;
    while (rad < 13) {
      points(rad);
      rad += 0.4;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
