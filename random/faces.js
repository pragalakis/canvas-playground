const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  const points = (n, sx, sy, radX, radY) =>
    Array(n)
      .fill()
      .map((v, i) => {
        const phi = (i / n) * Math.PI * 2;
        const x = radX * Math.cos(phi) * Math.cos((Math.random() / 2) * sign());
        const y = radY * Math.sin(phi) * Math.cos((Math.random() / 2) * sign());
        return [sx + x, sy + y, phi];
      });

  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    const face = (x, y, rad) => {
      return {
        head: points(30, x, y, rad, rad),
        leftEye: points(
          10,
          x - Math.random(),
          y - Math.random(),
          rad / 10,
          rad / 10
        ),
        rightEye: points(
          10,
          x + Math.random(),
          y - Math.random(),
          rad / 10,
          rad / 10
        ),
        mouth: Array(10)
          .fill()
          .map((v, i) => {
            const x2 = i * rad * 0.1;
            const y2 = Math.cos(-Math.random() * i) / 10;
            return [x - 1 + x2, y + 1 + y2];
          }),
        nose: Array(5)
          .fill()
          .map((v, i) => {
            const x2 = Math.cos(-Math.random() * i) / 10;
            const y2 = i * rad * 0.05;
            return [x + x2, y + y2];
          }),
        hair: points(100, x, y, rad, rad).filter(n => n[2] > Math.PI)
      };
    };

    // grid
    for (let j = 3.5; j < height - 1; j += 6) {
      for (let i = 2.5; i < width - 1; i += 6) {
        let data = face(i, j, 2);

        // draw head
        const head = data.head;
        context.beginPath();
        context.moveTo(head[0][0], head[0][1]);
        head.forEach(v => {
          context.lineTo(v[0], v[1]);
        });
        context.closePath();
        context.stroke();

        // draw eyes
        const left = data.leftEye;
        context.beginPath();
        context.moveTo(left[0][0], left[0][1]);
        left.forEach(v => {
          context.lineTo(v[0], v[1]);
        });
        context.closePath();
        context.stroke();

        const right = data.rightEye;
        context.beginPath();
        context.moveTo(right[0][0], right[0][1]);
        right.forEach(v => {
          context.lineTo(v[0], v[1]);
        });
        context.closePath();
        context.stroke();

        // draw mouth
        const mouth = data.mouth;
        context.beginPath();
        context.moveTo(mouth[0][0], mouth[0][1]);
        mouth.forEach(v => {
          context.lineTo(v[0], v[1]);
        });
        context.stroke();

        // draw nose
        const nose = data.nose;
        context.beginPath();
        context.moveTo(nose[0][0], nose[0][1]);
        nose.forEach(v => {
          context.lineTo(v[0], v[1]);
        });
        context.stroke();

        // draw hair
        const hair = data.hair;
        if (Math.random() >= 0.5) {
          // spiked hair
          hair.forEach(v => {
            context.beginPath();
            context.moveTo(v[0], v[1]);
            context.lineTo(v[0] + sign() * Math.random(), v[1] - Math.random());
            context.stroke();
          });
        } else {
          // curly hair
          context.beginPath();
          context.moveTo(hair[0][0], hair[0][1]);
          hair.forEach(v => {
            context.bezierCurveTo(
              v[0] + sign() * Math.random(),
              v[1] + sign() * Math.random(),
              v[0] + sign() * Math.random(),
              v[1] + sign() * Math.random(),
              v[0] + sign() * Math.random(),
              v[1] + sign() * Math.random()
            );
          });
          context.stroke();
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
