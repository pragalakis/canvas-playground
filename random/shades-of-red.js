const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.03;

    function randomColor() {
      return `rgb(${Math.floor(Math.random() * 255)}, 0, 0)`;
    }

    function range(min, max) {
      return Math.random() * (max - min) + min;
    }

    let N = 200000;
    const margin = 2;
    const size = 0.5;
    while (N > 0) {
      context.strokeStyle = randomColor();
      context.strokeRect(
        range(margin, width - margin - size),
        range(margin, height - margin - size),
        size,
        size
      );

      N--;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
