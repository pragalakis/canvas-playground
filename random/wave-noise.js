const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,4%)';
    context.lineWidth = 0.05;

    const margin = 1;
    const size = 0.2;
    for (let j = margin; j < height - margin; j += 2 * size) {
      for (let i = margin; i < width - margin; i += 2 * size) {
        const noise = random.noise2D(i * 0.05, j * 0.05);

        // draw cross
        context.beginPath();
        context.moveTo(i + noise, j + noise - size / 2);
        context.lineTo(i + noise, j + noise + size / 2);
        context.stroke();

        context.beginPath();
        context.moveTo(i + noise - size / 2, j + noise);
        context.lineTo(i + noise + size / 2, j + noise);
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
