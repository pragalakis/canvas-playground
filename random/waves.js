const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,96%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.2, 0);
    context.lineWidth = 0.05;

    let stepX = 3;
    let stepY = 0.25;
    let sizeX = stepX / 1.5;
    let sizeY = stepY / 2.5;
    for (let i = stepX; i < width - sizeX; i += stepX) {
      context.strokeStyle = 'rgba(0,0,0,0.8)';
      context.beginPath();
      context.moveTo(i, 0);
      context.lineTo(i, height);
      context.stroke();
      for (let j = stepY; j < height - stepY; j += stepY) {
        let d = Math.random() > 0.5 ? -1 * Math.random() : Math.random();
        context.strokeStyle = 'rgba(255,0,0,0.8)';
        context.strokeRect(i - (sizeX + d) / 2, j, sizeX + d, sizeY);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
