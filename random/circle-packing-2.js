const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const range = (min, max) => Math.random() * (max - min) + min;
  const dist = (x1, x2, y1, y2) =>
    Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));

  const colors = ['white', 'black', 'yellow', 'blue', 'green', 'red'];

  // circle points
  const N = 10000;
  const points = [];
  for (let i = 0; i < N; i++) {
    let intersects = false;
    const rad = range(1, 3);
    const x = range(rad, width - rad);
    const y = range(rad, height - rad);
    const color = colors[Math.floor(Math.random() * colors.length)];

    // check if the new circle intersects with the old ones
    for (let j = 0; j < points.length; j++) {
      let d = dist(x, points[j][0], y, points[j][1]);
      if (d < rad + points[j][2]) {
        intersects = true;
      }
    }
    if (!intersects) points.push([x, y, rad, color]);
  }

  // sub-circle points
  const subPoints = [];
  for (let i = 0; i < points.length; i++) {
    const circles = [];
    let N2 = range(100, 500);

    for (let j = 0; j < N2; j++) {
      let intersects = false;
      const color = colors[Math.floor(Math.random() * colors.length)];
      const rad = points[i][2] / 20;
      const phi = Math.random() * Math.PI * 2;
      const x = points[i][0] + Math.cos(phi) * range(0.2, points[i][2]);
      const y = points[i][1] + Math.sin(phi) * range(0.2, points[i][2]);

      // check if the new sub-circle intersects with the old ones
      for (let z = 0; z < circles.length; z++) {
        let d = dist(x, circles[z][0], y, circles[z][1]);
        if (d < rad + circles[z][2]) {
          intersects = true;
        }
      }
      if (!intersects) circles.push([x, y, rad, color]);
    }

    subPoints.push(circles);
  }

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'blue';
    context.fillRect(0, 0, width, height);
    for (let j = 0; j < height; j += 0.1) {
      for (let i = 0; i < width; i += 0.1) {
        const noise = random.noise2D(i * 0.035, j * 0.035);
        if (noise < -0.5) {
          context.fillStyle = colors[0];
        } else if (noise >= -0.5 && noise < -0.2) {
          context.fillStyle = colors[1];
        } else if (noise >= -0.2 && noise < 0.1) {
          context.fillStyle = colors[2];
        } else if (noise >= 0.1 && noise < 0.3) {
          context.fillStyle = colors[3];
        } else if (noise >= 0.3 && noise < 0.6) {
          context.fillStyle = colors[4];
        } else {
          context.fillStyle = colors[5];
        }
        context.beginPath();
        context.arc(i, j, 0.1, 0, Math.PI * 2, true);
        context.fill();
      }
    }

    // draw circle
    points.forEach((v, i) => {
      context.fillStyle = v[3];

      context.save();
      context.beginPath();
      context.arc(v[0], v[1], v[2], 0, Math.PI * 2, true);
      context.clip();
      context.fill();

      // draw sub-circles
      subPoints.forEach(circle => {
        circle.forEach(v2 => {
          context.fillStyle = v2[3];
          context.beginPath();
          context.arc(v2[0], v2[1], v2[2], 0, Math.PI * 2, true);
          context.fill();
        });
      });

      context.restore();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
