const canvasSketch = require('canvas-sketch');
const util = require('canvas-sketch-util');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // calculate line points with noise
  const linePoints = (iMin, iMax, oMin, oMax) =>
    Array(iMax)
      .fill()
      .map((v, i) => {
        const rnd = util.random.createRandom();
        const noise = (Math.cos(i) * rnd.noise1D(i)) / 5;
        const y = util.math.mapRange(i, iMin, iMax, oMin, oMax);
        return [noise, y];
      });

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';

    const size = 1;
    const N = 10;
    const margin = 0.5;

    // grid
    for (let j = margin * 1.5; j < height; j += size + margin) {
      for (let i = margin / 2; i < width; i += size + margin) {
        // horizontal line
        const ph = linePoints(0, N, i, i + size);
        context.beginPath();
        context.moveTo(i, j);
        ph.forEach(v => {
          context.lineTo(v[1], j + v[0]);
        });
        context.stroke();

        // vertical line
        const pv = linePoints(0, N, j - size / 2, j + size / 2);
        context.beginPath();
        context.moveTo(i + size + margin / 2, j - size / 2);
        pv.forEach(v => {
          context.lineTo(i + size + margin / 2 + v[0], v[1]);
        });
        context.stroke();
      }
    }
  };
};
canvasSketch(sketch, settings);
