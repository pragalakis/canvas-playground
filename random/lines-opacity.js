const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // https://stackoverflow.com/questions/21646738/convert-hex-to-rgba/28056903#28056903
    const hexToRGB = (hex, alpha) => {
      const r = parseInt(hex.slice(1, 3), 16);
      const g = parseInt(hex.slice(3, 5), 16);
      const b = parseInt(hex.slice(5, 7), 16);

      return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
    };

    const draw = y => {
      // draw lines on square
      for (let i = width / 2 - size / 2; i < width / 2 + size / 2; i += lw) {
        // random color from the palettes with fixed opacity
        const color = palettes[Math.floor(Math.random() * palettes.length)][0];
        context.strokeStyle = hexToRGB(color, 0.7);

        // draw line
        context.beginPath();
        context.moveTo(i, y - size / 2);
        context.lineTo(i, y + size / 2);
        context.stroke();
      }
    };

    // settings
    const lw = 0.5;
    context.lineWidth = lw;
    let size = width;

    // draw squares for different y
    draw(height / 2 - size);
    draw(height / 2 - size / 2);
    draw(height / 2);
    draw(height / 2 + size / 2);
    draw(height / 2 + size);

    size = width / 2;
    draw(height / 2 - size);
    draw(height / 2 - size / 2);
    draw(height / 2);
    draw(height / 2 + size / 2);
    draw(height / 2 + size);

    size = width / 3;
    draw(height / 2 - size);
    draw(height / 2 - size / 2);
    draw(height / 2);
    draw(height / 2 + size / 2);
    draw(height / 2 + size);

    size = width / 4;
    draw(height / 2 - size);
    draw(height / 2 - size / 2);
    draw(height / 2);
    draw(height / 2 + size / 2);
    draw(height / 2 + size);
  };
};
canvasSketch(sketch, settings);
