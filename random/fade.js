const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(0.7, 0.0);
    context.lineWidth = 0.06;

    let size = 1.5;

    for (let i = size + 0.5; i <= width - size; i = i + size * 2.3) {
      let alpha = 1;
      for (let j = size + 0.5; j <= height - size; j = j + size * 2.3) {
        // draw circles
        context.beginPath();
        context.strokeStyle = `rgba(255,0,0,${alpha})`;
        context.arc(i, j, size, 0, Math.PI * 2, true);
        context.arc(i, j, Math.random(), 0, Math.PI * 2, true);
        context.stroke();

        // opacity
        alpha = alpha - alpha / 4;

        // draw squares
        context.strokeStyle = `rgba(255,0,0,${alpha})`;
        context.strokeRect(i - size, j - size, size * 2, size * 2);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
