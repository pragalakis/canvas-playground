const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,5%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.04;

    context.translate(0, 0);

    const box = function(x, y, w, h) {
      // right side
      context.fillStyle = '#6b5c7b';
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x + w, y - w / 2);
      context.lineTo(x + w, y - h - w / 2);
      context.lineTo(x, y - h);
      context.closePath();
      context.fill();
      context.stroke();

      // left side
      context.fillStyle = '#f8b195';
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x - w, y - w / 2);
      context.lineTo(x - w, y - h - w / 2);
      context.lineTo(x, y - h);
      context.closePath();
      context.fill();
      context.stroke();

      // top side
      context.fillStyle = '#c06c84';
      context.beginPath();
      context.moveTo(x, y - h);
      context.lineTo(x - w, y - h - w / 2);
      context.lineTo(x, y - h - 2 * (w / 2));
      context.lineTo(x + w, y - h - w / 2);
      context.closePath();
      context.fill();
      context.stroke();
    };

    const boxWidth = 1;
    const boxHeight = 6;
    const N = 20;
    const points = Array(N)
      .fill(0)
      .map((v, j) => {
        let arr = [];
        let offsetX = boxWidth;

        let total = j > N / 2 ? N - j : j;
        for (let i = 0; i < total; i++) {
          let x = offsetX - total;
          let y = 5 + 30 * Math.tan(Math.random());

          arr.push([x, y]);
          offsetX += 2 * boxWidth;
        }

        return arr;
      });

    points.forEach(arr => {
      arr.forEach(v => {
        box(width / 2 + v[0], v[1], boxWidth, boxHeight);
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
