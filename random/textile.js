const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.strokeStyle = 'hsl(0, 0%, 100%)';
    context.lineWidth = 0.03;

    function randomColor(s) {
      return `hsl(${Math.floor(Math.random() * 70)}, ${s + 10}%, 50%)`;
    }

    let strokeArr = [];

    function drawShape(i, j) {
      let x, y1, y2;
      let flag = false;
      if (Math.random() <= 0.05) {
        x = width / 14 + 0.5;
        y1 = 2.48;
        y2 = 4.5;
        flag = true;
      } else {
        x = width / 14;
        y1 = 2;
        y2 = 4;
      }
      context.fillStyle = randomColor(j);
      context.beginPath();
      context.moveTo(i, j);
      context.lineTo(i - x, j + y1);
      context.lineTo(i - x, j + y2);
      context.lineTo(i, j + 2);
      context.closePath();
      context.fill();

      if (flag) strokeArr.push({ i: i, j: j, x: x, y1: y1, y2: y2 });
    }

    for (let i = width / 14; i < width + 1; i += width / 14) {
      // colored top
      context.fillStyle = randomColor(-5);
      context.fillRect(i - width / 14, 0, width / 14, height);

      for (let j = 0; j < height; j += 2) {
        // colored shapes
        drawShape(i, j);
      }
    }

    // draw stroked tiles
    strokeArr.forEach(v => {
      context.beginPath();
      context.moveTo(v.i, v.j);
      context.lineTo(v.i - v.x, v.j + v.y1);
      context.lineTo(v.i - v.x, v.j + v.y2);
      context.lineTo(v.i, v.j + 2);
      context.closePath();
      context.stroke();
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
