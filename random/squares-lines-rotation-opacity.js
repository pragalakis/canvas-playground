const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    const lw = 0.5;
    context.lineWidth = lw;
    const size = 10.5;
    const colors = ['rgba(0,0,0,0.4)', 'rgba(255,255,255,1)'];

    const draw = y => {
      let colorIndx = 0;

      context.save();
      // rotation
      context.translate(width / 2, y);
      context.rotate(Math.random() * Math.PI * 2);
      context.translate(-width / 2, -y);

      // draw lines on square
      for (let i = width / 2 - size / 2; i < width / 2 + size / 2; i += lw) {
        context.strokeStyle = colors[colorIndx];

        context.beginPath();
        context.moveTo(i, y - size / 2);
        context.lineTo(i, y + size / 2);
        context.stroke();

        colorIndx = colorIndx >= colors.length - 1 ? 0 : colorIndx + 1;
      }
      context.restore();
    };

    // draw squares for different y
    draw(height / 2 - size);
    draw(height / 2 - size / 2);
    draw(height / 2);
    draw(height / 2 + size / 2);
    draw(height / 2 + size);
  };
};
canvasSketch(sketch, settings);
