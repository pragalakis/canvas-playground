const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const size = 3.25;
    const margin = 0.5;

    // grid
    for (let j = size / 5; j < height - size; j += size + margin) {
      for (let i = 0.1; i < width; i += size + margin) {
        let n = 5 + Math.random() * 10;
        while (n > 0) {
          // rect
          const sizex = Math.random() * size;
          const sizey = Math.random() * size;
          context.strokeRect(
            i + size / 2 - sizex / 2,
            j + size / 2 - sizey / 2,
            sizex,
            sizey
          );
          n--;
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
