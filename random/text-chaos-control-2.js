const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // text color
    context.fillStyle = 'hsl(0, 50%, 56%)';

    // distance calculation
    const dist = function(x1, x2, y1, y2) {
      return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    };

    // random point between min and max
    const range = (min, max) => Math.random() * (max - min) + min;

    // settings
    const margin = 1;
    const maxRad = 1; // max font size
    const minRad = 0.3; // min font size
    let points = [];

    // generate points that dont intersect - given a random radius
    const N = 200000;
    for (let i = 0; i < N; i++) {
      let intersects = false;

      let rad = range(minRad, maxRad);
      let x = range(0 + rad + margin, width - rad - margin);
      let y = range(0 + rad + margin, height - rad - margin);

      for (let j = 0; j < points.length; j++) {
        // check for every point in the points array
        // if the current point intersects
        let d = dist(x, points[j][0], y, points[j][1]);
        if (d < rad + points[j][2]) {
          intersects = true;
        }
      }

      if (!intersects) points.push([x, y, rad]);
    }

    // draw text with random rotation
    const text = 'CHAOS';
    points.forEach(v => {
      const fontSize = v[2] / 2;
      context.font = `${fontSize}pt Monospace`;
      const textW = context.measureText(text).width;

      context.save();
      // text rotation
      context.translate(v[0], v[1]);
      context.rotate(Math.random() * Math.PI);
      context.translate(-v[0], -v[1]);

      context.fillText(text, v[0] - textW / 2, v[1] + fontSize / 2);
      context.restore();
    });

    // settings
    const fontSize2 = 1;
    const text2 = 'CONTROL';
    context.font = `${fontSize2}pt Monospace`;
    const textW2 = context.measureText(text2).width;

    // draw centered rect
    context.fillRect(
      width / 2 - textW2,
      height / 2 - textW2 / 2,
      2 * textW2,
      textW2
    );

    // draw centered text
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillText(text2, width / 2 - textW2 / 2, height / 2 + fontSize2 / 2);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
