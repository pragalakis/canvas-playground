const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const size = 1;
    const orientation = [
      [size, size],
      [-size, size],
      [size, -size],
      [-size, -size]
    ];
    const margin = 2;
    let x = width / 2;
    let y = height / 2;
    let minX = x;
    let maxX = x;
    let minY = y;
    let maxY = y;

    let n = 1000;
    while (n > 0) {
      context.beginPath();
      context.moveTo(x, y);
      const oIndx = Math.floor(Math.random() * orientation.length);
      context.lineTo(x + orientation[oIndx][0], y + orientation[oIndx][1]);
      context.stroke();

      if (x + orientation[oIndx][0] < 2) {
        x -= orientation[oIndx][0];
      } else if (x + orientation[oIndx][0] > width - margin) {
        x -= orientation[oIndx][0];
      } else {
        x += orientation[oIndx][0];
      }

      if (y + orientation[oIndx][1] < 2) {
        y -= orientation[oIndx][1];
      } else if (y + orientation[oIndx][1] > height - margin) {
        y -= orientation[oIndx][1];
      } else {
        y += orientation[oIndx][1];
      }

      minX = x < minX ? x : minX;
      maxX = x > maxX ? x : maxX;
      minY = y < minY ? y : minY;
      maxY = y > maxY ? y : maxY;
      n--;
    }

    // border
    context.beginPath();
    context.moveTo(minX - size, minY - size);
    context.lineTo(maxX + size, minY - size);
    context.lineTo(maxX + size, maxY + size);
    context.lineTo(minX - size, maxY + size);
    context.closePath();
    context.stroke();
  };
};
canvasSketch(sketch, settings);
