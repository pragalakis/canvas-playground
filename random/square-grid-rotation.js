const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,4%)';
    context.lineWidth = 0.05;

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);
    const margin = 2;
    let size = (width - margin * 2) / 10;
    let sizeSquare = size;

    for (let j = margin; j < height - margin; j += size) {
      for (let i = margin; i < width - margin; i += size) {
        while (sizeSquare > 0) {
          context.save();
          // random rotation
          context.translate(i + sizeSquare / 2, j + sizeSquare / 2);
          if (sizeSquare != size) {
            context.rotate(Math.random() * sign() * 0.01 * Math.PI);
          }
          context.translate(-i - sizeSquare / 2, -j - sizeSquare / 2);

          // draw square
          context.strokeRect(
            i + sizeSquare / 2,
            j + sizeSquare / 2,
            size - sizeSquare,
            size - sizeSquare
          );

          context.restore();
          sizeSquare -= 0.3;
        }
        sizeSquare = size;
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
