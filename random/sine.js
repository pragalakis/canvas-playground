// check https://github.com/mattdesl/canvas-sketch/blob/master/examples/canvas-generative-silhouette.js
const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');
const { lerp } = require('canvas-sketch-util/math');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'black';

    let N = 20000;
    let data = Array(N)
      .fill(0)
      .map((v, i) => {
        const alpha = Math.random();
        const angle = (i / N) * Math.PI * 2;
        const noisex = Math.cos(angle);
        const noisey = Math.sin(angle);
        const nf = 0.07 + random.range(0, 0.5);
        const noise = random.noise2D(noisex * nf, noisey * nf);
        const offset = random.gaussian(0, 0.02);

        const x = noise * 5;
        const y = lerp(0, height, i / N + offset);

        return [x, y, alpha];
      });

    data.forEach(v => {
      context.beginPath();
      context.fillStyle = `rgba(0,0,0,${v[2]})`;
      context.arc(v[0] + width / 2, v[1], 0.04, 0, Math.PI * 2, true);
      context.fill();
    });
  };
};
// Start the sketch
canvasSketch(sketch, settings);
