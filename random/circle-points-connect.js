const canvasSketch = require('canvas-sketch');
const pointsOnCircle = require('points-on-circle');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const rad1 = 14;
    const rad2 = 10;
    const rad3 = 6;
    const rad4 = 2;
    const x = width / 2;
    const y = height / 2;
    const p = pointsOnCircle(500);

    // circle lines from point to point
    p.forEach(v => {
      context.beginPath();
      context.moveTo(x + v.x * rad1, y + v.y * rad1);
      if (Math.random() >= 0.3) {
        context.lineTo(x + v.x * rad2, y + v.y * rad2);
      }
      context.stroke();

      context.beginPath();
      context.moveTo(x + v.x * rad2, y + v.y * rad2);
      if (Math.random() >= 0.4) {
        context.lineTo(x + v.x * rad3, y + v.y * rad3);
      }
      context.stroke();

      context.beginPath();
      context.moveTo(x + v.x * rad3, y + v.y * rad3);
      if (Math.random() >= 0.5) {
        context.lineTo(x + v.x * rad4, y + v.y * rad4);
      }
      context.stroke();
    });
  };
};
canvasSketch(sketch, settings);
