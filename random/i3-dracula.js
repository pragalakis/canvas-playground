const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: [1680, 1048],
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = '#282a36';
    context.fillRect(0, 0, width, height);

    // grid color
    context.strokeStyle = '#44475a';
    context.lineWidth = 0.5;

    const topMargin = 25;
    const step = 30;

    // vertical lines
    for (let i = 0; i < width; i += step) {
      context.beginPath();
      context.moveTo(i, topMargin);
      context.lineTo(i, height);
      context.stroke();
    }

    // horizontal lines
    for (let i = topMargin; i < height; i += step) {
      context.beginPath();
      context.moveTo(0, i);
      context.lineTo(width, i);
      context.stroke();
    }

    // dracula theme color palette
    const colors = [
      '#f8f8f2',
      '#6272a4',
      '#8be9fd',
      '#50fa7b',
      '#ffb86c',
      '#ff79c6',
      '#bd93f9',
      '#ff5555',
      '#f1fa8c'
    ];

    const rad = 6;
    // draw a circle inside every grid cell
    for (let j = 0; j < width; j += step) {
      for (let i = topMargin; i < height; i += step) {
        // random color
        let color = colors[Math.floor(Math.random() * colors.length)];
        context.fillStyle = color;

        context.beginPath();
        context.arc(j + step, i + step, rad, 0, Math.PI * 2, true);
        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
