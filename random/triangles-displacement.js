const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(20,40%,80%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,97%)';
    context.lineWidth = 0.05;

    // point generation
    const size = width / 20;
    const points = [];
    let odd = true;
    for (let j = size / 2; j <= height; j += size) {
      const line = [];
      for (let i = size / 4; i <= width; i += size) {
        const x = odd ? i + size / 2 : i;
        const y = j;
        line.push([
          (sign() * Math.random()) / 2 + x,
          (sign() * Math.random()) / 2 + y
        ]);
      }
      odd = !odd;
      points.push(line);
    }

    // draw
    odd = true;
    points.forEach((line, i) => {
      line.forEach((v, j) => {
        if (odd) {
          if (j < line.length - 1) {
            context.fillStyle = `hsl(${Math.random() * 20},${40 +
              Math.random() * 30}%,${40 + Math.random() * 30}%)`;
            context.beginPath();
            context.moveTo(v[0], v[1]);
            context.lineTo(points[i + 1][j][0], points[i + 1][j][1]);
            context.lineTo(points[i + 1][j + 1][0], points[i + 1][j + 1][1]);
            context.closePath();
            context.fill();
            context.stroke();

            context.fillStyle = `hsl(${Math.random() * 20},${40 +
              Math.random() * 30}%,${40 + Math.random() * 30}%)`;
            context.beginPath();
            context.moveTo(v[0], v[1]);
            context.lineTo(line[j + 1][0], line[j + 1][1]);
            context.lineTo(points[i + 1][j + 1][0], points[i + 1][j + 1][1]);
            context.closePath();
            context.fill();
            context.stroke();
          }
        } else {
          if (j < line.length - 1) {
            context.fillStyle = `hsl(${Math.random() * 20},${40 +
              Math.random() * 30}%,${40 + Math.random() * 30}%)`;
            context.beginPath();
            context.moveTo(v[0], v[1]);
            context.lineTo(line[j + 1][0], line[j + 1][1]);
            context.lineTo(points[i + 1][j][0], points[i + 1][j][1]);
            context.closePath();
            context.fill();
            context.stroke();

            context.fillStyle = `hsl(${Math.random() * 20},${40 +
              Math.random() * 30}%,${40 + Math.random() * 30}%)`;
            context.beginPath();
            context.moveTo(line[j + 1][0], line[j + 1][1]);
            context.lineTo(points[i + 1][j][0], points[i + 1][j][1]);
            context.lineTo(points[i + 1][j + 1][0], points[i + 1][j + 1][1]);
            context.closePath();
            context.fill();
            context.stroke();
          }
        }
      });
      odd = !odd;
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
