const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,7%)';
    context.fillRect(0, 0, width, height);

    // font color
    context.fillStyle = 'hsl(0,0%,97%)';

    const fontSize = 2;
    context.font = `${fontSize}pt FreeMono`;
    //context.font = `${fontSize}pt Lato Hairline`;

    const text = '1986';
    const textW = context.measureText(text).width;

    let c = 1;
    for (let y = 4; y < height; y += fontSize * 2) {
      let x = width / 2 - textW / 2;
      let n = c;
      if (c === 1) {
        context.fillText('1986', x, y);
      } else {
        while (n > 0) {
          x += sign() * Math.random();
          context.fillText('1986', x, y + sign() * Math.random());
          n--;
        }
      }
      c++;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
