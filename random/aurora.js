const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    // calculate points
    const stepy = 0.3;
    const points = [];
    let p = Math.random();
    for (let j = 0; j < height; j += stepy) {
      points.push([p, j]);
      p += Math.random() >= 0.5 ? -1 * Math.random() : Math.random();
    }

    // draw points
    const stepx = 0.3;
    let rad = 0.1;
    for (let i = width / 2; i < width / 2 + 4; i += stepx) {
      points.forEach(v => {
        context.beginPath();
        context.arc(v[0] + i, v[1], rad, 0, Math.PI * 2, true);
        context.fill();

        context.beginPath();
        context.arc(
          v[0] + width - i,
          v[1] + Math.random(),
          rad,
          0,
          Math.PI * 2,
          true
        );
        context.fill();
      });
      rad -= 0.001;
    }
  };
};
canvasSketch(sketch, settings);
