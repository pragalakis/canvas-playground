const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const N = 100000;
  // calculate N points inside a ring
  const ring = Array(N)
    .fill(0)
    .map((_, i) => {
      const ring_thickness = 1;
      const random = Math.cos(i) / ring_thickness;
      const phi = Math.random() * Math.PI * 2;
      const theta = random * Math.PI;
      const x = (theta * Math.cos(phi)) / Math.tan(theta);
      const y = (theta * Math.sin(phi)) / Math.tan(theta);
      const rad = 2;

      // big distance from the light center = darker color
      const alpha = Math.random();
      return [rad * x, rad * y, alpha];
    });

  return ({ context, width, height }) => {
    // background
    context.fillStyle = '#29274f';
    context.fillRect(0, 0, width, height);

    // draw points
    ring.forEach(v => {
      context.fillStyle = `rgba(32,178,170,${v[2]})`;
      context.fillRect(width / 2 + v[0], height / 2 + v[1], 0.05, 0.05);
    });
  };
};

canvasSketch(sketch, settings);
