const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 96%)';

    // fix offset
    context.translate(0, 0.4);

    function shape(i, j, rad, rotate) {
      context.beginPath();
      context.save();
      context.translate(i, j);
      context.rotate(rotate[0]);
      context.translate(-i, -j);
      context.arc(i, j, rad, 0, Math.PI, true);
      context.fill();
      context.restore();

      context.beginPath();
      context.save();
      context.translate(i, j);
      context.rotate(rotate[1]);
      context.translate(-i, -j);
      context.arc(i, j, rad, 0, Math.PI, false);
      context.fill();
      context.restore();
    }

    const rad = 1.2;
    const offset = 0.5;
    const rotation = [
      [0, Math.PI / 2],
      [0, (3 * Math.PI) / 2],
      [Math.PI, (3 * Math.PI) / 2],
      [Math.PI, Math.PI / 2]
    ];

    // fill the canvas with shapes with a random rotation
    for (let j = rad + offset; j < height; j += rad * 2 + offset) {
      for (let i = rad + offset; i < width; i += rad * 2 + offset) {
        let rotate = rotation[Math.floor(Math.random() * rotation.length)];
        shape(i, j, rad, rotate);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
