const canvasSketch = require('canvas-sketch');

const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'black';
    context.lineWidth = 0.05;

    const sign = () => (Math.random() >= 0.5 ? -1 : 1);

    // background horizontal lines
    for (let i = 0; i < width; i += 0.4) {
      for (let j = 0; j < height; j += 0.4) {
        let y = j + sign() * Math.random();
        context.beginPath();
        context.moveTo(i, y);
        context.lineTo(i + Math.random(), y);
        context.stroke();
      }
    }

    // circle path
    const rad = 10;
    const x = width / 2;
    const y = height / 2;
    context.beginPath();
    context.arc(x, y, rad, 0, Math.PI * 2, true);
    context.clip();

    // circle vertical lines
    for (let i = x - rad; i < x + rad; i += 0.4) {
      for (let j = y - rad; j < y + rad; j += 0.4) {
        let h = i + sign() * Math.random();
        context.beginPath();
        context.moveTo(h, j);
        context.lineTo(h, j + Math.random());
        context.stroke();
      }
    }
  };
};

canvasSketch(sketch, settings);
