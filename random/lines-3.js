const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.1;

    let size = 1;
    const lines = function(i, j) {
      let chance = 0.8;
      let count = 0;
      if (Math.random() <= 0.5) {
        if (Math.random() <= 0.5) {
          //|\
          //| \
          //|__\
          context.beginPath();
          if (Math.random() <= chance) {
            context.moveTo(i, j);
            context.lineTo(i + size, j + size);
            count++;
          }
          if (Math.random() <= chance) {
            if (count == 0) context.moveTo(i + size, j + size);
            context.lineTo(i, j + size);
            count++;
          }
          context.stroke();
          if (count == 2 && Math.random() > 0.8) {
            context.closePath();
            context.fill();
          }
        } else {
          // ___
          // \  |
          //  \ |
          //   \|
          context.beginPath();
          if (Math.random() <= chance) {
            context.moveTo(i, j);
            context.lineTo(i + size, j);
            count++;
          }
          if (Math.random() <= chance) {
            if (count == 0) context.moveTo(i + size, j);
            context.lineTo(i + size, j + size);
            count++;
          }
          context.stroke();
          if (count == 2 && Math.random() > 0.8) {
            context.closePath;
            context.fill();
          }
        }
      } else {
        if (Math.random() <= 0.5) {
          // ___
          //|  /
          //| /
          //|/
          context.beginPath();
          if (Math.random() <= chance) {
            context.moveTo(i, j);
            context.lineTo(i + size, j);
            count++;
          }
          if (Math.random() <= chance) {
            if (count == 0) context.moveTo(i + size, j);
            context.lineTo(i, j + size);
            count++;
          }
          context.stroke();
          if (count == 2 && Math.random() > 0.8) {
            context.closePath;
            context.fill();
          }
        } else {
          //   /|
          //  / |
          // /__|
          context.beginPath();
          if (Math.random() <= chance) {
            context.moveTo(i + size, j);
            context.lineTo(i + size, j + size);
            count++;
          }
          if (Math.random() <= chance) {
            if (count == 0) context.moveTo(i + size, j + size);
            context.lineTo(i, j + size);
            count++;
          }
          context.stroke();
          if (count == 2 && Math.random() > 0.8) {
            context.closePath;
            context.fill();
          }
        }
      }
    };
    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        lines(i, j);
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
