const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    //context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillStyle = 'hsl(0, 0%, 8%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.03;

    // fix offset
    context.translate(0, 0.5);

    function randomRange(min, max) {
      return Math.random() * (max - min) + min;
    }

    function randomColor() {
      let r = Math.floor(Math.random() * 255);
      let g = Math.floor(Math.random() * 255);
      let b = Math.floor(Math.random() * 255);
      return `rgba(${r},${g},${b},0.4)`;
    }

    const N = Math.floor(width + width / 5);
    const data = Array(100)
      .fill(0)
      .map((v, i) => {
        let spread = 1;

        let arr = [];
        for (let j = 0; j < N; j += 0.1) {
          let angle = randomRange(0.1, 1) * Math.PI;
          let x = j + angle;
          let y = Math.cos(j) + angle;
          arr.push([spread * x, spread * y]);
        }

        return arr;
      });

    let size = 0.05;
    // draw rects for its line point
    context.strokeStyle = randomColor();
    data.forEach(arr => {
      //context.strokeStyle = randomColor();
      arr.forEach(v => {
        context.strokeRect(-6 + v[0], 1 + v[1], size, size);
        context.strokeRect(-3.2 + v[0], 4.2 + v[1], size, size);
        context.strokeRect(-6.7 + v[0], 7.4 + v[1], size, size);
        context.strokeRect(-3.9 + v[0], 10.6 + v[1], size, size);
        context.strokeRect(-7.3 + v[0], 13.8 + v[1], size, size);
        context.strokeRect(-4.4 + v[0], 17 + v[1], size, size);
        context.strokeRect(-7.8 + v[0], 20.2 + v[1], size, size);
        context.strokeRect(-4.9 + v[0], 23.4 + v[1], size, size);
        context.strokeRect(-2.1 + v[0], 26.6 + v[1], size, size);
        context.strokeRect(-5.5 + v[0], 29.8 + v[1], size, size);
        context.strokeRect(-2.1 + v[0], 26.6 + v[1], size, size);
        context.strokeRect(-5.5 + v[0], 29.8 + v[1], size, size);
        context.strokeRect(-2.7 + v[0], 33 + v[1], size, size);
        context.strokeRect(-6.2 + v[0], 36.2 + v[1], size, size);
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
