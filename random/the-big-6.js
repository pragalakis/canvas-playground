const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 3
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  const stop = Array(6)
    .fill()
    .map(v => [Math.random() * 10]);

  return ({ context, width, height, time }) => {
    // draw once
    if (time === 0) {
      // draw background
      context.fillStyle = 'hsl(0, 0%, 6%)';
      context.fillRect(0, 0, width, height);

      // draw text
      context.fillStyle = 'hsl(0, 0%, 96%)';
      context.font = '600 2pt URW Gothic';
      context.fillText('The Big 6', 2, 3.5);
    }

    context.lineWidth = 0.05;
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.strokeStyle = 'hsl(0, 0%, 96%)';

    // draw discs
    let step = 5;
    for (let i = 0; i < 6; i++) {
      if (height - time - 3 - stop[i] > 3) {
        let x = Math.random() <= 0.1 ? step + sign() * 0.5 : step;
        context.beginPath();
        context.arc(x, height - time, 3, 0, Math.PI * 2, true);
        context.fill();
        context.stroke();
      }
      step += 4;
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
