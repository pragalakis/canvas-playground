const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.fillStyle = 'hsl(0, 0%, 6%)';
    const size = 3.25;
    const margin = 0.5;

    const rects = (x, y) => {
      // top side
      const r1 = size / 3 + Math.random() * (size / 3);
      const rx1 = x + r1 + size * Math.random() - r1;
      context.fillRect(rx1, y, r1, size / 2);

      // right side
      const r2 = size / 3 + Math.random() * (size / 3);
      const ry2 = y + r2 + size * Math.random() - r2;
      context.fillRect(x + size / 2, ry2, size / 2, r2);

      // bottom side
      const r3 = size / 3 + Math.random() * (size / 3);
      const r3x = x + r3 + size * Math.random() - r3;
      context.fillRect(r3x, y + size / 2, r3, size / 2);

      // left side
      const r4 = size / 3 + Math.random() * (size / 3);
      const ry4 = y + r4 + size * Math.random() - r4;
      context.fillRect(x, ry4, size / 2, r4);
    };

    // rect grid
    for (let j = size / 5; j < height - size; j += size + margin) {
      for (let i = 0.1; i < width; i += size + margin) {
        context.save();
        context.beginPath();
        context.rect(i, j, size, size);
        context.clip();
        rects(i, j);
        context.stroke();
        context.restore();
      }
    }
  };
};
canvasSketch(sketch, settings);
