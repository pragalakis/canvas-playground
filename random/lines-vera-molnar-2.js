const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const r = () => (Math.random() >= 0.5 ? -Math.random() : Math.random());
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const step = 0.3;

    // vertical lines
    for (let i = 0; i < width; i += step) {
      context.beginPath();
      context.moveTo(i, 0);
      context.lineTo(i, height);
      context.stroke();
    }

    // horizontal lines
    const lines = [];
    let h = 10;
    // calculate line data
    for (let j = 0; j < height; j += step) {
      lines.push([0, j, h, j]);
      h += Math.cos(j) / 4 + r() / 4;
    }

    // fill space
    context.beginPath();
    lines.forEach(v => {
      context.lineTo(v[2], v[3]);
    });
    context.lineTo(0, height);
    context.lineTo(0, 0);
    context.closePath();
    context.fill();

    // stroke lines
    lines.forEach(v => {
      context.beginPath();
      context.moveTo(v[0], v[1]);
      context.lineTo(v[2], v[3]);
      context.stroke();
    });
  };
};
canvasSketch(sketch, settings);
