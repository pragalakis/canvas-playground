// Also check Matt Desl's generative arcs
const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 5%)';
    context.fillRect(0, 0, width, height);

    function randomRange(min, max) {
      return Math.random() * (max - min) + min;
    }

    const N = 1500;
    const arcsData = Array(N)
      .fill(0)
      .map((v, i) => {
        return {
          angle: Math.PI - randomRange(0, Math.PI * 1.7),
          pointsNum: Math.floor(randomRange(30, 150)),
          rad: randomRange(1, 20),
          x: randomRange(-1.5, 1.5),
          y: randomRange(-1.5, 1.5)
        };
      });

    arcsData.forEach(value => {
      // draw rects for each arc on arcsData
      for (let i = 1; i <= value.pointsNum; i++) {
        let phi = value.angle * randomRange(1.2, 2);
        let x = value.x + Math.cos(phi) * value.rad;
        let y = value.y + Math.sin(phi) * value.rad;

        context.fillStyle = `rgba(255, 255, 255, ${randomRange(0.2, 0.8)})`;
        context.fillRect(width / 2 + x, height / 2 + y, 0.03, 0.03);
      }
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
