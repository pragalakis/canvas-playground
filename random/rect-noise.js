const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,96%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.2, 0);
    context.fillStyle = 'rgba(0,0,0,0.8)';

    function noise(i, j) {
      let arr = [];
      for (i; i < 25; i++) {
        let noise = random.noise1D(i);
        arr.push([i, j + noise / 6]);
      }
      return arr;
    }

    let x = 2;
    let y = 4;
    context.moveTo(x, y);
    noise(x, y).forEach(v => context.lineTo(v[0], v[1]));
    noise(y, width - x).forEach(v => context.lineTo(v[1], v[0]));
    noise(x, width - y).forEach(v => context.lineTo(width - v[0], v[1]));
    noise(y, x).forEach(v => context.lineTo(v[1], width - v[0]));
    context.lineTo(x, y);
    context.fill();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
