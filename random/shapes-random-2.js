const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 97%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.lineWidth = 0.05;
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    const angles = [0, Math.PI, Math.PI / 2, -Math.PI / 2];
    const size = width / 12;

    // draw functions
    const drawSquare = (x, y, size) => {
      context.strokeRect(x, y, size, size);
    };

    const drawCircle = (x, y, rad, phi = Math.PI * 2) => {
      context.beginPath();
      context.arc(x, y, rad, 0, phi, true);
      context.closePath();
      context.stroke();
    };

    const drawLine = (x1, y1, x2, y2) => {
      context.beginPath();
      context.moveTo(x1, y1);
      context.lineTo(x2, y2);
      context.stroke();
    };

    const drawTriangle = (x1, y1, x2, y2, x3, y3) => {
      context.beginPath();
      context.moveTo(x1, y1);
      context.lineTo(x2, y2);
      context.lineTo(x3, y3);
      context.closePath();
      context.stroke();
    };

    const drawTriangle2 = (x, y, size) => {
      context.beginPath();
      context.moveTo(x, y - (Math.sqrt(3) / 3) * size);
      context.lineTo(x - size / 2, y + (Math.sqrt(3) / 6) * size);
      context.lineTo(x + size / 2, y + (Math.sqrt(3) / 6) * size);
      context.closePath();
      context.stroke();
    };

    // grid
    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        const rnd = Math.random();

        if (rnd <= 0.2) {
          // full circle
          drawCircle(i + size / 2, j + size / 2, size / 2);

          // draw triangle
          if (Math.random() >= 0.5) {
            drawTriangle2(i + size / 2, j + size / 2, size - 0.2);
          }
        } else if (rnd > 0.2 && rnd <= 0.4) {
          // square
          drawSquare(i, j, size);

          // cross line
          if (Math.random() >= 0.5) {
            if (Math.random() >= 0.5) {
              drawLine(i, j, i + size, j + size);
            } else {
              drawLine(i + size, j, i, j + size);
            }
          }
        } else if (rnd > 0.4 && rnd <= 0.6) {
          // forth of a circle
          const centers = [
            [i, j],
            [i + size, j],
            [i + size, j + size],
            [i, j + size]
          ];
          let cCenter = centers[Math.floor(Math.random() * centers.length)];
          context.save();
          context.beginPath();
          context.rect(i, j, size, size);
          context.clip();
          drawCircle(cCenter[0], cCenter[1], size);
          context.restore();

          // big full circle
          cCenter = centers[Math.floor(Math.random() * centers.length)];
          if (Math.random() >= 0.9) {
            drawCircle(cCenter[0], cCenter[1], size);
          }
        } else {
          // rotation
          const angle = angles[Math.floor(Math.random() * angles.length)];
          context.save();
          context.translate(i + size / 2, j + size / 2);
          context.rotate(angle);
          context.translate(-i - size / 2, -j - size / 2);

          if (Math.random() >= 0.5) {
            // half circle
            drawCircle(i + size / 2, j + size / 2, size / 2, Math.PI);
          } else {
            // triangle
            drawTriangle(i, j, i + size, j + size, i, j + size);
          }

          context.restore();
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
