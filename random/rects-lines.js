const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.2, 0);
    context.lineWidth = 0.1;

    function rnd() {
      return Math.random();
    }

    let size = 2;
    for (let i = size; i <= width - size; i += size) {
      for (let j = size; j < height - size; j += size) {
        context.strokeRect(i, j, size, size);
        context.beginPath();
        context.moveTo(i + size / 2 + size / 6, j + size / 2 + size / 6);
        context.lineTo(i + rnd(), j + rnd());
        context.lineTo(i + rnd(), j + rnd());
        context.lineTo(i + rnd(), j + rnd());
        context.lineTo(i + rnd(), j + rnd());
        context.stroke();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
