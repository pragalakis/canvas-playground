const canvasSketch = require('canvas-sketch');
const rnd = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 3%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.strokeStyle = 'hsl(0, 0%, 98%)';
    context.lineWidth = 0.1;

    let n = 100000;
    while (n > 0) {
      const x = width / 2 + 2 * Math.log(Math.random()) + 2;
      context.fillStyle = `hsl(${300 - x * 15},100%,50%)`;
      context.fillRect(x, Math.random() * height, 0.05, 0.05);
      n--;
    }

    context.fillStyle = 'hsl(0, 0%, 3%)';
    context.beginPath();
    context.moveTo(width / 2, 0);
    for (let j = 0.1; j < height; j += 0.1) {
      context.lineTo(width / 2 + rnd.noise1D(j / 5), j);
      //context.lineTo(width / 2 + Math.cos(j) + Math.random(), j);
    }
    context.lineTo(width, height);
    context.lineTo(width, 0);
    context.closePath();
    context.fill();
  };
};
canvasSketch(sketch, settings);
