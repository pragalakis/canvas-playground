const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.strokeStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const cube = (x, y, size) => {
      // right side
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x + size, y - size / 2);
      context.lineTo(x + size, y - size - size / 2);
      context.lineTo(x, y - size);
      context.closePath();
      context.fill();
      context.stroke();

      // diagonal lines
      if (Math.random() < 0.05) {
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x + size, y - size - size / 2);
        context.stroke();

        context.beginPath();
        context.moveTo(x + size, y - size / 2);
        context.lineTo(x, y - size);
        context.stroke();
      }

      // left side
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x - size, y - size / 2);
      context.lineTo(x - size, y - size - size / 2);
      context.lineTo(x, y - size);
      context.closePath();
      context.fill();
      context.stroke();

      // diagonal lines
      if (Math.random() < 0.05) {
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - size, y - size - size / 2);
        context.stroke();

        context.beginPath();
        context.moveTo(x - size, y - size / 2);
        context.lineTo(x, y - size);
        context.stroke();
      }

      // top side
      context.beginPath();
      context.moveTo(x, y - size);
      context.lineTo(x - size, y - size - size / 2);
      context.lineTo(x, y - size - 2 * (size / 2));
      context.lineTo(x + size, y - size - size / 2);
      context.closePath();
      context.fill();
      context.stroke();

      // diagonal lines
      if (Math.random() < 0.05) {
        context.beginPath();
        context.moveTo(x, y - size);
        context.lineTo(x, y - size - 2 * (size / 2));
        context.stroke();

        context.beginPath();
        context.moveTo(x - size, y - size - size / 2);
        context.lineTo(x + size, y - size - size / 2);
        context.stroke();
      }
    };

    // cube grid
    const size = 1;
    for (let j = 2.2 * size; j < height; j += 1.5 * size) {
      for (let i = 1.2 * size; i < width - size; i += size) {
        if (Math.random() < 0.2) {
          cube(i, j, size);
        }
      }
    }
  };
};
canvasSketch(sketch, settings);
