const canvasSketch = require('canvas-sketch');

/* source
https://github.com/rmorabia/radhika.dev/blob/master/src/components/mouseTrail.js
*/

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'px'
};

// Artwork function
const sketch = () => {
  return ({ canvas, context, width, height }) => {
    const drawPoints = () => {
      // background
      context.fillStyle = 'hsl(0, 0%, 98%)';
      context.fillRect(0, 0, width, height);

      const duration = (0.7 * (1 * 1000)) / 60;

      points.forEach((v, i) => {
        let lastPoint;
        if (points[i - 1] !== undefined) {
          lastPoint = points[i - 1];
        } else lastPoint = v;

        v[2] += 1;

        if (v[2] > duration) {
          // If the point dies, remove it.
          points.shift();
        } else {
          // Otherwise animate it:
          // As the lifetime goes on, lifePercent goes from 0 to 1.
          const lifePercent = v[2] / duration;
          const spreadRate = 7 * (1 - lifePercent);

          context.lineJoin = 'round';
          context.lineWidth = spreadRate;

          const r = Math.floor(255 - (255 * lifePercent) / 2);
          const g = 0;
          const b = Math.floor(200 + (55 * lifePercent) / 2);
          context.strokeStyle = `rgb(${r},${g},${b}`;

          context.beginPath();
          context.moveTo(lastPoint[0], lastPoint[1]);
          context.lineTo(v[0], v[1]);
          context.stroke();
          context.closePath();
        }
      });
    };

    // save mouse path
    const points = [];
    const handleMouseMove = e => {
      const x = e.clientX - canvas.offsetLeft;
      const y = e.clientY - canvas.offsetTop;

      points.push([x, y, 0]);
      drawPoints();
    };

    addEventListener('mousemove', handleMouseMove, false);
  };
};
canvasSketch(sketch, settings);
