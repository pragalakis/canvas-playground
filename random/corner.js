const canvasSketch = require('canvas-sketch');
// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    // settings
    context.fillStyle = 'hsl(0, 0%, 6%)';
    context.lineWidth = 0.05;

    const x = width / 2 + sign() * Math.random() * 5;
    const angle = Math.random() * Math.PI * 2;
    let n = 1000;

    // lines
    while (n > 0) {
      // random color
      context.strokeStyle = `rgb(${Math.floor(
        Math.random() * 255
      )}, ${Math.floor(Math.random() * 255)}, ${Math.floor(
        Math.random() * 255
      )})`;

      const y = Math.random() * height;
      // corner
      context.beginPath();
      context.moveTo(0, y);
      context.lineTo(x + Math.cos(angle), y + Math.sin(angle));
      context.lineTo(width - Math.cos(angle), y - Math.sin(angle));
      context.stroke();
      n--;
    }
  };
};
canvasSketch(sketch, settings);
