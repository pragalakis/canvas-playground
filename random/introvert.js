const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  units: 'cm',
  animate: true,
  playbackRate: 'fixed',
  fps: 64
};

// Artwork function
const sketch = () => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);

  return ({ context, width, height, time }) => {
    // draw once
    if (time === 0) {
      // draw background
      context.fillStyle = 'hsl(330, 50%, 85%)';
      context.fillRect(0, 0, width, height);

      context.fillStyle = 'hsl(0, 0%, 6%)';
      context.fillRect(6, 2, width - 8, height - 4);

      // draw text
      context.save();
      context.translate(4.9, height - 4);
      context.rotate(-Math.PI / 2);
      context.translate(-4.9, -height + 4);
      context.fillStyle = 'hsl(0, 0%, 6%)';
      context.font = '600 4pt Monoton';
      context.fillText('INTROVERT', 4.9, height - 4);
      context.restore();

      // draw borders
      context.lineWidth = 0.1;
      context.strokeStyle = 'hsl(0, 0%, 6%)';

      context.moveTo(5.5, 1.5);
      context.lineTo(5.5, height - 1.5);
      context.lineTo(width - 1.5, height - 1.5);
      context.lineTo(width - 1.5, 1.5);
      context.closePath();
      context.stroke();

      context.strokeRect(0, 0, width, height);
    }

    context.fillStyle = 'hsl(330, 50%, 85%)';
    context.beginPath();
    context.rect(6, 2, width - 8, height - 4);
    context.clip();

    // labyrinth
    context.fillRect(Math.random() * width, Math.random() * height, 0.1, 0.1);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
