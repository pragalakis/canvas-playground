const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0, 0%, 96%)';
    context.fillRect(0, 0, width, height);

    const range = (min, max) => Math.random() * (max - min) + min;
    const dist = function(x1, y1, x2, y2) {
      return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    };

    // random squares
    const N = 100000;
    for (let i = 0; i < N; i++) {
      // random color
      const palette = palettes[Math.floor(Math.random() * palettes.length)];
      const color = palette[Math.floor(Math.random() * palette.length)];
      context.fillStyle = color;

      // random xy coords
      let x = range(0, width);
      let y = range(0, height);

      // square size
      let distMid = dist(x, y, width / 2, height / 2);
      let size = distMid / 70;

      // draw square
      context.fillRect(x, y, size, size);
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
