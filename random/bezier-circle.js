const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  // calculate random points on a circle
  const points = Array(1000)
    .fill()
    .map(v => {
      const phi = Math.random() * Math.PI * 2;
      const x = Math.cos(phi);
      const y = Math.sin(phi);
      return [x * 10, y * 10];
    });
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 7%)';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.05;

    // draw a bezier curve on random points
    for (let i = 0; i < points.length; i += 4) {
      const color = palettes[Math.floor(Math.random() * palettes.length)][0];
      //context.strokeStyle = 'rgba(255,255,255,0.3)';
      context.strokeStyle = color;

      const x = width / 2;
      const y = height / 2;
      context.beginPath();
      context.moveTo(x + points[i][0], y + points[i][1]);
      context.bezierCurveTo(
        x + points[i + 1][0],
        y + points[i + 1][1],
        x + points[i + 2][0],
        y + points[i + 2][1],
        x + points[i + 3][0],
        y + points[i + 3][1]
      );
      context.stroke();
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
