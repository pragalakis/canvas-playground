const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.2, -0.5);
    context.lineWidth = 0.1;

    let size = 2;
    let rotation = Math.PI / 180;

    // draw squares
    for (let i = size; i <= width - size; i += size) {
      for (let j = size; j <= height - size; j += size) {
        let sign = Math.random() < 0.5 ? -1 : 1;
        context.save();
        let rotate = j * rotation * sign * Math.random();
        context.translate(i + size / 2, j + size / 2);
        context.rotate(rotate);
        context.strokeRect(-size / 2, -size / 2, size, size);
        context.restore();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
