const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    context.lineWidth = 0.03;
    context.strokeStyle = 'black';

    let N = 100000;
    let data = Array(N)
      .fill()
      .map(() => {
        let cosTheta = 2 * Math.random() - 1;
        let phi = Math.random() * Math.PI * 2;
        let x = cosTheta * Math.log(phi);
        let y = cosTheta * Math.sin(phi);
        return [14 * x, 14 * y];
      });

    data.forEach(v => {
      context.beginPath();
      context.arc(
        width / 2 + v[1],
        height / 2 + v[0],
        0.03,
        0,
        Math.PI * 2,
        true
      );
      context.stroke();
    });
  };
};
// Start the sketch
canvasSketch(sketch, settings);
