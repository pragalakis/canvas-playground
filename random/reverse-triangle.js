const canvasSketch = require('canvas-sketch');
const palette = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    context.fillStyle = 'black';

    let leg = 1.5;
    let hyp = 2;
    function triangle(i, j, rotate) {
      if (rotate == 1) {
        // down
        context.moveTo(i, j);
        context.lineTo(i + hyp, j);
        context.lineTo(i + hyp / 2, j + leg);
        context.lineTo(i, j);
      } else if (rotate == 2) {
        // up
        context.moveTo(i, j);
        context.lineTo(i + hyp, j);
        context.lineTo(i + hyp / 2, j - leg);
        context.lineTo(i, j);
      }
    }

    for (let j = 0; j < height + leg; j += leg) {
      for (let i = -1; i <= width; i++) {
        // random color
        let color = palette[Math.floor(Math.random() * 100)];
        context.fillStyle = color[2];

        let rotate = i % 2 == 0 ? 1 : 2;

        context.beginPath();
        triangle(i, j, rotate);
        context.fill();
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
