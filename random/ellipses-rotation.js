const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 300,
  units: 'cm'
};

// Artwork function
const sketch = () => {
  return ({ context, width, height }) => {
    //background
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    // grid
    const size = width / 9;
    for (let j = 0; j < height; j += size) {
      for (let i = 0; i < width; i += size) {
        // random palette
        const palette = palettes[Math.floor(Math.random() * palettes.length)];

        // square
        context.fillStyle = palette[0];
        context.fillRect(i, j, size, size);

        // ellipses
        let n = 4;
        const angle = Math.random() * Math.PI * 2;
        let sizeX = size / 2;
        let sizeY = size / 3;
        while (n > 0) {
          context.fillStyle = palette[n];
          context.beginPath();
          context.ellipse(
            i + size / 2,
            j + size / 2,
            sizeX,
            sizeY,
            angle,
            0,
            Math.PI * 2
          );
          context.fill();
          sizeX -= 0.3;
          sizeY -= 0.3;
          n--;
        }
      }
    }
  };
};

// Start the sketch
canvasSketch(sketch, settings);
