const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  // random starting positions
  const N = 300;
  const points = Array(N)
    .fill()
    .map(() => {
      const radius = 0.2 + Math.random() * 0.5;
      const xpos = 2 * radius + Math.random() * (width - 2 * radius);
      const ypos = 2 * radius + Math.random() * (height - 2 * radius);
      const speedx = (sign() * Math.random()) / 2;
      const speedy = (sign() * Math.random()) / 2;
      const mass = Math.random() / 500;
      return {
        rad: radius,
        x: xpos,
        y: ypos,
        sx: speedx,
        sy: speedy,
        m: mass
      };
    });

  // LOOP
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,100%)';
    context.fillRect(0, 0, width, height);

    points.forEach((point, i) => {
      // position
      point.x += point.sx;
      point.y += point.sy;

      // wall bounce
      if (point.x >= width || point.x <= 0) {
        point.sx = -point.sx;
      }
      if (point.y >= height || point.y <= 0) {
        point.sy = -point.sy;
      }

      // draw circle
      context.fillStyle = `hsl(4,80%,${point.m * 20000}%)`;
      context.beginPath();
      context.arc(point.x, point.y, point.rad, 0, Math.PI * 2, true);
      context.fill();

      // iterate through each point to check for gravitational forces
      points.forEach((point2, j) => {
        if (i != j) {
          var diffX = point2.x - point.x;
          var diffY = point2.y - point.y;
          var distSquare = diffX * diffX + diffY * diffY;
          var distance = Math.sqrt(distSquare);

          // collision
          if (distance <= point.rad + point2.rad) {
            const aX =
              (point.m * point.sx + point2.m * point2.sx) /
              (point.m + point2.m);
            const aY =
              (point.m * point.sy + point2.m * point2.sy) /
              (point.m + point2.m);

            point.sx = aX;
            point.sy = aY;
            point2.sx = aX;
            point2.sy = aY;
          } else {
            const totalForce = point2.m / distSquare;
            point.sx += (totalForce * diffX) / distance;
            point.sy += (totalForce * diffY) / distance;
          }
        }
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
