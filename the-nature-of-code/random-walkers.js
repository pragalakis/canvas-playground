const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util').random;

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const rad = 0.1;
  // random starting positions
  const N = 1000;
  const points = Array(N)
    .fill()
    .map(() => {
      const xpos = 2 * rad + Math.random() * (width - 2 * rad);
      const ypos = 2 * rad + Math.random() * (height - 2 * rad);
      const speedX = Math.random() * random.sign();
      const speedY = Math.random() * random.sign();
      const acceleration = 0.01 * Math.random();
      return {
        x: xpos,
        y: ypos,
        sx: speedX,
        sy: speedY,
        a: acceleration
      };
    });

  return ({ context, width, height, time }) => {
    // background
    context.fillStyle = 'hsl(0,0%,7%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,97%)';

    points.forEach((point, i) => {
      // wall bounce
      if (point.x >= width - 2 * rad || point.x <= 2 * rad) {
        point.sx = -point.sx;
      }
      if (point.y >= height - 2 * rad || point.y <= 2 * rad) {
        point.sy = -point.sy;
      }

      // new position
      let x = point.x + point.sx * point.a;
      let y = point.y + point.sy * point.a;

      // draw
      context.beginPath();
      context.arc(x, y, rad, 0, Math.PI * 2, true);
      context.fill();

      // save the position
      point.x = x + random.noise2D(0.5 * x, 0.5 * y) * 0.05;
      point.y = y + random.noise2D(0.5 * x, 0.5 * y) * 0.05;

      // acceleration
      if (point.a > 0.1) {
        point.a /= 2;
      } else {
        point.a += point.a * 0.005;
      }
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
