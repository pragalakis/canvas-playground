const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);
  // random starting positions
  const N = 300;
  const points = Array(N)
    .fill()
    .map(() => {
      const radius = 0.2 + Math.random() * 0.5;
      const xpos = 2 * radius + Math.random() * (width - 2 * radius);
      const ypos = 2 * radius + Math.random() * (height - 2 * radius);
      const speedx = (sign() * Math.random()) / 2;
      const speedy = (sign() * Math.random()) / 2;
      const charge = Math.random() >= 0.5 ? '+' : '-';
      return {
        rad: radius,
        x: xpos,
        y: ypos,
        sx: speedx,
        sy: speedy,
        c: charge
      };
    });

  // LOOP
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,7%)';
    context.fillRect(0, 0, width, height);

    points.forEach((point, i) => {
      // position
      point.x += point.sx / 20;
      point.y += point.sy / 20;

      // wall bounce
      if (point.x >= width || point.x <= 0) {
        point.sx = -point.sx;
      }
      if (point.y >= height || point.y <= 0) {
        point.sy = -point.sy;
      }

      // draw circle
      context.fillStyle = 'hsl(0,0%,97%)';
      context.beginPath();
      context.arc(point.x, point.y, point.rad, 0, Math.PI * 2, true);
      context.fill();

      // draw text
      context.font = `${point.rad}pt Monospace`;
      context.fillStyle = 'hsl(0,0%,7%)';
      context.fillText(
        point.c,
        point.x - point.rad / 2.5,
        point.y + point.rad / 2.5
      );

      // iterate through each point to check for gravitational forces
      points.forEach((point2, j) => {
        if (i != j) {
          var diffX = point2.x - point.x;
          var diffY = point2.y - point.y;
          var distSquare = diffX * diffX + diffY * diffY;
          var distance = Math.sqrt(distSquare);

          // collision
          if (distance <= point.rad + point2.rad) {
            const aX =
              (point.rad * point.sx + point2.rad * point2.sx) /
              (point.rad + point2.rad);
            const aY =
              (point.rad * point.sy + point2.rad * point2.sy) /
              (point.rad + point2.rad);

            if (point.c === '+') {
              point.sx = aX;
              point.sy = aY;
            }
            if (point.c === '-') {
              point.sx = -aX;
              point.sy = -aY;
            }
            if (point2.c === '+') {
              point2.sx = aX;
              point2.sy = aY;
            }
            if (point2.c === '-') {
              point2.sx = -aX;
              point2.sy = -aY;
            }
          } else {
            const totalForce = point2.rad / distSquare;
            if (point.c === '+') {
              point.sx += (totalForce * diffX) / distance;
              point.sy += (totalForce * diffY) / distance;
            }
            if (point.c === '-') {
              point.sx -= (totalForce * diffX) / distance;
              point.sy -= (totalForce * diffY) / distance;
            }
            if (point2.c === '+') {
              point2.sx += (totalForce * diffX) / distance;
              point2.sy += (totalForce * diffY) / distance;
            }
            if (point2.c === '-') {
              point2.sx -= (totalForce * diffX) / distance;
              point2.sy -= (totalForce * diffY) / distance;
            }
          }
        }
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
