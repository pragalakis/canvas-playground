const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const sign = () => (Math.random() >= 0.5 ? -1 : 1);

  // random starting positions
  const N = 400;
  const rad = 0.15;
  const points = Array(N)
    .fill()
    .map(() => {
      const xpos = 2 * rad + Math.random() * (width - 2 * rad);
      const ypos = 2 * rad + Math.random() * (height - 2 * rad);
      const speedx = sign() * Math.random();
      const speedy = sign() * Math.random();
      return {
        x: xpos,
        y: ypos,
        sx: speedx,
        sy: speedy
      };
    });

  // random flow field direction tiles
  const step = 5;
  const direction = [];
  for (let j = 0; j < height; j += step) {
    for (let i = 0; i < width; i += step) {
      const minx = i;
      const maxx = i + step;
      const miny = j;
      const maxy = j + step;
      direction.push([minx, miny, maxx, maxy, sign(), sign()]);
    }
  }

  // LOOP
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,100%)';
    context.fillRect(0, 0, width, height);
    context.fillStyle = 'hsl(0,0%,0%)';

    points.forEach((point, i) => {
      // position
      point.x += point.sx * 0.1;
      point.y += point.sy * 0.1;

      /* point teleportation */
      // from right-to-left
      if (point.x > width) point.x = 0;
      // from left-to-right
      if (point.x < 0) point.x = width;
      // from bottom-to-top
      if (point.y > height) point.y = 0;
      // from top-to-bottom
      if (point.y < 0) point.y = height;

      // draw rect
      context.fillRect(point.x, point.y, rad, rad);

      // change direction depending on near particles
      points.forEach((point2, j) => {
        if (i !== j) {
          const distance = Math.hypot(point2.x - point.x, point2.y - point.y);
          if (distance < 2) {
            point.sx = (point.sx + point2.sx) / 2;
            point.sy = (point.sy + point2.sy) / 2;

            point2.sx = (point.sx + point2.sx) / 2;
            point2.sy = (point.sy + point2.sy) / 2;
          }
        }
      });

      // change direction - flow field
      direction.forEach(v => {
        if (point.x >= v[0] && point.x < v[2]) {
          if (point.y >= v[1] && point.y < v[3]) {
            // acceleration
            point.sx = point.sx > 0.5 ? point.sx / 2 : point.sx + v[4] * 0.1;
            point.sy = point.sy > 0.5 ? point.sy / 2 : point.sy + v[5] * 0.1;
          }
        }
      });
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
