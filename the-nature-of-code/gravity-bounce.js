const canvasSketch = require('canvas-sketch');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  pixelsPerInch: 72,
  animate: true,
  units: 'cm'
};

// Artwork function
const sketch = ({ width, height }) => {
  const gravity = 0.02;
  // starting data
  const N = 100;
  const points = Array(N)
    .fill()
    .map(() => {
      const radius = 0.2 + Math.random() * 0.5;
      const xpos = 2 * radius + Math.random() * (width - 2 * radius);
      const ypos = 2 * radius + Math.random() * (height - 2 * radius);
      const speedx = 0;
      const speedy = 0;
      const acceleration = gravity;
      const mass = 1 + Math.random() * 50;
      return {
        rad: radius,
        x: xpos,
        y: ypos,
        sx: speedx,
        sy: speedy,
        a: acceleration,
        m: mass
      };
    });

  //LOOP
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,7%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,97%)';

    points.forEach(point => {
      // speed
      point.sy += point.a;
      // position
      point.y += point.sy;

      // floor bounce
      if (point.y >= height - point.rad) {
        point.y = height - point.rad;
        point.sy *= -0.9; // bounce speed
      }

      // draw
      context.beginPath();
      context.arc(point.x, point.y, point.rad, 0, Math.PI * 2, true);
      context.fill();

      // acceleration
      point.a += gravity / point.m;
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
